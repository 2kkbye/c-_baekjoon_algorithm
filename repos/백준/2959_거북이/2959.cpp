#include<iostream>
#define SIZE 4
using namespace std;

int arr[SIZE];

void quick(int start, int end) {
	int s = start;
	int e = end;
	int pivot = arr[(s + e) / 2];
	do {
		while (arr[s] < pivot) s++;
		while (arr[e] > pivot) e--;
		if (s <= e) {
			int temp = arr[s];
			arr[s] = arr[e];
			arr[e] = temp;
			s++; e--;
		}
	} while (s <= e);
	if (start < e) {
		quick(start, e);
	}
	if (s < end) {
		quick(s, end);
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	for (int a = 0; a < SIZE; a++) {
		cin >> arr[a];
	}
	quick(0, SIZE - 1);
	cout << arr[0] * arr[2];
}