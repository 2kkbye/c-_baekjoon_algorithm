#include<iostream>
#define SIZE 31

using namespace std;

long long num[SIZE];
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	long long temp;
	cin >> temp;
	num[0] = 1;
	num[1] = 2;
	for (int a = 2; a < SIZE; a++) {
		num[a] = num[a - 1] * 2;
	}
	for (int a = 0; a < SIZE; a++) {
		if (num[a] == temp) {
			cout << "1"; return 0;
		}
	}
	cout << "0";
}