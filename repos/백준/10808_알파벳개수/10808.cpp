#include<iostream>
#include<string>
#define SIZE 26

using namespace std;

int arr[SIZE];
void init() {
	for (int a = 0; a < SIZE; a++) {
		arr[a] = 0;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string str;
	cin >> str;
	for (int a = 0; a < str.size(); a++) {
		arr[((int)str[a] - 97)]++;
	}
	for (int a = 0; a < SIZE; a++) {
		cout << arr[a];
		if (a == SIZE - 1) break;
		cout << " ";
	}
}