#include<iostream>
#define SIZE 20

using namespace std;

int N;
int map[SIZE][SIZE];
int copyMap[SIZE][SIZE];
int dir[5];
int ans = -1;

void init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
			copyMap[a][b] = 0;
		}
	}
	for (int a = 0; a < 5; a++) {
		dir[a] = 0;
	}
}
void Input() {
	cin >> N;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			cin >> map[a][b];
		}
	}
}
void cpMap() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			copyMap[a][b] = map[a][b];
		}
	}
}
void moveMap(int index) {
	if (index == 0) {
		for (int b = 0; b < N; b++) {
			int count = 0;
			for (int a = 0; a < N; a++) {
				if (copyMap[a][b] == 0) {
					count++; continue;
				}
				if (count == 0) continue;
				copyMap[a - count][b] = copyMap[a][b];
				copyMap[a][b] = 0;
				a -= count;
				count = 0;
			}
		}
	}
	//아래
	else if (index == 1) {
		for (int b = 0; b < N; b++) {
			int count = 0;
			for (int a = N - 1; a >=0; a--) {
				if (copyMap[a][b] == 0) {
					count++; continue;
				}
				if (count == 0) continue;
				copyMap[a + count][b] = copyMap[a][b];
				copyMap[a][b] = 0;
				a += count;
				count = 0;
			}
		}
	}
	//왼쪽
	else if (index == 2) {
		for (int a = 0; a < N; a++) {
			int count = 0;
			for (int b = 0; b < N; b++) {
				if (copyMap[a][b] == 0) {
					count++; continue;
				}
				if (count == 0) continue;
				copyMap[a][b-count] = copyMap[a][b];
				copyMap[a][b] = 0;
				b -= count;
				count = 0;
			}
		}
	}
	//오른쪽
	else {
		for (int a = 0; a < N; a++) {
			int count = 0;
			for (int b = N - 1; b >= 0; b--) {
				if (copyMap[a][b] == 0) {
					count++; continue;
				}
				if (count == 0) continue;
				copyMap[a][b + count] = copyMap[a][b];
				copyMap[a][b] = 0;
				b += count;
				count = 0;
			}
		}
	}
}
void sumMap(int index) {
	//위
	if (index == 0) {
		for (int b = 0; b < N; b++) {
			for (int a = 0; a < N-1; a++) {
				if (copyMap[a][b] == copyMap[a+1][b] && copyMap[a][b] != 0) {
					copyMap[a][b] = copyMap[a][b] * 2;
					copyMap[a+1][b] = 0;
					a++;
				}
			}
		}
	}
	//아래
	else if (index == 1) {
		for (int b = 0; b < N; b++) {
			for (int a = N-1; a >=1; a--) {
				if (copyMap[a][b] == copyMap[a - 1][b] && copyMap[a][b] != 0) {
					copyMap[a][b] = copyMap[a][b] * 2;
					copyMap[a - 1][b] = 0;
					a--;
				}
			}
		}
	}
	//왼쪽
	else if (index == 2) {
		for (int a = 0; a < N; a++) {
			for (int b = 0; b < N - 1; b++) {
				if (copyMap[a][b] == copyMap[a][b+1] && copyMap[a][b] != 0) {
					copyMap[a][b] = copyMap[a][b] * 2;
					copyMap[a][b+1] = 0;
					b++;
				}
			}
		}
	}
	//오른쪽
	else {
		for (int a = 0; a < N; a++) {
			for (int b = N-1; b >=1; b--) {
				if (copyMap[a][b] == copyMap[a][b - 1] && copyMap[a][b] != 0) {
					copyMap[a][b] = copyMap[a][b] * 2;
					copyMap[a][b - 1] = 0;
					b--;
				}
			}
		}
	}
}
int ansMap() {
	int tans = 0;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			if (copyMap[a][b] > tans) {
				tans = copyMap[a][b];
			}
		}
	}
	return tans;
}
void Solution() {
	cpMap();
	for (int a = 0; a < 5; a++) {
		moveMap(dir[a]);
		sumMap(dir[a]);
		//print();
		moveMap(dir[a]);
		//print();
	}

	int tans = ansMap();
	if (tans > ans) {
		ans = tans;
	}
}
void dfs(int level) {
	if (level == 5) {
		Solution();
		return;
	}
	for (int a = 0; a < 4; a++) {
		dir[level] = a;
		dfs(level + 1);
	}
}
void Solve() {
	init();
	Input();
	dfs(0);
	cout << ans;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();
}