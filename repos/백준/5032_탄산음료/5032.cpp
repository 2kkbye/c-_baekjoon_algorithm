#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int e, f, c, total,ans=0,temp;
	cin >> e >> f >> c;
	total = e + f;
	while (total>=c) {
		temp = (total / c);
		ans +=temp;
		total -= c * temp;
		total += temp;
	}
	cout << ans;
}