#include<iostream>
#include<vector>
using namespace std;

vector<int> v;
int N;
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	cin >> N;
	int temp;
	for (int a = 1; a <= N; a++) {
		v.push_back(a);
	}
	while (v.size()!=1) {
		cout << v.front()<<" ";
		v.erase(v.begin());
		temp = v.front();
		v.erase(v.begin());
		v.push_back(temp);
	}
	cout << v.front();
}