#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	int N;
	cin >> N;
	int res = 1;
	for (int a = 1; a <= N; a++) {
		res *= a;
	}
	cout << res;
}