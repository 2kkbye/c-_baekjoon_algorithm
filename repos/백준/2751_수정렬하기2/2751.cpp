#include<iostream>
#define SIZE 1000001

using namespace std;

int num[SIZE];

void quick(int start, int end) {
	int s = start;
	int e = end;
	int pivot = num[(s + e) / 2];
	do {
		while (num[s] < pivot) s++;
		while (num[e] > pivot) e--;
		if (s <= e) {
			int temp = num[s];
			num[s] = num[e];
			num[e] = temp;
			s++; e--;
		}
	} while (s <= e);

	if (s < end) {
		quick(s, end);
	}
	if (start < e) {
		quick(start, e);
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int n;
	cin >> n;
	for (int a = 0; a < n; a++) {
		cin >> num[a];
	}
	quick(0, n - 1);

	for (int a = 0; a < n; a++) {
		//printf("%d",num[a]);
		//if (a != n - 1) printf("\n");
		cout << num[a];
		if (a != n - 1) cout << '\n';
	}

}