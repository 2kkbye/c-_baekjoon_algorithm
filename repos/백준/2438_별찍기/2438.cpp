#include <iostream>
using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int N;

	cin >> N;
	for (int a = 0; a < N; a++) {
		for (int b = 1; b < N - a; b++) {
			cout << " ";
		}
		for (int b = 0; b < a * 2 + 1; b++) {
			if (a == 0 ) {
				cout << "*";
			}
			else {
				if(b==0 || b==a*2+1-1){
					cout << "*";
				}
				else {
					cout << " ";
				}
			}
		}
		if (a == N - 1) break;
		cout << '\n';
	}
	return 0;
}