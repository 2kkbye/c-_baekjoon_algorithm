#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int A, B, C, D, P;
	cin >> A >> B >> C >> D >> P;

	int res_A = A * P;
	int res_B = B;
	if (P > C) {
		res_B += ((P - C) * D);
	}
	if (res_A > res_B) {
		cout << res_B;
	}
	else {
		cout << res_A;
	}
}