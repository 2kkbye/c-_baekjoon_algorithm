#include<iostream>
#include<vector>
using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int n, num1, num2;
	cin >> n;
	vector<int> v_ans;
	for (int a = 0; a < n; a++) {
		cin >> num1 >> num2;
		v_ans.push_back(num1 + num2);
	}
	for (int a = 0; a < v_ans.size(); a++) {
		cout << "Case " << a + 1 << ": " << v_ans[a];
		if (a != v_ans.size() - 1) cout << endl;
	}
}