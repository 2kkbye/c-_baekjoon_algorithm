#include<iostream>
#include<vector>
#define SIZE 1001
using namespace std;

int stu[SIZE][5];
int ans[SIZE];
int N;

vector<int> v_index;
void Init() {
	for (int a = 0; a < SIZE; a++) {
		ans[a] = 0;
		for (int b = 0; b < 5; b++) {
			stu[a][b] = 0;
		}
	}
}
void Input() {
	cin >> N;
	for (int a = 1; a <= N; a++) {
		for (int b = 0; b < 5; b++) {
			cin >> stu[a][b];
		}
	}
}
int checkAns() {
	int max = -1;
	int tans = -1;
	for (int a = 1; a < SIZE; a++) {
		if (ans[a] <= max) continue;
		max = ans[a];
		tans = a;
	}
	return tans;
}
void Solution() {
	for (int b = 0; b < 5; b++) {
		for (int t = 1; t <= 9; t++) {
			v_index.clear();
			for (int a = 1; a <= N; a++) {
				if (stu[a][b] == t) {
					v_index.push_back(a);
				}
			}
			if (v_index.size() <= 1) continue;
			for (int a = 0; a < v_index.size(); a++) {
				ans[v_index[a]] += (v_index.size() - 1);
			}
		}
	}
	cout << checkAns();
}
void Solve() {
	Init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();
}