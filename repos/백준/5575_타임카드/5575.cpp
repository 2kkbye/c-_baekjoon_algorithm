#include<iostream>
#define SIZE 3
using namespace std;

int time[2][SIZE];
void Solve() {
	for (int a = 2; a >= 0; a--) {
		if (time[1][a] == -1) time[1][a] = 59;
		if (time[1][a] - time[0][a] < 0) {
			time[1][a - 1]--;
			if (time[1][a - 1] < 0) { time[1][a - 2]--; }
			time[1][a] += (60 - time[0][a]);
		}
		else {
			time[1][a] -= time[0][a];
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	for (int t = 0; t < 3; t++) {
		for (int a = 0; a < 2; a++) {
			for (int b = 0; b < SIZE; b++) {
				cin >> time[a][b];
			}
		}
		Solve();
		cout << time[1][0] << " " << time[1][1] << " "<<time[1][2];
		if (t != 2) cout << endl;
	}

}