#include<iostream>
#define MAX 2000
using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	//red -> 가장자리, brown -> 안쪽
	int R, B, L=0, W=0,temp;

	cin >> R >> B;
	//높이
	for (int a = 3; a < MAX; a++) {
		//너비
		for (int b = a; b < MAX; b++) {
			temp = (b * 2) + ((a - 2) * 2);
			if (temp > R) break;
			if (temp < R) continue;
			if ((a * b) - temp != B) continue;
			L = b, W = a;
		}
	}
	cout << L << " " << W;

}