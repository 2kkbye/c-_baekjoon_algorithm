#include<iostream>
#define SIZE 1500
using namespace std;

bool primeNum[SIZE];
void primeNumber() {
	for (int a = 0; a < SIZE; a++) {
		primeNum[a] = true;
	}
	for (int a = 2; a < SIZE; a++) {
		if (!primeNum[a]) continue;
		int index = 2;
		while (a * index < SIZE) {
			primeNum[a * index] = false;
			index++;
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	//a = 97, A = 65 z = 122, Z = 90
	//cout << (int)'z' << '\n';
	//cout << (int)'Z' << '\n';
	string str;
	cin >> str;
	primeNumber();
	int sum = 0, temp = 0;
	for (int a = 0; a < str.length(); a++) {
		temp = (int)str[a];
		if (temp >= 97 && temp <= 122) {
			temp -= 96;
		}
		else {
			temp -= 38;
		}
		sum += temp;
	}
	if (primeNum[sum]) {
		cout << "It is a prime word.";
	}
	else {
		cout << "It is not a prime word.";
	}
}