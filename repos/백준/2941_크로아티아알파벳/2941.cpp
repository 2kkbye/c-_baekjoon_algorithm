#include<iostream>
#define SIZE 8
using namespace std;

string cro[8] = { "c=","c-","dz=","d-","lj","nj","s=","z=" };
string str;
int ans = 0;
int checkCro(int index) {
	ans++;
	for (int a = 0; a < SIZE; a++) {
		if (str[index] != cro[a][0]) continue;
		for (int b = 1; b < cro[a].length(); b++) {
			bool type = true;
			if (index + b < str.length()) {
				if (str[index + b] != cro[a][b]) {
					type = false;
					break;
				}
			}
			else break;
			if(type && b==cro[a].length()-1) return cro[a].length() - 1;
		}
	}
	return 0;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cin >> str;
	int temp = 0;
	for (int a = 0; a < str.length(); a++) {
		temp = checkCro(a);
		a += temp;
	}
	cout << ans;
}