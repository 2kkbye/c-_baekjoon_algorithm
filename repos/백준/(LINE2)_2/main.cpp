#include<iostream>
#include<vector>
#define ROW_SIZE 100
#define COL_SIZE 3

using namespace std;
//flag_rules
string INPUT_FR[ROW_SIZE][COL_SIZE];
//commands
string INPUT_CM[ROW_SIZE][COL_SIZE];
void init() {
	for (int a = 0; a < ROW_SIZE; a++) {
		for (int b = 0; b < COL_SIZE; b++) {
			INPUT_FR[a][b] = "";
			INPUT_CM[a][b] = "";
		}
	}
}
bool CheckInstruction(string ins, int type) {
	//type 확인 나머지는 그냥 문자열이 같은지 안같은지만 확인
	if (type == 1) {
		// dash 확인용 변수 없으면 바로 return
		bool dash_check = false;
		string fname = "", ftype = "";
		int typeIndex = 0;
		vector<string> v_ftype;
		for (int a = 0; a < ins.length() - 1; a++) {
			if (ins[a] == '-') {
				dash_check = true;
				fname += ins[a];
			}
			else if (ins[a] == ' ' && typeIndex == 0) {
				typeIndex++;
			}
			else if (typeIndex == 0 && ins[a] != ' ') {
				fname += ins[a];
			}
			else if (typeIndex == 1 && ins[a] ==' ') {
				if (ftype.length() != 0) v_ftype.push_back(ftype);
				ftype ="";
			}
			else if (typeIndex == 1 && ins[a] != ' ') {
				ftype +=ins[a];
			}
		}
		if (!dash_check) return false;
		//분리완료
		for (int a = 0; a < ROW_SIZE; a++) {
			if (INPUT_FR[a][0] == "") break;
			if (INPUT_FR[a][1] == fname) {
				if (INPUT_FR[a][2] == "NUMBER") {
					for (int b = 0; b < v_ftype.size(); b++) {
						for (int c = 0; c < v_ftype[b].length(); c++) {
							if (v_ftype[b][c] >= '0' && v_ftype[b][c] <= '9') {
								continue;
							}
							else return false;
						}
					}
				}
				else if (INPUT_FR[a][2] == "NULL") {
					for (int b = 0; b < v_ftype.size(); b++) {
						for (int c = 0; c < v_ftype[b].length(); c++) {
							if (v_ftype[b].length()==0) {
								continue;
							}
							else return false;
						}
					}
				}
				else if (INPUT_FR[a][2] == "STRING") {
					for (int b = 0; b < v_ftype.size(); b++) {
						for (int c = 0; c < v_ftype[b].length(); c++) {
							if ((v_ftype[b][c] >= 'A' && v_ftype[b][c] <= 'Z')  || (v_ftype[b][c] >= 'a' && v_ftype[b][c] <= 'z')) {
								continue;
							}
							else return false;
						}
					}
				}
			}
		}
	}
	else {
		for (int a = 0; a < ROW_SIZE; a++) {
			if (INPUT_FR[a][type] == ins) {
				return true;
			}
			else return false;
		}
	}
	return true;;
}
vector<bool> solution(string program, vector<string> flag_rules, vector<string> commands) {
	vector<bool> answer;
	//표준 flag_rules를 저장할 배열 FR 생성
	//program, flag_name, flag_argument_type,
	init();
	for (int a = 0; a < flag_rules.size(); a++) {
		for (int b = 0; b < COL_SIZE - 1; b++) {
			if (b == 0) {
				INPUT_FR[a][b] = program;
				continue;
			}
			else {
				string temp = "";
				//flag_rule에서 name과 type 분리
				for (int c = 0; c < flag_rules[a].length(); c++) {
					if (flag_rules[a][c] == ' ') {
						INPUT_FR[a][b] = temp;
						temp = "";
					}
					else {
						temp += flag_rules[a][c];
					}
				}
				INPUT_FR[a][COL_SIZE - 1] = temp;
			}
		}
	}
	//commands 배열로 분리 및 전환
	for (int a = 0; a < commands.size(); a++) {
		string temp = "";
		int index = 0;
		bool result = true;
		for (int b = 0; b < commands[a].length(); b++) {
			if (index < 1 && commands[a][b] != ' ') {
				temp += commands[a][b];
			}
			else if (index < 1 && commands[a][b] == ' ') {
				if (!CheckInstruction(temp, index++)) {
					result = false;
					break;
				}
				temp = "";
			}
			else if (index > 0 && commands[a][b] == '-') {
				if (temp.length() != 0) {
					//검사 함수 호출
					if (!CheckInstruction(temp, index)) {
						result = false;
						break;
					}
				}
				temp = '-';
			}
			else if (index > 0 && commands[a][b] != '-') {
				temp += commands[a][b];
			}
		}
		if (!CheckInstruction(temp, index)) {
			result = false;
		}
		if (result) answer.push_back(true);
		else answer.push_back(false);
	}
	return answer;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	string program = "line";
	vector<string> flag_rules;
	vector<string> commands;
	flag_rules.push_back("-s STRING");
	flag_rules.push_back("-n NUMBER");
	flag_rules.push_back("-e NULL");
	//commands.push_back("line -s 123 -n HI");
	commands.push_back("line -n 100 102 -s hi -e");
	commands.push_back("line -n id pwd -n 100");
	vector<bool> result;
	result = solution(program, flag_rules, commands);
	for (int a = 0; a < result.size(); a++) {
		cout << result[a] << " ";
	}
}