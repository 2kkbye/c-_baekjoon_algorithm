#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N;
	cin >> N;
	int cnt = 0;
	int num;
	for(int a = 1;a<=N;a++){
		cin >> num;
		if (a == num) continue;
		cnt++;
	}
	cout << cnt;
}