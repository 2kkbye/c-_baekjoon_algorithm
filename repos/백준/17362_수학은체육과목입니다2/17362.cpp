#include<iostream>
#define SIZE 1000000000
using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int n;
	cin >> n;
	int temp = n % 8;
	if (temp == 0) { cout << "2"; return 0; }
	if (temp == 6) { cout << "4"; return 0; }
	if (temp == 7) { cout << "3"; return 0; }
	cout << temp;
}