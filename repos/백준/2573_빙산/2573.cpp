#include<iostream>
#define SIZE 300
#define DSIZE 4

using namespace std;

int dx[DSIZE] = {0,1,0,-1};
int dy[DSIZE] = {1,0,-1,0};

bool visit[SIZE][SIZE];
int map[SIZE][SIZE];
int copyMap[SIZE][SIZE];
int N, M;
void init() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			visit[a][b] = false;
			map[a][b] = copyMap[a][b];
		}
	}
}
int checkIce(int x, int y) {
	int cnt = 0;
	int px = x, py = y;
	for (int a = 0; a < DSIZE; a++) {
		int rx = px + dx[a];
		int ry = py + dy[a];
		if (rx >= 0 && ry >= 0 && rx < N && ry < M) {
			if (map[rx][ry] == 0) {
				cnt++;
			}
		}
	}
	return cnt;
}
bool checkMap() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			if (copyMap[a][b] != 0) return false;
		}
	}
	return true;
}
void dfs(int x, int y) {
	int px = x, py = y;
	for (int a = 0; a < DSIZE; a++) {
		int rx = px + dx[a];
		int ry = py + dy[a];
		if (rx >= 0 && ry >= 0 && rx < N && ry < M) {
			if (map[rx][ry] == 0) continue;
			if (visit[rx][ry]) continue;
			visit[rx][ry] = true;
			dfs(rx, ry);
		}
	}
}
void insertIce() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			if (map[a][b] == 0) continue;
			int px = a;
			int py = b;
			int cnt = 0;
			for (int c = 0; c < DSIZE; c++) {
				int rx = px + dx[c];
				int ry = py + dy[c];
				if (rx >= 0 && ry >= 0 && rx < N && ry < M) {
					if (map[rx][ry] == 0) cnt++;
				}
			}
			copyMap[px][py] = map[px][py] - cnt;
			if (copyMap[px][py] < 0) copyMap[px][py] = 0;
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cin >> N >> M;
	int temp;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			cin >> copyMap[a][b];
		}
	}
	int year = 0;
	int sizeIce = 1;
	while (true) {
		init();
		sizeIce = 0;
		for (int a = 0; a < N; a++) {
			for (int b = 0; b < M; b++) {
				if (map[a][b] == 0) continue;
				if (visit[a][b]) continue;
				sizeIce++;
				visit[a][b] = true;
				dfs(a, b);
			}
		}
		if (sizeIce > 1) {
			cout << year;
			break;
		}
		if (checkMap()) {
			cout << "0";
			break;
		}
		year++;
		insertIce();
	}
}