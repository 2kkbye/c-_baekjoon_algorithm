#include<iostream>

using namespace std;
#define SIZE 46

int arr[SIZE];

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N;
	cin >> N;
	arr[0] = 0;
	arr[1] = 1;
	for (int a = 2; a <= N; a++) {
		arr[a] = arr[a - 1] + arr[a - 2];
	}
	cout << arr[N];
	
}