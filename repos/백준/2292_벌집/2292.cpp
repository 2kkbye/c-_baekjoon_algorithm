#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	long N;
	cin >> N;
	int ans = 1;
	if (N == 1) {
		cout << ans;
		return 0;
	}
	else {
		long plus = 6;
		long num = 7;
		ans = 2;
		while (true) {
			if (num >= N) {
				break;
			}
			plus += 6;
			num += plus;
			ans++;
		}
	}
	cout << ans;
}