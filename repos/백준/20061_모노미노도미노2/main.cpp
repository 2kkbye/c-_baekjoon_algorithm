#include<iostream>
#define SIZE 10

using namespace std;
int map[SIZE][SIZE];
int dx[2] = {0,1};
int dy[2] = {1,0};
int ans = 0;
void print() {
	cout << "======================" << endl;
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			cout << map[a][b] << " ";
		}
		cout << endl;
	}
}
void Init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
		}
	}
}
void moveBolck(int x1, int y1, int x2, int y2) {
	//블록이 1개인 경우
	if (x2 == -1 && y2 == -1) {
		int px = x1, py = y1;
		//오른쪽
		while (true) {
			int rx = px + dx[0];
			int ry = py + dy[0];
			if (rx >= SIZE || ry >= SIZE) break;
			if (map[rx][ry] != 0) break;
			px = rx; py = ry;
		}
		map[px][py] = 1;
		//아래
		px = x1, py = y1;
		while (true) {
			int rx = px + dx[1];
			int ry = py + dy[1];
			if (rx >= SIZE || ry >= SIZE) break;
			if (map[rx][ry] != 0) break;
			px = rx; py = ry;
		}
		map[px][py] = 1;
	}
	//블록이 2개인 경우
	else {
		int px1 = x1, py1 = y1, px2 = x2, py2 = y2;
		//오른쪽
		while (true) {
			int rx1 = px1 + dx[0];
			int ry1 = py1 + dy[0];
			int rx2 = px2 + dx[0];
			int ry2 = py2 + dy[0];
			if (rx1 >= SIZE || ry1 >= SIZE || rx2>=SIZE || ry2>=SIZE) break;
			if (map[rx1][ry1] != 0 || map[rx2][ry2]!=0) break;
			px1 = rx1; py1 = ry1; px2 = rx2; py2 = ry2;
		}
		map[px1][py1] = 2; map[px2][py2] = 2;

		px1 = x1, py1 = y1, px2 = x2, py2 = y2;
		//아래쭉
		while (true) {
			int rx1 = px1 + dx[1];
			int ry1 = py1 + dy[1];
			int rx2 = px2 + dx[1];
			int ry2 = py2 + dy[1];
			if (rx1 >= SIZE || ry1 >= SIZE || rx2 >= SIZE || ry2 >= SIZE) break;
			if (map[rx1][ry1] != 0 || map[rx2][ry2] != 0) break;
			px1 = rx1; py1 = ry1; px2 = rx2; py2 = ry2;
		}
		map[px1][py1] = 2; map[px2][py2] = 2;
	}
}
void checkMove(int t, int x, int y) {
	//블록이 1개인경우
	if (t == 1) {
		moveBolck(x, y, -1, -1);
	}
	//블록이 2개인경우
	else {
		if (t == 2) {
			moveBolck(x, y, x, y+1);
		}
		else {
			moveBolck(x, y, x+1, y);
		}
	}
}
void eraseMap(int type, int start, int end, int count) {
	//지워야 할 행
	if (type == 0) {
		for (int a = start; a >= end; a--) {
			for (int b = 0; b < 4; b++) {
				map[a][b] = map[a - count][b];
			}
		}
	}
	//지워야 할 열
	else {
		for (int b = start; b >= end; b--) {
			for (int a = 0; a < 4; a++) {
				map[a][b] = map[a][b-count];
			}
		}
	}
}
bool eraseBlock(int type) {
	bool tans = false;
	//행검사
	bool check;
	if (type == 0) {
		for (int a = 6; a < SIZE; a++) {
			check = true;
			for (int b = 0; b < 4; b++) {
				if (map[a][b] == 0) {
					check = false;
					break;
				}
			}
			if (check) {
				tans = true;
				ans++;
				//지우고 위에칸 땡겨오고, 점수 1점 추가해야함.
				eraseMap(0, a, 5, 1);
			}
		}
	}
	//열검사
	else {
		for (int b = 6; b < SIZE; b++) {
			check = true;
			for (int a = 0; a < 4; a++) {
				if (map[a][b] == 0) {
					check = false;
					break;
				}
			}
			if (check) {
				tans = true;
				ans++;
				eraseMap(1, b, 5, 1);
			}
		}
	}
	return tans;
}
void checkErase() {
	bool ck1, ck2;
	while (true) {
		//행 검사
		ck1 = eraseBlock(0);
		//열 검사
		ck2 = eraseBlock(1);

		if (!ck1 && !ck2) break;
	}
}
int countSpecialSpace(int type) {
	int count = 0;
	//행 검사
	if (type == 0) {
		for (int a = 4; a <= 5; a++) {
			for (int b = 0; b < 4; b++) {
				if (map[a][b] == 0) continue;
				count++; break;
			}
		}
	}
	//열 검사
	else {
		for (int b = 4; b <= 5; b++) {
			for (int a = 0; a < 4; a++) {
				if (map[a][b] == 0) continue;
				count++; break;
			}
		}
	}
	return count;
}
void checkSpecialSpace() {
	int cnt;
	cnt = countSpecialSpace(0);
	if (cnt != 0) {
		eraseMap(0, SIZE - 1, 6, cnt);
		for (int a = 4; a <= 5; a++) {
			for (int b = 0; b < 4; b++) {
				map[a][b] = 0;
			}
		}
	}
	cnt = countSpecialSpace(1);
	if (cnt != 0) {
		eraseMap(1, SIZE - 1, 6, cnt);
		for (int a = 4; a <= 5; a++) {
			for (int b = 0; b < 4; b++) {
				map[b][a] = 0;
			}
		}
	}
}
int checkAns() {
	int count = 0;
	for (int a = 6; a < SIZE; a++) {
		for (int b = 0; b < 4; b++) {
			if (map[a][b] == 0) continue;
			count++;
		}
	}
	for (int b = 6; b < SIZE; b++) {
		for (int a = 0; a < 4; a++) {
			if (map[a][b] == 0) continue;
			count++;
		}
	}
	return count;
}
void Input() {
	int x, y, t,n;
	cin >> n;
	for (int a = 0; a < n; a++) {
		cin >> t >> x >> y;
		checkMove(t, x, y);
		//print();
		checkErase();
		//print();
		checkSpecialSpace();
		//print();
	}
}
void Solve() {
	Init();
	Input();
	cout << ans << endl;
	cout << checkAns();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();

}