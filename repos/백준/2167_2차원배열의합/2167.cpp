#include<iostream>
#define SIZE 300
int arr[SIZE][SIZE] = { 0, };

using namespace std;
int N, M;
void Solve(int i, int j, int x, int y) {
	int sum = 0;
	for (int a = i; a <= x; a++) {
		for (int b = j; b <= y; b++) {
			sum += arr[a][b];
		}
	}
	cout << sum;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	int i, j, x, y,t;
	cin >> N>>M;
	int temp;
	for (int a = 0; a<N; a++) {
		for (int b = 0; b < M; b++) {
			cin >> temp;
			arr[a][b] = temp;
		}
	}
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> i >> j >> x >> y;
		Solve(i - 1, j - 1, x - 1, y - 1);
		if (a == t - 1) break;
		cout << '\n';
	}
}