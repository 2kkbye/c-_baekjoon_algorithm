#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int L, P, V;
	int index = 1;
	while (true) {
		cin >> L >> P >> V;
		if (L == 0 && P == 0 && V == 0) break;
		int res = (V / P) * L;

		if ((V % P) >= L) {
			res += L;
		}
		else {
			res += (V % P);
		}
		cout <<"Case "<<index<<": "<< res<<endl;
		index++;
	}
}