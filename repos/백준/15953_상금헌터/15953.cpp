#include<iostream>

using namespace std;

int score_2017[6][3] = { {1,1,5000000},{2,3,3000000},
	{4,6,2000000},{7,10,500000},{11,15,300000},{16,21,100000} };

int score_2018[5][3] = { {1,1,5120000},{2,3,2560000},{4,7,1280000},
	{8,15,640000},{16,31,320000} };
int score_max_2017 = 21;
int score_max_2018 = 32;

int Solve(int index, int score) {
	if (index == 0) { //2017
		for (int a = 0; a < 6; a++) {
			if (score >= score_2017[a][0] && score <= score_2017[a][1]) {
				return score_2017[a][2];
			}
		}
	}
	else {//2018
		for (int a = 0; a < 6; a++) {
			if (score >= score_2018[a][0] && score <= score_2018[a][1]) {
				return score_2018[a][2];
			}
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t;
	cin >> t;
	int in_2017, in_2018,ans=0;
	for (int a = 0; a < t; a++) {
		cin >> in_2017 >> in_2018;
		ans = 0;
		if (in_2017 != 0 && in_2017 <= score_max_2017) {
			ans += Solve(0,in_2017);
		}
		if (in_2018 != 0 && in_2018 <= score_max_2018) {
			ans += Solve(1, in_2018);
		}
		cout << ans;
		if (a != t - 1) cout << endl;
	}
}