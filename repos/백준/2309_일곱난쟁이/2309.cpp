#include<iostream>
#define SIZE 9
#define RES 7
using namespace std;

bool check[SIZE] = { false, };
int height[SIZE] = { 0, };
int result[RES] = { 0, };
int sum = 0, ans = 0;
bool br = false;
void sort(int start,int end) {
	int s = start;
	int e = end;
	int pivot = result[(s + e) / 2];
	do {
		while (result[s] < pivot)s++;
		while (result[e] > pivot) e--;
		if (s <= e) {
			int temp = result[e];
			result[e] = result[s];
			result[s] = temp;
			e--; s++;
		}

	} while (s <= e);

	if (s < end) {
		sort(s, end);
	}
	if (e > start) {
		sort(start, e);
	}

}
void Solve(int index, int level) {
	if (br) return;
	if (level == 7) {
		if (sum == 100) {
			br = true;
			sort(0, 6);
			for (int a = 0; a < RES; a++) {
				cout << result[a];
				if (a != RES - 1) cout <<endl;
			}
			return;
		}
		return;
	}
	for (int a = index; a < SIZE; a++) {
		if (br) return;
		check[a] = true;
		sum += height[a];
		result[level] = height[a];
		Solve(a + 1, level+1);
		check[a] = false;
		sum -= height[a];
		result[level] = 0;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	for (int a = 0; a < SIZE; a++) {
		cin >> height[a];
	}
	for (int a = 0; a < 3; a++) {
		check[a] = true;
		sum += height[a];
		result[0] = height[a];
		Solve(a + 1, 1);
		check[a] = false;
		sum -= height[a];
		result[0] = 0;
	}
}