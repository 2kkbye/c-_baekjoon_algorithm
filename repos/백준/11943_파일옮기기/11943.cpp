#include<iostream>
#define SIZE 2
using namespace std;

int arr[SIZE][SIZE] = { 0, };
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			cin >> arr[a][b];
		}
	}
	int min;
	if ((arr[0][0] + arr[1][1]) < (arr[0][1] + arr[1][0])) {
		min = arr[0][0] + arr[1][1];
	}
	else {
		min = arr[0][1] + arr[1][0];
	}
	cout << min;
}