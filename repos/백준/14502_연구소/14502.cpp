#include<iostream>
#include<queue>
#include<vector>
#define SIZE 8

using namespace std;

int map[SIZE][SIZE];
int c_map[SIZE][SIZE];
int N, M,ans=0;
int dx[4] = {-1,0,1,0};
int dy[4] = {0,-1,0,1};

vector<pair<int, int>> v;
//queue<pair<int, int>> virus;
void print() {
	cout << endl;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			cout << c_map[a][b] << " ";
		}
		cout << endl;
	}
}
void copy() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			c_map[a][b] = map[a][b];
		}
	}
}
void result() {
	int sum = 0;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			if (c_map[a][b] == 1)continue;
			if (c_map[a][b] == 2) continue;
			sum++;
		}
	}
	if (sum > ans) ans = sum;
}
void Spread_V(int x, int y) {
	int px = x;
	int py = y;
	for (int a = 0; a < 4; a++) {
		int rx = px + dx[a];
		int ry = py + dy[a];
		if (rx < 0 || ry < 0 || rx >= N || ry >= M) continue;
		if (c_map[rx][ry] == 1 || c_map[rx][ry]==2) continue;
		c_map[rx][ry] = 2;
		Spread_V(rx, ry);
	}
}
void Virus() {
	int x, y;
	for (int a = 0; a < v.size(); a++) {
		x = v[a].first;
		y = v[a].second;
		Spread_V(x, y);
	}
	result();
}
void Defense(int x, int y,int level) {
	//cout << x << " " << y << endl;
	if (level == 3) {
		//if (map[1][0] == 1 && map[0][1] == 1 && map[3][5] == 1) {
			//cout << "cc";
		//}
		copy();
		//print();
		Virus();
		//print();
		return;
	}
	for (int a = x; a < N; a++) {
		for (int b = 0; b < M; b++) {
			//if (a == x && b<y+1) b = y + 1;
			if (map[a][b] == 1) continue;
			if (map[a][b] == 2) continue;
			map[a][b] = 1;
			Defense(a, b, level + 1);
			map[a][b] = 0;
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	cin >> N >> M;
	int temp;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			cin >> temp;
			map[a][b] = temp;
			if (temp == 2) {
				v.push_back(make_pair(a, b));
			}
		}
	}
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			if (map[a][b] == 1) continue;
			if (map[a][b] == 2) continue;
			map[a][b] = 1;
			Defense(a,b,1);
			map[a][b] = 0;
		}
	}
	cout << ans;

}