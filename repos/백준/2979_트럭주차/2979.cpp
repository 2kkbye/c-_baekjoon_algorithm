#include<iostream>
#define SIZE 101
using namespace std;

int arr[SIZE] = {0,};

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int A, B, C, s, e;
	cin >> A >> B >> C;
	for (int a = 0; a < 3; a++) {
		cin >> s >> e;
		for (int b = s; b < e; b++) {
			arr[b]++;
		}
	}
	int sum = 0;
	for (int a = 1; a < SIZE; a++) {
		if (arr[a] == 0) continue;
		if (arr[a] == 1) {
			sum += (arr[a] * A);
		}
		else if (arr[a] == 2) {
			sum += (arr[a] * B);
		}
		else {
			sum += (arr[a] * C);
		}
	}
	cout << sum;
}