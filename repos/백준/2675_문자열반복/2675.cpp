#include<iostream>

using namespace std;

void Solve(int n, string str) {
	for (int a = 0; a < str.length(); a++) {
		for (int b = 0; b < n; b++) {
			cout << str[a];
		}
	}
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t, n;
	string str;
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> n >> str;
		Solve(n, str);
		if (a == t - 1) break;
		cout << '\n';
	}
}