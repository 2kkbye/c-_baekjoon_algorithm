#include<iostream>
#define SIZE 1000
using namespace std;

int arr[SIZE];

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N;
	cin >> N;

	for (int a = 0; a<N; a++) {
		cin >> arr[a];
	}
	int temp;
	for (int a = 0; a < N - 1; a++) {
		for (int b = 0; b < N - 1 - a; b++) {
			if (arr[b] > arr[b + 1]) {
				temp = arr[b];
				arr[b] = arr[b + 1];
				arr[b + 1] = temp;
			}
		}
	}
	for (int a = 0; a < N; a++) {
		cout << arr[a];
		if (a == N - 1) break;
		cout << '\n';
	}
}