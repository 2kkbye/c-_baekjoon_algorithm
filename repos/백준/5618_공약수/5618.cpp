#include<iostream>
#define SIZE 3

using namespace std;

int num[SIZE];
void sort(int start, int end) {
	int s = start;
	int e = end;
	int pivot = num[(start + end) / 2];
	do {
		while (num[s] < pivot)s++;
		while (num[e] > pivot) e--;
		if (s <= e) {
			int temp = num[s];
			num[s] = num[e];
			num[e] = temp;
			s++; e--;
		}

	} while (s <= e);
	if (start < e) {
		sort(start, e);
	}
	if (s < end) {
		sort(s, end);
	}
}
void init() {
	for (int a = 0; a < SIZE; a++) {
		num[a] = 0;
	}
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int n;
	cin >> n;
	for(int a = 0;a<n;a++) {
		cin >> num[a];
	}
	bool check;
	for (int a = 1; a <= num[0]; a++) {
		check = true;
		for (int b = 0; b < n; b++) {
			if (num[b] % a != 0) check = false;
		}
		if (check) cout << a<<endl;
	}
}