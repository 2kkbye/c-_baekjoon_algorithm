#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t;
	cin >> t;
	int n;
	for (int a = 0; a < t; a++) {
		cin >> n;
		for (int b = 0; b < n; b++) {
			cout << "=";
		}
		if (a != t - 1) cout << endl;
	}
}