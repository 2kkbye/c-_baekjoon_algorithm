#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	int T,V,E,ans;
	cin >> T;
	for (int a = 0; a < T; a++) {
		cin >> V >> E;
		ans = 2 + E - V;
		cout << ans;
		if (a != T - 1) cout << endl;
	}

}