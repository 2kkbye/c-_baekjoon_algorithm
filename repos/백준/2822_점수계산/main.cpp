#include<iostream>
#include<vector>
#include<algorithm>
#define SIZE 8
using namespace std;

vector<int> v_main;
vector<int> v_copy;
vector<int> v_idx;
int findIdx(int num) {
	for (int a = 0; a < SIZE; a++) {
		if (v_copy[a] != num) continue;
		return a + 1;
	}
}
void Input() {
	int num;
	for (int a = 0; a < SIZE; a++) {
		cin >> num;
		v_main.push_back(num);
		v_copy.push_back(num);
	}
}
void Solve() {
	Input();
	sort(v_main.begin(), v_main.end());
	int sum = 0;
	for (int a = SIZE - 5; a < SIZE; a++) {
		sum += v_main[a];
		v_idx.push_back(findIdx(v_main[a]));
	}
	sort(v_idx.begin(), v_idx.end());
	cout << sum << endl;
	for (int a = 0; a < v_idx.size(); a++) {
		cout << v_idx[a];
		if (a != v_idx.size() - 1) {
			cout << " ";
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();
}