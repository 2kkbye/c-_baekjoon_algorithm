#include<iostream>
#define SIZE 500001
#include<cmath>
#include<vector>

using namespace std;

int num[SIZE];
int cnt[8001];
int N;
vector<int> v_check;
void Init() {
	for (int a = 0; a < SIZE; a++) {
		num[a] = 0;
		cnt[a] = 0;
	}
}
void quick(int start, int end) {
	int s = start;
	int e = end;
	int pivot = num[(start + end) / 2];
	do {
		while (num[s] < pivot) s++;
		while (num[e] > pivot) e--;
		if (s <= e) {
			int temp = num[s];
			num[s] = num[e];
			num[e] = temp;
			s++; e--;
		}
	} while (s <= e);
	if (s < end) {
		quick(s, end);
	}
	if (start < e) {
		quick(start, e);
	}
}
void Input() {
	cin >> N;
	for (int a = 0; a < N; a++) {
		cin >> num[a];
		if (num[a] <= 0) {
			cnt[abs(num[a])]++;
		}
		else {
			cnt[num[a] + 4000]++;
		}
	}
	num[N] = 4001;
}
double checkAverage() {
	double sum = 0;
	for (int a = 0; a < N; a++) {
		sum += num[a];
	}
	return sum/N;

}
int checkFrequency() {
	int cnt = 1;
	int maxCnt = 1;
	int tempCnt = 1;
	int maxNum = num[0];
	int tempNum = num[0];
	for (int a = 1; a <= N; a++) {
		if (num[a] == tempNum) {
			tempCnt++;
			continue;
		}
		else {
			if (tempCnt == maxCnt) {
				cnt++;
				if (cnt != 2) continue;
				if (num[a] != 4001) {
					maxNum = num[a];
				}
				else {
					maxNum = num[a - 1];
				}
			}
			if (tempCnt > maxCnt) {
				maxCnt = tempCnt;
				maxNum = tempNum;
				cnt = 1;
			}
			tempCnt = 1;
			tempNum = num[a];
		}
	}
	return maxNum;
}
int checkFrequency2() {
	int mode = 0; // 최빈 값
	int cntmode = 0;
	int modecnt = 1; // 2번째값 판별 cnt
	bool check = false; // 중복확인
	// 마이너스 
	for (int i = 4000; i >= 0; i--)
	{
		if (cntmode < cnt[i] && cnt[i] != 0)
		{
			mode = (i * -1);
			cntmode = cnt[i];
			check = false;
			modecnt = 1;
		}
		else if (cntmode == cnt[i] && cnt[i] != 0 && !check)
		{
			if (modecnt == 1)
			{
				check = true;
				mode = (i * -1);
				modecnt++;
			}
		}
	}

	// 플러스

	for (int i = 4001; i < 8001; i++)
	{
		if (cntmode < cnt[i])
		{

			mode = i - 4000;
			cntmode = cnt[i];
			check = false;
			modecnt = 1;
		}
		else if (cntmode == cnt[i] && cnt[i] != 0 && !check)
		{

			if (modecnt == 1)
			{
				check = true;
				mode = i - 4000;
				modecnt++;
			}
		}
	}
	return mode;
}
void Solve() {
	Init();
	Input();
	quick(0, N - 1);
	double avg = checkAverage();
	cout << (int)round(avg)<<endl;
	cout << num[N / 2] << endl;
	cout << checkFrequency2() << endl;
	cout << num[N - 1] - num[0];
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();

}