#include<iostream>
using namespace std;

int sum10(int num) {
	int did_n = 10, n = num,sum=0,temp;
	
	while (n > did_n) {
		temp = n % did_n;
		sum += temp;
		n /= did_n;
	}
	return sum+n;
}
int sum16(int num) {
	int did_n = 16, n = num, sum = 0, temp;

	while (n > did_n) {
		temp = n % did_n;
		sum += temp;
		n /= did_n;
	}
	return sum + n;
}
int sum12(int num) {
	int did_n = 12, n = num, sum = 0, temp;

	while (n > did_n) {
		temp = n % did_n;
		sum += temp;
		n /= did_n;
	}
	return sum + n;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	for (int a = 2992; a <= 9999; a++) {
		if ((sum10(a) == sum12(a)) && (sum16(a)== sum10(a))) {
			cout << a<<'\n';
		}
	}
}