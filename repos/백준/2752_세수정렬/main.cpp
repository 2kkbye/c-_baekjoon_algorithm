#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

vector<int> num;
void Input() {
	int n;
	for (int a = 0; a < 3; a++) {
		cin >> n;
		num.push_back(n);
	}
}
void Solve() {
	Input();
	sort(num.begin(), num.end());
	for (int a = 0; a < num.size(); a++) {
		cout << num[a];
		if (a != num.size() - 1) cout << " ";
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}