#include<iostream>
#define SIZE 2000001

using namespace std;

int num[SIZE];
int c;
void init() {
	for (int a = 0; a < SIZE; a++) {
		num[a] = 0;
	}
}
void check(int nu) {
	for (int a = nu; a <= c; a += nu) {
		num[a]++;
	}
}
int check_ans() {
	int count = 0;
	for (int a = 1; a <= c; a++) {
		if (num[a] == 0) continue;
		count++;
	}
	return count;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int n, ans = 0,temp;
	cin >> n >> c;

	for (int a = 0; a < n; a++) {
		cin >> temp;
		check(temp);
	}
	cout << check_ans();
}