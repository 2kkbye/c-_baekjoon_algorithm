#include<iostream>

using namespace std;

int Solve(int num, int sum) {
	for (int a = num; a < 101; a++) {
		if (a * a == sum) return a;
		if (a * a > sum) return a - 1;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N, W, H;

	cin >> N >> W >> H;
	int he;
	if (W > H) {
		he = Solve(W, (W * W) + (H * H));
	}
	else {
		he = Solve(H, (W * W) + (H * H));
	}
	int tmp;
	for (int a = 0; a < N; a++) {
		cin >> tmp;
		if (tmp <= he || tmp <= W || tmp <= H) {
			cout << "DA";
		}
		else {
			cout << "NE";
		}
		if (a != N - 1) cout << endl;
	}

}