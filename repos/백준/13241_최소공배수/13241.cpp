#include<iostream>

using namespace std;
int gcd(long long x, long long y) {
	int ans = 0;
	while (true) {
		if (x % y == 0) { ans = y; break; }
		int temp = x % y;
		x = y;
		y = temp;
	}
	return ans;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	long long x, y;	
	cin >> x >> y;
	long long max_num;
	if (x >= y) {
		max_num = gcd(x, y);
	}
	else {
		max_num = gcd(y, x);
	}
	long long ans = max_num * (x / max_num) * (y / max_num);
	cout << ans;
}