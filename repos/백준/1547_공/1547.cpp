#include<iostream>

using namespace std;


int arr[4] = {0,1,2,3};

int find_index(int num) {
	for (int a = 1; a < 4; a++) {
		if (arr[a] == num) return a;
	}
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int M, num1, num2, temp, temp1,temp2;

	cin >> M;
	for (int a = 0; a < M; a++) {
		cin >> num1 >> num2;
		temp1 = find_index(num1);
		temp2 = find_index(num2);
		temp = arr[temp1];
		arr[temp1] = arr[temp2];
		arr[temp2] = temp;
	}
	cout << arr[1];
}