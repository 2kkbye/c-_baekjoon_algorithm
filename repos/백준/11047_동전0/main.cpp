#include<iostream>
#include<vector>

using namespace std;
int num;
vector<int> v_coin;
int ans = 0;
void Input() {
	int n;
	cin >> n >> num;
	int temp;
	for (int a = 0; a < n; a++) {
		cin >> temp;
		v_coin.push_back(temp);
	}
}
void Solve() {
	Input();
	for (int a = v_coin.size() - 1; a >= 0; a--) {
		if (num == 0) break;
		ans += num / v_coin[a];
		num %= v_coin[a];
	}
	cout << ans;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}