#include<iostream>

using namespace std;

string day[] = { "SUN","MON","TUE","WED","THU","FRI","SAT" };
int number[] = {0,31,28,31,30,31,30,31,31,30,31,30,31 };

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int m, d,sum = 0;
	cin >> m;
	cin >> d;
	
	for (int a = 1; a < m; a++) {
		sum += number[a];
	}
	sum += d;
	cout << day[sum%7];
}