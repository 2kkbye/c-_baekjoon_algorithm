#include<iostream>
#define SIZE 1001
#define NUMBER 101
using namespace std;

int prime_number[SIZE] = {0,};
int num[NUMBER];
int N;
void Solve(int n) {
	int temp = n;
	int c = 2;
	while (temp*c < SIZE) {
		prime_number[temp * c] = 2;
		c++;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cin >> N;
	for (int a = 0; a < N; a++) {
		cin >> num[a];
	}
	for (int a = 2; a < SIZE; a++) {
		if (prime_number[a] == 2) continue;
		prime_number[a] = 1;
		Solve(a);
	}
	int cnt = 0;
	for (int a = 0; a < N; a++) {
		if (prime_number[num[a]] == 1) cnt++;
	}
	cout << cnt;
	
}