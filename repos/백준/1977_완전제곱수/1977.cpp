#include<iostream>
#define SIZE 101
using namespace std;

int arr[SIZE];
int N,M;
void init() {
	for (int a = 1; a < SIZE; a++) {
		arr[a] = a * a;
	}
}
void Solve() {
	int min = -1, sum = 0;
	for (int a = 1; a < SIZE; a++) {
		if (arr[a] < M) continue;
		if (arr[a] > N) break;
		if (min == -1) {
			min = arr[a];
		}
		sum += arr[a];
	}
	if (sum == 0) { cout << -1; return; }
	cout << sum<<'\n';
	cout << min;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	init();
	cin >> M;
	cin >> N;
	Solve();
}