#include<iostream>

using namespace std;
#define SIZE 8

int arr[SIZE];
int Solve() {
	int temp = 0;
	if (arr[0] == 1) {
		temp = 0;
		for (int a = 0; a<SIZE; a++) {
			if ((a + 1) == arr[a]) continue;
			temp = 2; break;
		}
	}
	else if (arr[0] == 8) {
		temp = 1;
		for (int a = 0; a < SIZE; a++) {
			if ((SIZE-a) == arr[a]) continue;
			temp = 2; break;
		}
	}
	else {
		temp = 2;
	}
	return temp;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	for (int a = 0; a < SIZE; a++) {
		cin >> arr[a];
	}
	int res = Solve();
	if (res == 0) cout << "ascending";
	else if (res == 1) cout << "descending";
	else cout << "mixed";
}