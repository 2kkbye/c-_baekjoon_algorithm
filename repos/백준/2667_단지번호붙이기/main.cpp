#include<iostream>
#include<vector>
#include<algorithm>
#define SIZE 25

using namespace std;

vector<int> ans;
int map[SIZE][SIZE];
bool visit[SIZE][SIZE];
int tans = 0;
int N;
int dx[4] = {0,1,0,-1};
int dy[4] = {1,0,-1,0};
void Init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
			visit[a][b] = false;
		}
	}
}
void Input() {
	cin >> N;
	for (int a = 0; a < N; a++) {
		string str;
		cin >> str;
		for (int b = 0; b < N; b++) {
			map[a][b] = ((int)str[b] - 48);
		}
	}
}
void dfs(int x, int y) {
	tans++;
	for (int a = 0; a < 4; a++) {
		int rx = x + dx[a];
		int ry = y + dy[a];
		if (rx < 0 || ry < 0 || rx >= N || ry >= N) continue;
		if (map[rx][ry] == 0) continue;
		if (visit[rx][ry]) continue;
		visit[rx][ry] = true;
		dfs(rx, ry);
	}
}
void Solution() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			if (map[a][b] == 0) continue;
			if (visit[a][b]) continue;
			tans = 0;
			visit[a][b] = true;
			dfs(a, b);
			ans.push_back(tans);
		}
	}
	sort(ans.begin(), ans.end());
	cout << ans.size() << endl;
	for (int a = 0; a < ans.size(); a++) {
		cout << ans[a] << endl;
	}
}
void Solve() {
	Init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	Solve();

}