#include<iostream>
#include<vector>

#define SIZE 20
#define MAX_SHARK 400 * 4

struct SHARK {
	int x, y, force, dir;
	bool die = false;
};
struct SMELL {
	int x, y, k;
};
using namespace std;

int sharkMap[SIZE][SIZE];
int sharkTempMap[SIZE][SIZE];

int noseMap[SIZE][SIZE];

int pDir[MAX_SHARK][5];
int startIndex[MAX_SHARK];

vector<SMELL> v_nose;
vector<SHARK> v_shark;
int dx[5] = {0,-1,1,0,0};
int dy[5] = {0,0,0,-1,1};

int N, M, K;

void quick(int start, int end) {
	int s = start;
	int e = end;
	int pivot = v_shark[(s + e) / 2].force;

	do {
		while (v_shark[s].force < pivot) s++;
		while(v_shark[e].force > pivot) e--;
		if (s <= e) {
			SHARK S = v_shark[s];
			v_shark[s] = v_shark[e];
			v_shark[e] = S;
			s++; e--;
		}
	} while (s <= e);
	if (s < end) {
		quick(s, end);
	}
	if (start < e) {
		quick(start, e);
	}

}
void Init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			sharkMap[a][b] = 0;
			noseMap[a][b] = 0;
		}
	}
	for (int a = 0; a < MAX_SHARK; a++) {
		startIndex[a] = 0;
		for (int b = 0; b < 5; b++) {
			pDir[a][b] = 0;
		}
	}
}
int findSharkIndex(int num) {
	for (int a = 0; a < v_shark.size(); a++) {
		if (v_shark[a].force == num) {
			return a;
		}
	}
	/////////////여기 추가
	return 0;
}
void tempSharkMapInit() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			sharkTempMap[a][b] = 0;
		}
	}
}
void Input() {
	cin >> N >> M >> K;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			int temp;
			cin >> temp;
			if (temp == 0) continue;
			sharkMap[a][b] = temp;
			noseMap[a][b] = temp;
			SMELL e;
			e.x = a; e.y = b; e.k = K;
			SHARK s;
			s.x = a; s.y = b; s.force = temp; s.dir = 0;
			v_shark.push_back(s);
			v_nose.push_back(e);
		}
	}
	int d;
	for (int a = 0; a < M; a++) {
		cin >> d;
		int index = findSharkIndex(a + 1);
		v_shark[index].dir = d;
	}
	for (int a = 0; a < M * 4; a++) {
		for (int b = 1; b < 5; b++) {
			cin >> d;
			pDir[a][b] = d;
		}
	}
	int t = 0;
	for (int a = 0; a < M; a++) {
		startIndex[a] = t;
		t += 4;
	}
}
void reduceSmell() {
	vector<int> index;
	for (int a = 0; a < v_nose.size(); a++) {
		v_nose[a].k--;
		if (v_nose[a].k == 0) {
			index.push_back(a);
		}
	}
	if (index.size() == 0) return;
	for (int a = index.size()-1; a >= 0; a--) {
		noseMap[v_nose[index[a]].x][v_nose[index[a]].y] = 0;
		v_nose.erase(v_nose.begin() + index[a], v_nose.begin() + index[a] + 1);
	}
}
int findSmell(int x, int y) {
	for (int a = 0; a < v_nose.size(); a++) {
		if (v_nose[a].x == x && v_nose[a].y == y) {
			return a;
		}
	}
	///////여기 추가
	return v_nose.size();
}
void addSmell() {
	for (int a = 0; a < v_shark.size(); a++) {
		int px = v_shark[a].x;
		int py = v_shark[a].y;
		noseMap[px][py] = v_shark[a].force;
		int index = findSmell(px, py);
		if (index == v_nose.size()) {
			SMELL s;
			s.x = px;
			s.y = py;
			s.k = K;
			v_nose.push_back(s);
		}
		else {
			v_nose[index].k = K;
		}
	}
}
void checkSpread(int index) {
	SHARK s = v_shark[index];
	int px = s.x;
	int py = s.y;
	int pdir = s.dir;
	//int start = (index * 4) + pdir -1;
	int start = ((s.force - 1) * 4) + pdir - 1;
	bool check = false;
	for (int a = 1; a < 5; a++) {
		int rx = px + dx[pDir[start][a]];
		int ry = py + dy[pDir[start][a]];
		if (rx < 0 || ry < 0 || rx >= N || ry >= N) continue;
		if (noseMap[rx][ry] != 0) continue;
		check = true;
		if (sharkTempMap[rx][ry] == 0) {
			v_shark[index].x = rx;
			v_shark[index].y = ry;
			sharkTempMap[rx][ry] = s.force;
			v_shark[index].dir = pDir[start][a];
		}
		else {
			//더 작은게 큰 상어다.
			int ti = findSharkIndex(sharkTempMap[rx][ry]);
			if (sharkTempMap[rx][ry] < s.force) {
				v_shark[index].die = true;
			}
			else {
				v_shark[ti].die = true;
				sharkTempMap[rx][ry] = s.force;
				v_shark[index].x = rx;
				v_shark[index].y = ry;
				v_shark[index].dir = pDir[start][a];
			}
		}
		break;
	}
	if (check) return;

	for (int a = 1; a < 5; a++) {
		int rx = s.x + dx[pDir[start][a]];
		int ry = s.y + dy[pDir[start][a]];
		if (rx < 0 || ry < 0 || rx >= N || ry >= N) continue;
		if (noseMap[rx][ry] != s.force) continue;
		v_shark[index].x = rx;
		v_shark[index].y = ry;
		sharkTempMap[rx][ry] = s.force;
		v_shark[index].dir = pDir[start][a];
		break;
	}
}
void deathShark() {
	vector<int> ti;
	for (int a = 0; a < v_shark.size(); a++) {
		if (!v_shark[a].die) continue;
		ti.push_back(a);
	}
	for (int a = ti.size() - 1; a >= 0; a--) {
		v_shark.erase(v_shark.begin() + ti[a], v_shark.begin() + ti[a] + 1);
	}
}
void copyMapToMap() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			sharkMap[a][b] = sharkTempMap[a][b];
		}
	}
}
void sharkMove() {
	tempSharkMapInit();
	for (int a = 0; a < v_shark.size(); a++) {
		checkSpread(a);
	}
	copyMapToMap();
	deathShark();

}
void Solution() {
	quick(0, v_shark.size()-1);
	int ans = -1;
	for (int a = 1; a < 1001; a++) {
		sharkMove();
		reduceSmell();
		addSmell();
		if (v_shark.size() == 1) {
			ans = a;
			break;
		}

	}
	cout << ans;
}
void Solve() {
	Init();
	Input();
	Solution();
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}