#include<iostream>

using namespace std;

int arr[3];
bool check[100] = {false,};

bool check_res() {
	if (check[arr[1] - 1] && check[arr[1] + 1]) return true;
	return false;
}
bool check_l_r() {
	//왼쪽이 크면 true, 오른쪽이 크거나, 같으면 false
	if (arr[1] - arr[0] > arr[2] - arr[1]) return true;
	else return false;
}
void move(int cnt) {
	int temp;
	if (cnt == 0) {//왼쪽으로 이동
		check[arr[2]] = false;
		arr[2] = arr[0] + 1;
		check[arr[2]] = true;
		temp = arr[1];
		arr[1] = arr[2];
		arr[2] = temp;
	}
	else {
		check[arr[0]] = false;
		arr[0] = arr[2] - 1;
		check[arr[0]] = true;
		temp = arr[1];
		arr[1] = arr[0];
		arr[0] = temp;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int res = 0;
	int temp, cnt;
	for (int a = 0; a < 3; a++) {
		cin >> temp;
		arr[a] = temp;
		check[temp] = true;
	}
	
	while (!check_res()) {
		if (check_res() && res == 0) break;
		if (check_l_r()) {
			cnt = 0;
		}
		else {
			cnt = 1;
		}
		move(cnt);
		res++;	
	}
	cout << res;
}