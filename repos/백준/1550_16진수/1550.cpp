#include<iostream>

using namespace std;
string str;

int toTen(int num) {
	int res = 1;
	if (num == 0) return res;
	for (int a = 0; a < num; a++) {
		res *= 16;
	}
	return res;
}
int toNum(char c) {
	if (c == '1') { return 1; }
	else if (c == '0') { return 0; }
	else if (c == '2') { return 2; }
	else if (c == '3') { return 3; }
	else if (c == '4') { return 4; }
	else if (c == '5') { return 5; }
	else if (c == '6') { return 6; }
	else if (c == '7') { return 7; }
	else if (c == '8') { return 8; }
	else if (c == '9') { return 9; }
	else if (c == 'A') { return 10; }
	else if (c == 'B') { return 11; }
	else if (c == 'C') { return 12; }
	else if (c == 'D') { return 13; }
	else if (c == 'E') { return 14; }
	else if (c == 'F') { return 15; }
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	cin >> str;
	int sum = 0;
	for (int a = 0; a < str.size(); a++) {
		sum = sum + (toTen((str.size() - 1) - a) * toNum(str[a]));
	}
	cout << sum;

}