#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t,n;
	int num,max_price;
	string name,max_name;
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> n;
		max_price = -1;
		for (int b = 0; b < n; b++) {
			cin >> num;
			cin >> name;
			if (num < max_price) continue;
			max_price = num;
			max_name = name;
		}
		cout << max_name;
		if (a != t - 1) cout << endl;
	}
}