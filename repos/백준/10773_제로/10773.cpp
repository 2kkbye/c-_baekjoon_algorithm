#include<iostream>
#include<vector>

using namespace std;

vector<int> v;
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	int t,num;
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> num;
		if (num == 0) {
			v.pop_back();
			continue;
		}
		v.push_back(num);
	}
	int sum = 0;
	for (int a = 0; a < v.size(); a++) {
		sum += v[a];
	}
	cout << sum;
}