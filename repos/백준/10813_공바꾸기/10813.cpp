#include<iostream>
#define SIZE 101
using namespace std;

int arr[SIZE] = { 0, };
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N, M,i,j,temp;
	cin >> N >> M;

	for (int a = 1; a <= N; a++) {
		arr[a] = a;
	}
	for (int a = 0; a < M; a++) {
		cin >> i >> j;
		temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
	for (int a = 1; a <= N; a++) {
		cout << arr[a];
		if (a == N) break;
		cout << " ";
	}
}