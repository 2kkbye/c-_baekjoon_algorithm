#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	//long long b = 4000000000;
	long long N, M;
	cin >> N >> M;

	if (N > M) {
		long long temp = N;
		N = M;
		M = temp;
	}
	cout << M - N;
}