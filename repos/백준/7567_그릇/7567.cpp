#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string str;
	int res = 10;

	cin >> str;

	for (int a = 1; a < str.size(); a++) {
		if (str[a] == str[a - 1]) res += 5;
		else res += 10;
	}
	cout << res;
}