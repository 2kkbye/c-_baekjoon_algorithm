#include<iostream>
#include<vector>
#include<queue>
#define SIZE 21

struct CUSTOMER {
	int sx, sy, dx, dy;
	bool complete = false;
};
struct POINT {
	int x, y;
};

using namespace std;

vector<CUSTOMER> v_customer;

int tX, tY;
int map[SIZE][SIZE];
int copyMap[SIZE][SIZE];
bool visit[SIZE][SIZE];

int dx[4] = {0,1,0,-1};
int dy[4] = {1,0,-1,0};

int oil, N,M;
void Init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
		}
	}
}
void Input() {
	//map크기, 승객 수, oil의 양
	cin >> N >> M >> oil;
	for (int a = 1; a <= N; a++) {
		for (int b = 1; b <= N; b++) {
			cin >> map[a][b];
		}
	}
	//현재 택시의 위치
	cin >> tX >> tY;
	for (int a = 0; a < M; a++) {
		CUSTOMER c;
		cin >> c.sx >> c.sy >> c.dx >> c.dy;
		v_customer.push_back(c);
	}
}
int findIndex(int x, int y) {
	for (int a = 0; a < v_customer.size(); a++) {
		if (v_customer[a].sx == x && v_customer[a].sy == y) {
			return a;
		}
	}
	return v_customer.size();
}
void initMap() {
	for (int a = 1; a <= N; a++) {
		for (int b = 1; b <= N; b++) {
			visit[a][b] = false;
			copyMap[a][b] = 0;
		}
	}
}
bool oilCheck() {
	int index = findIndex(tX, tY);
	if (oil < copyMap[v_customer[index].dx][v_customer[index].dy]) {
		return false;
	}
	if (copyMap[v_customer[index].dx][v_customer[index].dy] == 0) {
		return false;
	}
	v_customer[index].complete = true;
	tX = v_customer[index].dx; tY = v_customer[index].dy;
	oil -= copyMap[tX][tY];
	oil += (copyMap[tX][tY] * 2);
	return true;
}
bool findCustomerMap() {
	int x = 0, y = 0, dis = 1000000;
	for (int a = 0; a < v_customer.size(); a++) {
		if (v_customer[a].complete) continue;
		CUSTOMER c = v_customer[a];
		if (copyMap[c.sx][c.sy] < dis) {
			x = c.sx; y = c.sy;
			dis = copyMap[c.sx][c.sy];
		}
		else if (copyMap[c.sx][c.sy] == dis) {
			if (c.sx < x) {
				x = c.sx; y = c.sy;
				dis = copyMap[c.sx][c.sy];
			}
			else if (c.sx == x) {
				if (c.sy < y) {
					x = c.sx; y = c.sy;
					dis = copyMap[c.sx][c.sy];
				}
			}
		}
	}
	if (oil < copyMap[x][y]) {
		return false;
	}
	if (tX != x || tY != y) {
		if (copyMap[x][y] == 0) return false;
	}
	oil -= copyMap[x][y];
	tX = x; tY = y;
	return true;
}
void bfs() {
	initMap();
	queue<POINT> q_pair;
	visit[tX][tY] = true;
	POINT p;
	p.x = tX; p.y = tY;
	q_pair.push(p);

	while (!q_pair.empty()) {
		POINT p = q_pair.front();
		q_pair.pop();
		
		int px = p.x;
		int py = p.y;
		for (int a = 0; a < 4; a++) {
			int rx = px + dx[a];
			int ry = py + dy[a];

			if (rx < 1 || ry < 1 || rx > N || ry > N) continue;
			if (visit[rx][ry]) continue;
			if (map[rx][ry] == 1) continue;
			visit[rx][ry] = true;
			copyMap[rx][ry] = copyMap[px][py] + 1;
			POINT q;
			q.x = rx; q.y = ry;
			q_pair.push(q);
		}
	}
}
bool checkAns() {
	for (int a = 0; a < v_customer.size(); a++) {
		if (!v_customer[a].complete) return false;
	}
	return true;
}
void Solution() {
	bool check = true;
	for (int a = 0; a <= M; a++) {
		bfs();
		if (!findCustomerMap()) {
			check = false; break;
		}
		bfs();
		if (!oilCheck()) {
			check = false; break;
		}
		if (checkAns()) {
			break;
		}
	}
	if (!check) {
		cout << -1;
		return;
	}
	cout << oil;

}
void Solve() {
	Init();
	Input();
	Solution();
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}