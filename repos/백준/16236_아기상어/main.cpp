#include<iostream>
#include<vector>
#include<queue>
#define SIZE 20

struct Point {
	int x, y;
};
struct shark {
	int x, y, age, eat_fish, time;
};

struct fish {
	int x, y, dis;
};
using namespace std;

int map[SIZE][SIZE];
int copyMap[SIZE][SIZE];
bool visit[SIZE][SIZE];

int N;
vector<shark> v_shark;
vector<fish> v_fish;
int dx[4] = { 0,1,0,-1 };
int dy[4] = { 1,0,-1,0 };
void init() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			map[a][b] = 0;
		}
	}
}
void visitInit() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			visit[a][b] = false;
			copyMap[a][b] = 0;
		}
	}
}
void Input() {
	cin >> N;
	int temp;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			cin >> temp;
			if (temp != 9) {
				map[a][b] = temp;
				continue;
			}
			shark t; t.age = 2;
			t.x = a; t.y = b;
			t.eat_fish = 0; t.time = 0;
			v_shark.push_back(t);
		}
	}
}
void bfs() {
	visitInit();
	queue<Point> q_shark;
	Point p;
	p.x = v_shark[0].x;
	p.y = v_shark[0].y;
	visit[p.x][p.y] = true;
	q_shark.push(p);
	while (q_shark.size() != 0) {
		int px = q_shark.front().x;
		int py = q_shark.front().y;
		q_shark.pop();
		for (int a = 0; a < 4; a++) {
			int rx = px + dx[a];
			int ry = py + dy[a];

			if (rx >= 0 && ry >= 0 && rx < N && ry < N) {
				if (visit[rx][ry]) continue;
				if (map[rx][ry] > v_shark[0].age) continue;
				copyMap[rx][ry] = copyMap[px][py] + 1;
				visit[rx][ry] = true;
				Point p; p.x = rx; p.y = ry;
				q_shark.push(p);
				if (map[rx][ry] == 0) continue;
				if (map[rx][ry] < v_shark[0].age) {
					fish f;	f.x = rx; f.y = ry;
					f.dis = copyMap[rx][ry];
					if (v_fish.size() == 0) {
						v_fish.push_back(f);
						continue;
					}
					if (v_fish[0].dis < f.dis) continue;
					if (v_fish[0].dis > f.dis) {
						v_fish.clear();
						v_fish.push_back(f);
						continue;
					}
					if (v_fish[0].x < f.x) continue;
					if (v_fish[0].x > f.x) {
						v_fish.clear();
						v_fish.push_back(f);
						continue;
					}
					if (v_fish[0].y < f.y) continue;
					if (v_fish[0].y > f.y) {
						v_fish.clear();
						v_fish.push_back(f);
						continue;
					}
				}
			}
		}
	}
}
void eatFish() {
	fish f = v_fish[0];
	v_fish.clear();
	v_shark[0].x = f.x;
	v_shark[0].y = f.y;
	v_shark[0].eat_fish++;
	v_shark[0].time += f.dis;
	if (v_shark[0].eat_fish == v_shark[0].age) {
		v_shark[0].age++;
		v_shark[0].eat_fish = 0;
	}
	map[f.x][f.y] = 0;
}
void Solution() {
	while (true) {
		bfs();
		if (v_fish.size() == 0) {
			break;
		}
		eatFish();
	}
	cout << v_shark[0].time;
}
void Solve() {
	init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();
}