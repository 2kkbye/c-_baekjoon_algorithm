#include<iostream>
#include<string>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	string str;
	int cnt = 0;
	getline(cin, str);
	//cin >> str;
	bool b = false;
	for (int a = 0; a < str.length(); a++) {
		if (str[a] == ' ' && !b) {
			continue;
		}
		if (str[a] != ' ' && b) {
			continue;
		}
		if (str[a] == ' ' && b) {
			b = false;
			continue;
		}
		if (str[a] != ' ' && !b) {
			b = true;
			cnt++;
		}
	}
	cout << cnt;

}