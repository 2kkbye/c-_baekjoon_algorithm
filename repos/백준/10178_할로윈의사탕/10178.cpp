#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int c, v,t;

	cin >> t;

	for (int a = 0; a < t; a++) {
		cin >> c >> v;
		cout << "You get " << c / v << " piece(s) and your dad gets " << c % v << " piece(s).";
		if (a != t - 1) cout << endl;
	}
}