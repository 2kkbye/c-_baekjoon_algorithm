#include<iostream>
using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string num1, num2;
	char op;
	cin >> num1 >> op >> num2;
	if (op == '*') {
		cout << "1";
		for (int a = 0; a < (num1.length() + num2.length() - 2); a++) {
			cout << "0";
		}
	}
	else {
		if (num1.length() == num2.length()) {
			cout << "2";
			for (int a = 0; a <num1.length()-1; a++) {
				cout << "0";
			}
		}
		else {
			cout << "1";
			if (num1.length() > num2.length()) {
				for (int a = 0; a < num1.length() - 1 - num2.length(); a++) {
					cout << "0";
				}
				cout << num2;
			}
			else {
				for (int a = 0; a < num2.length() - 1 - num1.length(); a++) {
					cout << "0";
				}
				cout << num1;
			}
		}
	}
}