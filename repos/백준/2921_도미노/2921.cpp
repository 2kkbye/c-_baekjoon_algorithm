#include<iostream>
#define SIZE 1001

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int num[SIZE];
	num[0] = 0;
	for (int a = 1; a < SIZE; a++) {
		num[a] = num[a - 1] + a;
	}
	int sum = 0, set;
	cin >> set;
	for (int a = 1; a <= set; a++) {
		int temp = 0;
		for (int b = 0; b < a + 1; b++) {
			temp += a;
		}
		sum += temp;
		sum += num[a];
	}
	cout << sum;
}