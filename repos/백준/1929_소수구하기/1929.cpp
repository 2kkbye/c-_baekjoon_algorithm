#include<iostream>
#define SIZE 1000001

using namespace std;

bool prime_number[SIZE];
int N, M;
void Solve() {
	for (int a = 0; a < SIZE; a++) {
		prime_number[a] = true;
	}
	prime_number[1] = false;
	for (int a = 2; a <= M; a++) {
		if (!prime_number[a]) continue;
		int temp = a;
		int c = 2;

		while (temp * c <= M) {
			prime_number[temp * c] = false;
			c++;
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	cin >> N >> M;
	Solve();
	for (int a = N; a <= M; a++) {
		if (prime_number[a])
			printf("%d\n", a);
	}
}