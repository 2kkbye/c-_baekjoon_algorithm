#include<iostream>
#define SIZE 9

using namespace std;

int people[SIZE];
void Init() {
	for (int a = 0; a < SIZE; a++) {
		people[a] = 0;
	}
}
void Input() {
	for (int a = 0; a < SIZE; a++) {
		cin >> people[a];
	}
}
bool checkPeople(int idx1, int idx2) {
	int sum = 0;
	for (int a = 0; a < SIZE; a++) {
		if (a == idx1 || a == idx2) continue;
		sum += people[a];
	}
	if(sum == 100){
		for (int a = 0; a < SIZE; a++) {
			if (a == idx1 || a == idx2) continue;
			cout << people[a] << endl;
		}
		return true;
	}
	else {
		return false;
	}
}
void Solution() {
	for (int a = 0; a < SIZE - 1; a++) {
		for (int b = a + 1; b < SIZE; b++) {
			if (checkPeople(a, b)) break;
		}
	}
}
void Solve() {
	Init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();

}