#include<iostream>
#include<string>
#define SIZE 30

using namespace std;
int N;
string strList[SIZE];

void Input() {
	cin >> N;
	cin.ignore();
	for (int a = 0; a < N; a++) {
		getline(cin, strList[a]);
	}
}
void Solve() {
	Input();
	for (int a = 0; a < N; a++) {
		if (strList[a][0] >= 'a' && strList[a][0] <= 'z') {
			char temp = strList[a][0];
			strList[a][0] = (char)((int)(temp)-32);
		}
		for (int b = 1; b < strList[a].length(); b++) {
			if (strList[a][b] != ' ' && strList[a][b - 1] == ' ') {
				if (strList[a][0] >= 'a' && strList[a][0] <= 'z') {
					char temp = strList[a][b];
					strList[a][b] = (char)((int)(temp)-32);
				}
			}
		}
	}
	for (int a = 0; a < N; a++) {
		cout << strList[a];
		if (a != N - 1) cout << endl;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	//소문자 97, 대문자 65
	Solve();
}