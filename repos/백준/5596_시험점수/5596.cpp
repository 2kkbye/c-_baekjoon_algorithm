#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int num, sum1=0,sum2 = 0;
	for (int a = 0; a < 2; a++) {
		for (int b = 0; b < 4; b++) {
			cin >> num;
			if (a == 0) {
				sum1 += num;
				continue;
			}
			sum2 += num;
		}
	}
	if (sum1 > sum2) {
		cout << sum1;
	}
	else {
		cout << sum2;
	}
}