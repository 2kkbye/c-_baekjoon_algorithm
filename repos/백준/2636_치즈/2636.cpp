#include<iostream>
#define SIZE 100
#define DIRSIZE 4
using namespace std;

int n, m;
int map[SIZE][SIZE];
int copyMap[SIZE][SIZE];
bool visit[SIZE][SIZE];
bool copyVisit[SIZE][SIZE];
int dx[] = { 0,1,0,-1 };
int dy[] = { 1,0,-1,0 };
void init() {
	for (int a = 0; a < n; a++) {
		for (int b = 0; b < m; b++) {
			copyMap[a][b] = 0;
			map[a][b] = 0;
			visit[a][b] = false;
			copyVisit[a][b] = false;
		}
	}
}
void dfs(int a, int b) {
	int px = a;
	int py = b;
	for (int a = 0; a < DIRSIZE; a++) {
		int rx = px + dx[a];
		int ry = py + dy[a];
		if (rx >= 0 && rx < n && ry >= 0 && ry < m) {
			if (copyMap[rx][ry] == 1) continue;
			if (visit[rx][ry]) continue;
			visit[rx][ry] = true;
			dfs(rx, ry);
		}
	}
}
bool checkCopyMap() {
	for (int a = 0; a < n; a++) {
		for (int b = 0; b < m; b++) {
			if (copyMap[a][b] == 0) continue;
			return false;
		}
	}
	return true;
}
int countMap() {
	int cnt = 0;
	for (int a = 0; a < n; a++) {
		for (int b = 0; b < m; b++) {
			if (map[a][b] == 0)continue;
			cnt++;
		}
	}
	return cnt;
}
void moveMap() {
	for (int a = 0; a < n; a++) {
		for (int b = 0; b < m; b++) {
			map[a][b] = copyMap[a][b];
			copyMap[a][b] = 0;
			if (!visit[a][b] && copyVisit[a][b]) {
				visit[a][b] = copyVisit[a][b];
			}
		}
	}
}
bool checkSpread(int a, int b) {
	int px = a;
	int py = b;
	for (int a = 0; a < DIRSIZE; a++) {
		int rx = px + dx[a];
		int ry = py + dy[a];
		if (rx >= 0 && rx < n && ry >= 0 && ry < m) {
			if (map[rx][ry] == 1) continue;
			if (!visit[rx][ry]) continue;
			return true;
		}
	}
	return false;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cin >> n >> m;
	init();
	for (int a = 0; a < n; a++) {
		for (int b = 0; b < m; b++) {
			cin >> copyMap[a][b];
		}
	}
	for (int a = 0; a < n; a++) {
		for (int b = 0; b < m; b++) {
			if (copyMap[a][b] == 1) continue;
			if (a == 0 || a == n - 1 || b == 0 || b == m - 1) {
				if (visit[a][b]) continue;
				visit[a][b] = true;
				dfs(a, b);
			}
		}
	}
	int year = 0;
	while (true) {
		if (checkCopyMap()) {
			cout << year << '\n';
			cout << countMap();
			break;
		}
		year++;
		moveMap();
		for (int a = 0; a < n; a++) {
			for (int b = 0; b < m; b++) {
				if (map[a][b] == 0 && visit[a][b]) continue;
				bool type = false;
				if (!checkSpread(a,b)) {
					type = true;
					copyMap[a][b] = map[a][b];
				}
				if (!type) copyVisit[a][b] = true;
			}
		}
		cout << "b" << '\n';
	}
}