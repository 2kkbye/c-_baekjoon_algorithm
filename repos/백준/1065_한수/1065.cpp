#include<iostream>

using namespace std;
bool Solve(int num) {
	int num1 = -1, num2 = -1, temp;
	int dif = 10000;
	while (num > 0) {
		temp = num % 10;
		num /= 10;
		if (num1 == -1) {
			num1 = temp;
			continue;
		}
		num2 = temp;
		if (dif == 10000) {
			dif = num2 - num1;
			num1 = num2;
			num2 = -1;
			continue;
		}
		if (dif != num2 - num1) return false;
		num1 = num2;
		num2 = -1;
	}
	return true;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int num,ans=99;
	cin >> num;
	if (num > 110) {
		for (int a = 111; a <= num; a++) {
			if (Solve(a)) ans++;
		}
	}
	else if (num < 100) {
		cout << num;
		return 0;
	}
	cout << ans;
}