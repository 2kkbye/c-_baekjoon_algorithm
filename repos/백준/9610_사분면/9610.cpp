#include<iostream>
#define SIZE 5

using namespace std;
int arr[SIZE] = { 0, };
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int n,x,y,res=0;
	cin >> n;
	for (int a = 0; a < n; a++) {
		cin >> x >> y;
		if (x == 0 || y == 0) arr[0]++;
		if (x > 0 && y > 0) {
			arr[1]++;
			continue;
		}
		if (x > 0 && y < 0) {
			arr[4]++;
			continue;
		}
		if (x < 0 && y > 0) {
			arr[2]++;
			continue;
		}
		if (x < 0 && y < 0) {
			arr[3]++;
			continue;
		}
	}
	for (int a = 1; a < SIZE; a++) {
		cout << "Q" << a << ": " << arr[a];
		cout << '\n';
	}
	cout << "AXIS: " << arr[0];
}