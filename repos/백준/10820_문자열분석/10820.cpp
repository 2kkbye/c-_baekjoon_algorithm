#include<iostream>
#include<string>
#define SIZE 4

using namespace std;

//소문자, 대문자, 숫자, 공백
int arr[SIZE];
void init() {
    for (int a = 0; a < SIZE; a++) {
        arr[a] = 0;
    }
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string temp;

    while (true) {
        init();
        getline(cin, temp);
        if (temp == "")
            break;
        for (int a = 0; a < temp.length(); a++) {
            if (temp[a] >= 'a' && temp[a] <= 'z') {
                arr[0]++;
            }
            else if (temp[a] >= 'A' && temp[a] <= 'Z') {
                arr[1]++;
            }
            else if (temp[a] >= '0' && temp[a] <= '9') {
                arr[2]++;
            }
            else {
                arr[3]++;
            }
        }
        for (int a = 0; a<SIZE; a++) {
            cout << arr[a];
            if (a != SIZE - 1) {
                cout << " ";
            }
        }
        cout << '\n';
    }
}