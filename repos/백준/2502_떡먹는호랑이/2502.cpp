#include<iostream>
#define SIZE 31
using namespace std;

int numA[SIZE];
int numB[SIZE];
int day, K;
void init() {
	for (int a = 1; a < SIZE; a++) {
		numA[a] = 0;
		numB[a] = 0;
	}
	numA[1] = 1;
	numB[2] = 1;
	for (int a = 3; a <= day; a++) {
		numA[a] = numA[a - 1] + numA[a - 2];
		numB[a] = numB[a - 1] + numB[a - 2];
	}
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	cin >> day >> K;
	init();
	int ansA, ansB;
	bool check = false;
	int tempA = numA[day];
	int tempB = numB[day];
	for (int a = 1; a <= 50000; a++) {
		for (int b = a; b <= 50000; b++) {
			//printf("%d %d \n", a, b);
			if (K - (tempA * a) < (tempB * b)) break;
			if (K - (tempA * a) > (tempB * b)) continue;
			ansA = a, ansB = b;
			check = true;
			break;
		}
		if (check) break;
	}
	cout << ansA << endl << ansB;
}