#include<iostream>
#define SIZE 4000001

using namespace std;

bool prime[SIZE];
int N;
int ans,sum;
bool check;
void Solve() {
	for (int a = 2; a < SIZE; a++) {
		prime[a] = true;
	}
	prime[1] = false;
	int mul;
	for (int a = 2; a < SIZE; a++) {
		if (!prime[a]) continue;
		mul = 2;
		while (a * mul < SIZE) {
			prime[a * mul] = false;
			mul++;
		}
	}
}
void dfs(int index) {
	if (sum > N) {
		check = false;
		return;
	}
	if (sum == N) {
		ans++; check = false; return;
	}
	for (int a = index; a < N; a++) {
		if (!check) return;
		if (!prime[a]) continue;
		sum += a;
		dfs(a + 1);
		sum -= a;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cin >> N;
	Solve();
	ans = 0;
	sum = 0;
	
	if (prime[N]) ans = 1;
	for (int a = 1; a < N; a++) {
		if (!prime[a]) continue;
		check = true;
		sum += a;
		dfs(a + 1);
		sum -= a;
	}
	cout << ans;
}