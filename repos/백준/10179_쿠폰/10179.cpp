#include<iostream>
#include <iomanip>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t;
	cin >> t;
	double num;
	for (int a = 0; a < t; a++) {
		cin >> num;
		double res = num - (num * 0.2);
		//cout << res << endl;
		cout << fixed;
		cout.precision(2);
		cout << "$" << setiosflags(ios::showpoint) << res;
		if (a != t - 1) cout << '\n';
		//cout << num * 1.2;
	}
}