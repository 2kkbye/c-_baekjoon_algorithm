#include<iostream>
#define SIZE 500000
using namespace std;

int N;
int arr[SIZE] = { 0, };
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	cin >> N;
	for (int a = 0; a < N; a++) {
		cin >> arr[a];
	}
	int sum = 0;
	for (int a = N - 1; a >= 0; a--) {
		if (a != N - 1) sum += (arr[a] - 1);
		else { sum += arr[a]; }
	}
	cout << sum;
}