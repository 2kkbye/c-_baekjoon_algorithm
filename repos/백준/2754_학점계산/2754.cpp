#include<iostream>
#include<cmath>
using namespace std;

string grade[] = {"A+","A0","A-","B+","B0","B-" ,"C+","C0","C-" ,"D+","D0","D-" ,"F"};
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	float res = 4.3;
	string str;
	cin >> str;
	int cnt = 0;
	if (str == "F") { cout << "0.0"; return 0; }
	for (int a = 0; a < 13; a++) {
		cnt++;
		if (str == grade[a]) break;
		if (cnt%3==0) res -= 0.4;
		else res -= 0.3;
	}
	res = round(res * 100) / 100;
	cout << res;
	if (res == 3.0 || res == 4.0 || res == 2.0 || res == 1.0) {
		cout << ".0";
	}
}