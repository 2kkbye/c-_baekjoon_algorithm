#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string str1, str2;
	cin >> str1;
	cin >> str2;
	for (int a = 0; a < str1.size(); a++) {
		for (int b = 0; b < str2.size(); b++) {
			if (str1[a] != str2[b]) continue;
			str1[a] = '*'; str2[b] = '*';
			break;
		}
	}
	int cnt = 0;
	for (int a = 0; a < str1.size(); a++) {
		if (str1[a] != '*') cnt++;
	}
	for (int a = 0; a < str2.size(); a++) {
		if (str2[a] != '*') cnt++;
	}
	cout << cnt;
}