#include<iostream>

using namespace std;

int num2[4] = { 2,4,8,6 };
int num3[4] = { 3,9,7,1 };
int num7[4] = { 7,9,3,1 };
int num8[4] = { 8,4,2,6 };
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t;
	cin >> t;
	for (int a = 0; a < t; a++) {
		int x, y;
		cin >> x >> y;
		x = x % 10;
		if (x == 1 || x == 5 || x == 6) {
			cout << x;
		}
		else if (x == 0) {
			cout << "10";
		}
		else if (x == 4 || x == 9) {
			if (y % 2 == 0) {
				if (x == 4) {
					cout << 6;
				}
				else {
					cout << 1;
				}
			}
			else {
				if (x == 4) {
					cout << 4;
				}
				else {
					cout << 9;
				}
			}
		}
		else {
			int index = 1, index2 = 0;
			while (index < y) {
				index++;
				index2++;
				if (index2 == 4) index2 = 0;
			}
			if (x == 2) {
				cout << num2[index2];
			}
			else if (x == 3) {
				cout << num3[index2];
			}
			else if (x == 7) {
				cout << num7[index2];
			}
			else if (x == 8) {
				cout << num8[index2];
			}
		}
		if (a != t - 1) cout << endl;
	}
}