#include<iostream>
#define SIZE 51

using namespace std;
bool checkMap[SIZE][SIZE];
int map[SIZE][SIZE];

//로봇의 현재 위치, 방향
int rx, ry, rdir;

int dx[4] = {-1,0,1,0};
int dy[4] = {0,1,0,-1};
int N, M;

void init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
			checkMap[a][b] = false;
		}
	}
}
void Input() {
	cin >> N >> M;
	cin >> rx >> ry >> rdir;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			cin >> map[a][b];
		}
	}
}
int countAns() {
	int ans = 0;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			if (checkMap[a][b]) ans++;
		}
	}
	return ans;
}
void checkNow() {
	checkMap[rx][ry] = true;
}

bool checkSpread() {
	int tdir = rdir;
	for (int a = 0; a < 4; a++) {
		tdir--;
		if (tdir == -1) tdir = 3;

		int px = rx + dx[tdir];
		int py = ry + dy[tdir];
		if (px >= 0 && py >= 0 && px < N && py < M) {
			if (map[px][py] == 1 || checkMap[px][py]) continue;
			rx = px;
			ry = py;
			rdir = tdir;
			return true;
		}
	}
	return false;
}
bool backDir() {
	int px = rx;
	int py = ry;
	while (true) {
		if (rdir == 0) {
			px = px + dx[2];
			py = py + dy[2];
		}
		else if (rdir == 1) {
			px = px + dx[3];
			py = py + dy[3];
		}
		else if (rdir == 2) {
			px = px + dx[0];
			py = py + dy[0];
		}
		else {
			px = px + dx[1];
			py = py + dy[1];
		}
		if (px >= 0 && py >= 0 && px < N && py < M) {
			if (map[px][py] == 0) {
				rx = px;
				ry = py;
				return true;
			}
			if (map[px][py] == 1) {
				return false;
			}
		}
	}
	
}
void Solution() {
	while (true) {
		//cout << rx << " " << ry << endl;
		checkNow();
		if (!checkSpread()) {
			if (!backDir()) {
				cout << countAns();
				return;
			}
		}
	}
}
void Solve() {
	init();
	Input();
	Solution();
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();

}