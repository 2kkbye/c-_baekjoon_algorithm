#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int n;
	cin >> n;
	long long sum = 0;
	long long mi = 3;
	for (int a = 1; a <= n; a++) {
		sum += (5 * a);
		if (a == 1) continue;
		sum -= mi;
		mi += 2;
	}
	cout << sum % 45678;
}