#include<iostream>
#include<string>

using namespace std;

int arr[26];

void init() {
	for (int a = 0; a < 26; a++) {
		arr[a] = -1;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	init();
	string s;
	cin >> s;
	for (int a = 0; a < s.length(); a++) {
		if (arr[(int)s[a] - 97] != -1) continue;
		arr[(int)s[a] - 97] = a;
	}
	for (int a = 0; a < 26; a++) {
		cout << arr[a];
		if (a != 25) cout << " ";
	}

}