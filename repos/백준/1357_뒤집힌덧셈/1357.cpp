#include<iostream>

using namespace std;

int num[2];
int resNum[4];
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	string temp;
	for (int a = 0; a < 2; a++) {
		cin >> temp;
		int sum = 0;
		int mul = 1;
		for (int b = 0; b < temp.length(); b++) {
			if (b == 0) {
				sum = sum + ((int)temp[b] - 48);
				continue;
			}
			mul *= 10;
			sum += (mul * ((int)temp[b] - 48));
		}
		num[a] = sum;
	}
	int res = num[0] + num[1];
	//cout << res<<'\n';
	bool type = false;
	while (true) {
		int tn = res % 10;
		if (!type && tn == 0) {
			if (res / 10 == 0) break;
			res /= 10;
			continue;
		}
		type = true;
		cout << tn;
		if (res / 10 == 0) break;
		res /= 10;
	}

}