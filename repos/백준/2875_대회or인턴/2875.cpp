#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N, M, K;

	cin >> N >> M >> K;
	int ans = 0;

	for (int a = 0; a <= K; a++) {
		if (a > N) break;
		if (K - a > M) continue;
		int ans_w = (N - a) / 2;
		int ans_m = M - (K - a);
		if (ans_w > ans_m && ans_m>ans) {
			ans = ans_m;
		}
		else if(ans_w < ans_m && ans_w > ans) {
			ans = ans_w;
		}
		else if (ans_w == ans_m && ans_w > ans) {
			ans = ans_w;
		}
	}
	printf("%d", ans);
}