#include<iostream>
#define SIZE 1000

using namespace std;

int arr[SIZE];
void init() {
	for (int a = 0; a < SIZE; a++) {
		arr[a] = -1;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int num, index,sum;
	while (true) {
		cin >> num;
		if (num == -1)	break;
		init();
		index = 1;
		arr[0] = 1;
		sum = 1;
		for (int a = 2; a <= (num / 2)+1; a++) {
			if (num % a == 0) { arr[index] = a; index++; sum += a; }
		}
		if (sum != num) {
			cout << num << " is NOT perfect." << endl;
			continue;
		}
		cout << num << " = ";
		for (int a = 0; a < index; a++) {
			cout << arr[a];
			if (a != index - 1) cout << " + ";
		}
		cout << endl;
	}
}