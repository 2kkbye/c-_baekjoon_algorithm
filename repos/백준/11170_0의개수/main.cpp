#include<iostream>
#include<string>
using namespace std;
int t;
int ans = 0;
int checkStr(string str) {
	int count = 0;
	for (int a = 0; a < str.length(); a++) {
		if (str[a] == '0') count++;
	}
	return count;
}
void Solution(int start, int end) {
	for (int a = start; a <= end; a++) {
		ans += checkStr(to_string(a));
	}
}
void Solve() {
	cin >> t;
	int start, end;
	for (int a = 0; a < t; a++) {
		ans = 0;
		cin >> start >> end;
		Solution(start, end);
		cout << ans;
		if (a != t - 1) {
			cout << endl;
		}
	}
	
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}