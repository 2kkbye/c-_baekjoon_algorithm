#include<iostream>
#define SIZE 1000001

using namespace std;

long num[SIZE];
void init() {
	for (int a = 0; a < SIZE; a++) {
		num[a] = 0;
	}
}
void quick(int start, int end) {
	int s = start;
	int e = end;
	int pivot = num[(start + end) / 2];
	do {
		while (num[s]<pivot) s++;
		while (num[e]>pivot) e--;
		if (s <= e) {
			int temp = num[s];
			num[s] = num[e];
			num[e] = temp;
			s++; e--;
		}
	} while (s <= e);
	if (s < end) {
		quick(s, end);
	}
	if (e > start) {
		quick(start, e);
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	long N;
	cin >> N;
	init();
	for (int a = 0; a < N; a++) {
		cin >> num[a];
	}
	quick(0, N - 1);
	long ans = num[N - 1] * num[0];

	cout << ans;
}