#include<iostream>
#define SIZE 3

using namespace std;
int num[SIZE];
void quick(int start, int end) {
	int s = start;
	int e = end;
	int pivot = num[(s + e) / 2];

	do {
		while (num[s] < pivot) s++;
		while (num[e] > pivot) e--;
		if (s <= e) {
			int temp = num[s];
			num[s] = num[e];
			num[e] = temp;
			s++, e--;
		}

	} while (s <= e);
	if (s < end) {
		quick(s, end);
	}
	if (start < e) {
		quick(start, e);
	}
}
bool check() {
	int check1 = num[1] - num[0];
	if (check1 < 0) check1 *= -1;
	int check2 = num[2] - num[1];
	if (check2 < 0) check2 *= -1;
	if (check1 == check2) return true;
	if(check1<check2){
		cout << num[1] + check1;
	}
	else {
		cout << num[1] - check2;
	}
	return false;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	for (int a = 0; a < SIZE; a++) {
		cin >> num[a];
	}
	quick(0, 2);
	int result = 0;
	if (check()) {
		result = num[1] - num[0];
		if (result < 0) result *= -1;
		result += num[2];
		cout << result;
	}
}