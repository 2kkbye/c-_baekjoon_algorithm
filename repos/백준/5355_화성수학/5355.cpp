#include<iostream>
#include<string>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t;
	double num;
	string str;
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> num;
		getline(cin, str);
		for (int b = 0; b < str.size(); b++) {
			if (str[b] == ' ') continue;
			if (str[b] == '@') {
				num *= 3;
			}
			else if (str[b] == '%') {
				num += 5;
			}
			else {
				num -= 7;
			}
		}
		printf("%.2f\n", num);
	}
}