#include<iostream>
#define SIZE 101

using namespace std;

long arr[SIZE];
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	arr[1] = 1;
	arr[2] = 1;
	arr[3] = 1;
	for (int a = 4; a < SIZE; a++) {
		arr[a] = arr[a - 2] + arr[a - 3];
	}
	int t,n;
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> n;
		cout << arr[n];
		if (a != t - 1) cout << endl;
	}
}