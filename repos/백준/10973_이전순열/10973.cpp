#include<iostream>
#define SIZE 10001

using namespace std;

int ch_num[SIZE];
bool visit[SIZE];
int num[SIZE];
int N;
bool res_check = false;
bool check() {
	for (int a = 0; a < N; a++) {
		if (num[a] != ch_num[a]) return false;
	}
	return true;
}

void dfs(int index) {
	if (index == N) {
		if (res_check) {
			for (int a = 0; a < N; a++) {
				printf("%d", num[a]);
				if (a != N - 1) printf(" ");
			}
			exit(0);
		}
		if (check()) {
			res_check = true;
		}
		return;
	}
	for (int a = N; a >= 1; a--) {
		if (visit[a]) continue;
		if (ch_num[index] != a && !res_check) continue;
		visit[a] = true;
		num[index] = a;
		dfs(index+1);
		visit[a] = false;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	cin >> N;
	for (int a = 0; a < N; a++) {
		cin >> ch_num[a];
	}
	
	for (int a = ch_num[0]; a >= ch_num[0] - 1; a--) {
		if (a == 0) break;
		visit[a] = true;
		num[0] = a;
		dfs(1);
		visit[a] = false;
	}
	printf("-1");
}