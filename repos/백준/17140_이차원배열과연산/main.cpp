#include<iostream>
#include<vector>
#define SIZE 101

struct number {
	int n, count;
};

using namespace std;

int map[SIZE][SIZE];
vector<number> num;
int R, C, K;
//행의 개수
int row_num = 3;
//열의 개수
int col_num = 3;
void Init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
		}
	}
}
void Input() {
	cin >> R >> C >> K;
	for (int a = 1; a <= 3; a++) {
		for (int b = 1; b <= 3; b++) {
			cin >> map[a][b];
		}
	}
}
void Print() {
	cout << "===================" << endl;
	for (int a = 1; a <= row_num; a++) {
		for (int b = 1; b <= col_num; b++) {
			cout << map[a][b] << " ";
		}
		cout << endl;
	}
}
void quick_num(int start, int end) {
	int s = start;
	int e = end;
	int pivot = num[(s + e) / 2].n;
	do {
		while (num[s].n < pivot) s++;
		while (num[e].n > pivot)e--;
		if (s <= e) {
			number temp = num[s];
			num[s] = num[e];
			num[e] = temp;
			s++; e--;
		}
	} while (s <= e);
	if (s < end) {
		quick_num(s, end);
	}
	if (start < e) {
		quick_num(start, e);
	}
}
void quick_count(int start, int end) {
	int s = start;
	int e = end;
	int pivot = num[(s + e) / 2].count;
	do {
		while (num[s].count < pivot) s++;
		while (num[e].count > pivot)e--;
		if (s <= e) {
			number temp = num[s];
			num[s] = num[e];
			num[e] = temp;
			s++; e--;
		}
	} while (s <= e);
	if (s < end) {
		quick_count(s, end);
	}
	if (start < e) {
		quick_count(start, e);
	}
}
void Instruction(bool type) {
	//R
	if (type) {
		for (int a = 1; a <= row_num; a++) {
			num.clear();
			for (int b = 1; b <= col_num; b++) {
				if (map[a][b] == 0) continue;
				bool check = false;
				for (int c = 0; c < num.size(); c++) {
					if (num[c].n == map[a][b]) {
						num[c].count++;
						check = true;
						break;
					}
				}
				if (check) continue;
				number n;
				n.n = map[a][b];
				n.count = 1;
				num.push_back(n);
			}
			quick_count(0, num.size() - 1);
			int start = 0;
			for (int b = 0; b < num.size() - 1; b++) {
				if (num[b].count == num[b + 1].count) continue;
				if (b - start == 0) {
					start = b + 1; continue;
				}
				quick_num(start, b);
				start = b + 1;
			}
			if (start != num.size() - 1) {
				quick_num(start, num.size() - 1);
			}
			if (col_num < num.size() * 2) {
				col_num = num.size() * 2;
			}
			int index = 0;
			for (int b = 1; b <= col_num; b += 2) {
				if (index > 50) break;
				if (index < num.size()) {
					map[a][b] = num[index].n;
					map[a][b + 1] = num[index].count;
					index++;
					continue;
				}
				map[a][b] = 0;
				map[a][b + 1] = 0;
			}
		}
	}
	//C
	else {
		for (int b = 1; b <= col_num; b++) {
			num.clear();
			for (int a = 1; a <= row_num; a++) {
				if (map[a][b] == 0) continue;
				bool check = false;
				for (int c = 0; c < num.size(); c++) {
					if (num[c].n == map[a][b]) {
						num[c].count++;
						check = true;
						break;
					}
				}
				if (check) continue;
				number n;
				n.n = map[a][b];
				n.count = 1;
				num.push_back(n);
			}
			quick_count(0, num.size() - 1);
			int start = 0;
			for (int a = 0; a < num.size() - 1; a++) {
				if (num[a].count == num[a + 1].count) continue;
				if (a - start == 0) {
					start = a + 1;
					continue;
				}
				quick_num(start, a);
				start = a + 1;
			}
			if (start != num.size() - 1) {
				quick_num(start, num.size() - 1);
			}
			if (row_num < num.size() * 2) {
				row_num = num.size() * 2;
			}
			int index = 0;
			for (int a = 1; a <= row_num; a += 2) {
				if (index > 50) break;
				if (index < num.size()) {
					map[a][b] = num[index].n;
					map[a + 1][b] = num[index].count;
					index++;
					continue;
				}
				map[a][b] = 0;
				map[a + 1][b] = 0;
			}
		}
	}
}
void Solution() {
	int ans = -1;
	for (int a = 0; a <= 100; a++) {
		if (map[R][C] == K) {
			ans = a;
			break;
		}
		//R연산
		if (row_num >= col_num) {
			Instruction(true);
		}
		//COL연산
		else {
			Instruction(false);
		}
		//Print();
	}
	cout << ans;
}
void Solve() {
	Init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}