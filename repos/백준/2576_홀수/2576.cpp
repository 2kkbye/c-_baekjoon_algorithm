#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int min = 101;
	int sum = 0;

	int num;
	for (int a = 0; a < 7; a++) {
		cin >> num;
		if (num % 2 == 0) continue;
		if (num < min) min = num;
		sum += num;
	}
	if (sum == 0) { cout << -1; return 0; }
	cout << sum << '\n';
	cout << min;
}