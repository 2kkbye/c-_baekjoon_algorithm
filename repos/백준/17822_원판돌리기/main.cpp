#include<iostream>
#include<vector>

using namespace std;

vector<vector<int>> v_circle;

int N, M, T;
int dx[4] = {0,1,0,-1};
int dy[4] = {1,0,-1,0};

int checkAns() {
	int sum = 0;
	for (int a = 0; a < v_circle.size(); a++) {
		for (int b = 0; b < v_circle[a].size(); b++) {
			sum += v_circle[a][b];
		}
	}
	return sum;
}
void rotateCircle(int index, int type, int cnt) {
	//시계
	if (type == 0) {
		for (int a = 0; a < cnt; a++) {
			int num = v_circle[index][v_circle[index].size() - 1];
			v_circle[index].pop_back();
			v_circle[index].insert(v_circle[index].begin(), num);
		}
	}
	//반시계
	else {
		for (int a = 0; a < cnt; a++) {
			int num = v_circle[index][0];
			v_circle[index].erase(v_circle[index].begin(), v_circle[index].begin() + 1);
			v_circle[index].push_back(num);
		}
	}
}
bool checkSpread() {
	bool tcheck = false;
	vector<vector<int>> v_temp = v_circle;
	for (int a = 0; a < v_circle.size(); a++) {
		for (int b = 0; b < v_circle[a].size(); b++) {
			if (v_circle[a][b] == 0) continue;
			int tnum = v_circle[a][b];
			bool check = false;
			for (int c = 0; c < 4; c++) {
				if (a == 0 && c == 3) continue;
				if (a == v_circle.size() - 1 && c == 1) continue;
				int rx = a + dx[c];
				int ry = b + dy[c];
				if (ry < 0) {
					ry = M - 1;
				}
				if (ry == M) {
					ry = 0;
				}
				if (tnum != v_circle[rx][ry]) continue;
				tcheck = true;
				check = true;
				if (v_temp[rx][ry] == 0) continue;
				v_temp[rx][ry] = 0;
				
			}
			if (check) v_temp[a][b] = 0;
		}
	}
	v_circle = v_temp;
	return tcheck;
}
void rotateCheckCircle(int index, int type, int cnt) {
	cnt = cnt % M;
	for (int a = index; a <= N; a+=index) {
		//vector 접근할때는 a-1 해야함.
		rotateCircle(a - 1, type, cnt);
	}
	if (!checkSpread()) {
		int sum = 0;
		int cnt = 0;
		for (int a = 0; a < v_circle.size(); a++) {
			for (int b = 0; b < v_circle[a].size(); b++) {
				if (v_circle[a][b] == 0) continue;
				sum += v_circle[a][b];
				cnt++;
			}
		}
		double div = (double)sum / (double)cnt;
		for (int a = 0; a < v_circle.size(); a++) {
			for (int b = 0; b < v_circle[a].size(); b++) {
				if (v_circle[a][b] == 0) continue;
				if (v_circle[a][b] > div) {
					v_circle[a][b]--;
				}
				else if (v_circle[a][b] < div) {
					v_circle[a][b]++;
				}
				
			}
		}
	}
}

void Input() {
	cin >> N >> M >> T;
	int n;
	for (int a = 0; a < N; a++) {
		vector<int> temp;
		for (int b = 0; b < M; b++) {
			cin >> n;
			temp.push_back(n);
		}
		v_circle.push_back(temp);
	}
	int x, d, k;
	for (int a = 0; a < T; a++) {
		cin >> x >> d >> k;
		rotateCheckCircle(x, d, k);
	}
	cout << checkAns();
}
void Solve() {
	Input();
}
int main() {

	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}