#include<iostream>
#define SIZE 7
using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t,sum,min;
	cin >> t;
	int num;
	for (int a = 0; a < t; a++) {
		sum = 0; min = 101;
		for (int b = 0; b < SIZE; b++) {
			cin >> num;
			if (num % 2 == 1) continue;
			sum += num;
			if (num >= min) continue;
			min = num;
		}
		cout << sum << " " << min;
		if (a == t - 1) break;
		cout << '\n';
	}
}