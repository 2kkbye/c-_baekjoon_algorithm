#include<iostream>
#define SIZE 101

using namespace std;

int N, L;
int map[SIZE][SIZE];
bool visit[SIZE][SIZE];
void init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
			visit[a][b] = false;
		}
	}
}
void visitInit() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			visit[a][b] = false;
		}
	}
}
bool checkFront(int type, int x, int y) {
	//0이면 행검사
	int count = 0;
	if (type == 0) {
		for (int b = y - 1; b >= y-L; b--) {
			if (b < 0) break;
			if (map[x][y] - 1 == map[x][b] && !visit[x][b]) {
				count++;
				visit[x][b] = true;
			}
			else break;
		}
	}
	//1이면 열 검사
	else {
		for (int a = x - 1; a >= x - L; a--) {
			if (a < 0) break;
			if (map[x][y] - 1 == map[a][y] && !visit[a][y]) { 
				count++; 
				visit[a][y] = true;
			}
			else break;
		}
	}
	if (count == L) return true;
	else return false;
}
bool checkBack(int type, int x, int y) {
	//0이면 행검사
	int count = 0;
	if (type == 0) {
		for (int b = y; b < L + y; b++) {
			if (b >= N) break;
			if (map[x][y] == map[x][b] && !visit[x][b]) {
				count++;
				visit[x][b] = true;
			}
			else break;
		}
	}
	//1이면 열 검사
	else {
		for (int a = x; a < L + x; a++) {
			if (a >= N) break;
			if (map[x][y] == map[a][y] && !visit[a][y]) { 
				count++;
				visit[a][y] = true;
			}
			else break;
		}
	}
	if (count == L) return true;
	else return false;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	init();
	cin >> N >> L;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			cin >> map[a][b];
		}
	}
	int ans = 0;
	bool check;
	//행 검사
	for (int a = 0; a < N; a++) {
		check = true;
		for (int b = 1; b < N; b++) {
			if (map[a][b] == map[a][b - 1]) continue;
			//1보다 차이가 크면 break
			if (map[a][b] < map[a][b - 1] && (map[a][b - 1] - map[a][b]) > 1) {
				check = false; break;
			}
			if (map[a][b] > map[a][b - 1] && (map[a][b] - map[a][b-1]) > 1) {
				check = false; break;
			}
			//뒤가 더 큰 경우
			if (map[a][b] > map[a][b - 1]) {
				if (checkFront(0, a, b)) continue;
				check = false; break;
			}
			//앞이 더 큰 경우 뒤를 살펴봐야 한다.
			if (map[a][b] < map[a][b - 1]) {
				if (checkBack(0, a, b)) continue;
				check = false; break;
			}
		}
		if (check) ans++;
	}
	visitInit();
	//열 검사
	for (int b = 0; b < N; b++) {
		check = true;
		for (int a = 1; a < N; a++) {
			if (map[a][b] == map[a-1][b]) continue;
			//1보다 차이가 크면 break
			if (map[a][b] < map[a-1][b] && (map[a-1][b] - map[a][b]) > 1) {
				check = false; break;
			}
			if (map[a][b] > map[a-1][b] && (map[a][b] - map[a-1][b]) > 1) {
				check = false; break;
			}
			//뒤가 더 큰 경우
			if (map[a][b] > map[a-1][b]) {
				if (checkFront(1, a, b)) continue;
				check = false; break;
			}
			//앞이 더 큰 경우 뒤를 살펴봐야 한다.
			if (map[a][b] < map[a-1][b]) {
				if (checkBack(1, a, b)) continue;
				check = false; break;
			}
		}
		if (check) ans++;
	}
	cout << ans;
}