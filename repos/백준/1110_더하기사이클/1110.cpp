#include<iostream>
using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N, temp_num, CNT=0, sum_num;
	cin >> N;
	temp_num = N;
	while (true) {
		sum_num = (temp_num / 10) + (temp_num % 10);
		temp_num = ((temp_num % 10) * 10) + (sum_num % 10);
		CNT++;
		if (temp_num == N) break;
	}
	cout << CNT;
}