#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int L, A, B, C, D;

	cin >> L >> A >> B >> C >> D;

	int carry = 0;
	if (A % C != 0) carry = 1;
	A = A / C;
	A += carry;

	carry = 0;
	if (B % D != 0) carry = 1;
	B = B / D;
	B += carry;
	if (A > B) {
		cout << L - A;
	}
	else {
		cout << L - B;
	}

}