#include<iostream>
#include <string>
#define SIZE 2000000

using namespace std;

bool primenum[SIZE];
void init() {
	for (int a = 2; a < SIZE; a++) {
		primenum[a] = true;
	}
}
void primeNumber() {
	for (int a = 2; a < SIZE; a++) {
		if (!primenum[a]) continue;
		int index = 2;
		while (a * index <SIZE) {
			primenum[a * index] = false;
			index++;
		}
	}
}
bool checkP(int num) {
	string str = to_string(num);
	int size = str.length();
	for (int a = 0; a < size / 2; a++) {
		if (str[a] == str[size - 1 - a]) continue;
		return false;
	}
	return true;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int n;
	cin >> n;

	init();
	primeNumber();
	for (int a = n; a < SIZE; a++) {
		if (!primenum[a]) continue;
		if (!checkP(a)) continue;
		cout << a; break;
	}
}