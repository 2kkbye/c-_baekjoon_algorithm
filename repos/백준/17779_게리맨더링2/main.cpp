#include<iostream>
#include<queue>
#define SIZE 21

using namespace std;

struct point {
	int x, y;
};

int people[SIZE][SIZE];
int copyMap[SIZE][SIZE];
bool visit[SIZE][SIZE];
int res[5];
int N;
int dx[4] = {0,1,0,-1};
int dy[4] = {1,0,-1,0};
int ans = 100000000;
void init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			people[a][b] = 0;
		}
	}
}
void copyInit() {
	for (int a = 1; a < N; a++) {
		for (int b = 1; b < N; b++) {
			copyMap[a][b] = 0;
			visit[a][b] = false;
		}
	}
	for (int a = 0; a < 5; a++) {
		res[a] = 0;
	}
}
int resultCheck() {
	int max = -100000000;
	int min = 100000000;
	for (int a = 0; a < 5; a++) {
		if (res[a] > max) {
			max = res[a];
		}
		if (res[a] < min) {
			min = res[a];
		}
	}
	return max - min;
}
void Input() {
	cin >> N;
	N += 1;
	for (int a = 1; a < N; a++) {
		for (int b = 1; b < N; b++) {
			cin >> people[a][b];
		}
	}
}
bool checkMap(int x, int y, int d1, int d2) {
	copyMap[x][y] = 5;
	//1
	for (int a = 1; a <= d1; a++) {
		if (x + a < 1 || x + a >= N || y - a <1 || y - a >= N) return false;
		copyMap[x + a][y - a] = 5;
	}
	//2
	for (int a = 1; a <= d2; a++) {
		if (x + a < 1 || x + a >= N || y + a < 1 || y + a >= N) return false;
		copyMap[x + a][y + a] = 5;
	}
	//3
	for (int a = 1; a <= d2; a++) {
		if (x + d1 + a < 1 || x + d1 + a >= N || y - d1 + a < 1 || y - d1 + a >= N) return false;
		copyMap[x + d1 + a][y - d1 + a] = 5;
	}
	//4
	for (int a = 1; a <= d1; a++) {
		if (x + d2 + a < 1 || x + d2 + a >= N || y + d2 - a < 1 || y + d2 - a >= N) return false;
		copyMap[x + d2 + a][y + d2 - a] = 5;
	}
	return true;
}
void Print() {
	cout << "========================" << endl;
	for (int a = 1; a < N; a++) {
		for (int b = 1; b < N; b++) {
			cout << copyMap[a][b] << " ";
		}
		cout << endl;
	}
}
void bfs(int x, int y, int s_x, int s_y, int type, int d1, int d2) {
	queue<point> q_elec;
	visit[s_x][s_y] = true;
	copyMap[s_x][s_y] = type;
	point temp;
	temp.x = s_x; temp.y = s_y;
	q_elec.push(temp);
	res[type] = people[s_x][s_y];
	while (!q_elec.empty()) {
		point p = q_elec.front();
		q_elec.pop();
		int px = p.x;
		int py = p.y;
		for (int a = 0; a < 4; a++) {
			int rx = dx[a] + px;
			int ry = dy[a] + py;
			if (rx >= 1 && ry >= 1 && rx < N && ry < N) {
				if (visit[rx][ry]) continue;
				if (copyMap[rx][ry] != type && copyMap[rx][ry] !=0) continue;
				if (type == 1) {
					if (rx < x + d1 && ry <= y) {
					}
					else continue;
				}
				else if (type == 2) {
					if (rx <= x + d2 && y < ry && ry < N) {
					}
					else continue;
				}
				else if (type == 3) {
					if (x + d1 <= rx && rx < N && ry < y - d1 + d2) {
					}
					else continue;
				}
				else if (type == 4) {
					if (x + d2 < rx && y - d1 + d2 <= ry && ry < N) {
					}
					else continue;
				}
				visit[rx][ry] = true;
				copyMap[rx][ry] = type;
				p.x = rx;
				p.y = ry;
				res[type] += people[rx][ry];
				q_elec.push(p);
			}
		}
	}
}
void zeroCheck() {
	for (int a = 1; a < N; a++) {
		for (int b = 1; b < N; b++) {
			if (copyMap[a][b] == 0 || copyMap[a][b] == 5) {
				res[0] += people[a][b];
			}
		}
	}
}
void Solution() {
	for (int a = 1; a < N; a++) {
		for (int b = 1; b < N; b++) {
			if (a == 1 && b == 1) continue;
			if (a == 1 && b == N - 1) continue;
			if (a == N - 1 && b == 1) continue;
			if (a == N - 1 && b == N - 1) continue;
			for (int d2 = 1; d2 + b <= N; d2++) {
				for (int d1 = 1; d1 + d2 + a <= N; d1++) {
					copyInit();
					if (!checkMap(a, b, d1, d2)) continue;
					bfs(a,b,1, 1, 1, d1, d2);
					bfs(a,b,1, N-1, 2, d1, d2);
					bfs(a,b,N-1, 1, 3, d1, d2);
					bfs(a,b,N-1, N-1, 4, d1, d2);
					//Print();
					zeroCheck();
					int tans = resultCheck();
					if (tans < ans) {
						ans = tans;
					}
				}
			}
		}
	}
	cout << ans;
}
void Solve() {
	init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();

}