#include<iostream>
#define SIZE 8
using namespace std;
int num[SIZE] = { 10,100,1000,10000,100000,1000000,10000000,100000000 };
int tem[SIZE] = {2,3,4,5,6,7,8,9};
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N;
	cin >> N;
	long long ans = 0;
	int temp;
	for (int a = SIZE - 1; a >= 0; a--) {
		if (num[a] > N) continue;
		temp = N - num[a] + 1;
		N -= temp;
		ans += (temp * tem[a]);
	}
	cout << ans+N;

}