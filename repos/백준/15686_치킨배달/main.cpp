#include<iostream>
#include<vector>
#include<queue>
#define SIZE 50

using namespace std;

vector<pair<int, int>> house;
vector<pair<int, int>> chicken;
queue<pair<int, int>> q_chicken;

//전체 지도
int map[SIZE][SIZE];

//bfs용 bool 함수
bool visitMap[SIZE][SIZE];

//dfs 전용 bool 함수
bool visit[13];
int N, M;

//bfs용 방향 변수
int dx[4] = {0,1,0,-1};
int dy[4] = {1,0,-1,0};
int ans = 100000000;


//초기화
void init() {
	for (int a = 0; a < 13; a++) {
		visit[a] = false;
	}
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
			visitMap[a][b] = false;
		}
	}
}

//지도들 초기화
void funInit() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
			visitMap[a][b] = false;
		}
	}
}

//입력
void Input() {
	cin >> N >> M;
	int temp;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			cin >> temp;
			if (temp == 0) continue;
			if (temp == 1) {
				house.push_back(make_pair(a, b));
			}
			else {
				chicken.push_back(make_pair(a, b));
			}
		}
	}
}
//bfs
void bfs() {
	while (q_chicken.size() != 0) {
		int px = q_chicken.front().first;
		int py = q_chicken.front().second;
		q_chicken.pop();
		for (int a = 0; a < 4; a++) {
			int rx = px + dx[a];
			int ry = py + dy[a];

			if (rx >= 0 && ry >= 0 && rx < N && ry < N) {
				if (visitMap[rx][ry]) continue;
				visitMap[rx][ry] = true;
				map[rx][ry] = map[px][py] + 1;
				q_chicken.push(make_pair(rx, ry));
			}
		}
	}
}

//치킨집들에서의 집들의 거리
int check_house_distance() {
	int tans = 0;
	for (int a = 0; a < house.size(); a++) {
		int px = house[a].first;
		int py = house[a].second;
		tans += map[px][py];
	}
	return tans;
}

void Solution() {
	int tans = 0;
	for (int a = 0; a < chicken.size(); a++) {
		if (!visit[a]) continue;
		q_chicken.push(make_pair(chicken[a].first, chicken[a].second));
		visitMap[chicken[a].first][chicken[a].second] = true;
	}
	bfs();
	tans = check_house_distance();
	if (tans < ans) {
		ans = tans;
	}
	funInit();
}
void dfs(int index, int level) {
	if (level == M) {
		Solution();
		return;
	}
	for (int a = index; a < chicken.size(); a++) {
		if (visit[a]) continue;
		visit[a] = true;
		dfs(a + 1, level + 1);
		visit[a] = false;
	}
}
void Solve() {
	init();
	Input();
	dfs(0, 0);
	cout << ans;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();
}