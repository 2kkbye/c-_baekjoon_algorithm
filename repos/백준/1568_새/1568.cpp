#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N;
	cin >> N;
	if (N <= 0) { cout << 0; return 0; }
	int time = 1,fly_num=1;
	while (true) {
		//cout <<"#####"<< fly_num<<'\n';
		N -= fly_num;
		if (N == 0) break;
		if (N < 0) { N += fly_num; fly_num = 1; continue; }
		time++;
		fly_num++;
	}
	cout << time;
}