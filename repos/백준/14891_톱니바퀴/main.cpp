#include<iostream>
#define ROW 8
#define COL 4

using namespace std;

int num[ROW][COL];
int cnum[ROW][COL];

void init() {
	for (int a = 0; a < ROW; a++) {
		for (int b = 0; b < COL; b++) {
			num[a][b] = 0;
			cnum[a][b] = 0;
		}
	}
}

void moveNum(int col, int type) {
	//시계방향
	if (type == -1) {
		for (int a = 0; a < ROW; a++) {
			int index = a + 1;
			if (index == ROW) index = 0;
			cnum[a][col] = num[index][col];
		}
	}
	//반시계방향
	else {
		cnum[0][col] = num[7][col];
		for (int a = 1; a < ROW; a++) {
			cnum[a][col] = num[a-1][col];
		}
	}
}
void copyToNum() {
	for (int a = 0; a < ROW; a++) {
		for (int b = 0; b < COL; b++) {
			num[a][b] = cnum[a][b];
		}
	}
}
void print() {
	cout << "==================================="<<endl;
	for (int a = 0; a < COL; a++) {
		for (int b = 0; b < ROW; b++) {
			cout << num[b][a];
		}
		cout << endl;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	init();
	string temp;
	for (int a = 0; a < 4; a++) {
		cin >> temp;
		for (int b = 0; b < temp.length(); b++) {
			num[b][a] = (int)temp[b] - 48;
			cnum[b][a] = num[b][a];
		}
	}

	int t;
	cin >> t;
	int tcol, dir;
	for (int a = 0; a < t; a++) {
		//dir 1이면 시계, -1이면 반시계
		//tcol은 1~4 존재.
		cin >> tcol >> dir;
		tcol -= 1;
		moveNum(tcol, dir);

		int Ldir = dir;
		int Rdir = dir;
		for (int a = tcol - 1; a >= 0; a--) {
			if (num[2][a] == num[6][a + 1]) break;
			Ldir *= -1;
			moveNum(a, Ldir);
		}
		for (int a = tcol; a < COL-1; a++) {
			if (num[2][a] == num[6][a + 1]) break;
			Rdir *= -1;
			moveNum(a+1, Rdir);
		}
		copyToNum();
		//print();
	}
	int ans = 0;
	for (int a = 0; a < COL; a++) {
		if (num[0][a] == 0) continue;
		if (a == 0) ans += 1;
		else if (a == 1) ans += 2;
		else if (a == 2) ans += 4;
		else if (a == 3) ans += 8;
	}
	cout << ans;
}