#include<iostream>
#define SIZE 101

using namespace std;
int arr[SIZE][SIZE] = {0,};

int result() {
	int res = 0;
	for (int a = 1; a < SIZE; a++) {
		for (int b = 1; b < SIZE; b++) {
			if (arr[a][b] == 0) continue;
			res++;
		}
	}
	return res;
}
void input_rec(int x, int y) {
	for (int a = 0; a < 10; a++) {
		for (int b = 0; b < 10; b++) {
			arr[x + a][y + b]++;
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int n,x,y;
	cin >> n;
	for (int a = 0; a < n; a++) {
		cin >> x >> y;
		input_rec(x, y);
	}
	cout << result();

}