#include<iostream>

using namespace std;
int num[3];

void quick(int start, int end) {
	int s = start;
	int e = end;
	int pivot = num[(start + end) / 2];
	do {
		while (num[s] < pivot) s++;
		while (num[e] > pivot) e--;
		if (s <= e) {
			int temp = num[s];
			num[s] = num[e];
			num[e] = temp;
			s++; e--;
		}
	} while (s <= e);
	if (start < e) {
		quick(start, e);
	}
	if (s < end) {
		quick(s, end);
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N;
	cin >> N;
	int ans = -1,sum,te;
	for (int t = 0; t < N; t++) {
		for (int a = 0; a < 3; a++) {
			cin >> num[a];
		}
		sum = 0;
		quick(0, 2);
		te = 1;
		for (int a = 1; a < 3; a++) {
			if (num[a] == num[a - 1]) te++;
		}
		if (te == 1) {
			sum = num[2] * 100;
		}
		else if (te == 2) {
			sum = 1000 + (num[1] * 100);
		}
		else { // ������
			sum = 10000 + (num[1] * 1000);
		}
		if (ans < sum) {
			ans = sum;
		}
	}
	cout << ans;

}