#include<iostream>
#define SIZE 101
using namespace std;

int arr[101][101] = { 0, };

void res() {
	int cnt = 0;
	for (int a = 1; a < SIZE; a++) {
		for (int b = 1; b < SIZE; b++) {
			if (arr[a][b] == 0) continue;
			cnt++;
		}
	}
	cout << cnt;
}

void Solve(int x, int y, int x1, int y1) {
	for (int a = x; a < x1; a++) {
		for (int b = y; b < y1; b++) {
			arr[a][b]++;
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int x, y, x1, y1;
	for (int a = 0; a < 4; a++) {
		cin >> y >> x >> y1 >> x1;
		Solve(x, y, x1, y1);
	}
	res();

}