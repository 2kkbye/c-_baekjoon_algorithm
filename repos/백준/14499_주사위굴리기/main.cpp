#include<iostream>
#define SIZE 20
#define INS_SIZE 1000

using namespace std;

int map[SIZE][SIZE];
int cube[6];
int ins[INS_SIZE];

int topIndex = 3, bottomIndex = 1, K, N,M, nowX, nowY;
int dx[5] = {0,0,0,-1,1};
int dy[5] = {0,1,-1,0,0};

void Input() {
	cin >> N >> M >> nowX >> nowY >> K;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			cin >> map[a][b];
		}
	}
	for (int a = 0; a < K; a++) {
		cin >> ins[a];
	}
}
void init() {
	for (int a = 0; a < 6; a++) {
		cube[a] = 0;
	}
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
		}
	}
	for (int a = 0; a < INS_SIZE; a++) {
		ins[a] = 0;
	}
}
bool checkDir(int d) {
	int px = nowX + dx[d];
	int py = nowY + dy[d];
	if (px >= 0 && py >= 0 && px < N && py < M) {
		nowX = px;
		nowY = py;
		return true;
	}
	return false;
}
void moveCube(int d) {
	int copyCube[6];
	for (int a = 0; a < 6; a++) {
		copyCube[a] = cube[a];
	}
	//동
	if (d == 1) {
		cube[3] = copyCube[4];
		cube[4] = copyCube[1];
		cube[1] = copyCube[5];
		cube[5] = copyCube[3];
	}
	//서
	else if (d == 2) {
		cube[3] = copyCube[5];
		cube[4] = copyCube[3];
		cube[1] = copyCube[4];
		cube[5] = copyCube[1];
	}
	//북
	else if (d == 3) {
		cube[0] = copyCube[3];
		cube[1] = copyCube[0];
		cube[2] = copyCube[1];
		cube[3] = copyCube[2];
	}
	//남
	else {
		cube[0] = copyCube[1];
		cube[1] = copyCube[2];
		cube[2] = copyCube[3];
		cube[3] = copyCube[0];
	}
}
void Solution() {
	for (int a = 0; a < K; a++) {
		//정상적인 map의 범위 안에 있을 때
		if (!checkDir(ins[a])) {
			continue;
		}
		moveCube(ins[a]);
		if (map[nowX][nowY] == 0) {
			map[nowX][nowY] = cube[bottomIndex];
		}
		else {
			cube[bottomIndex] = map[nowX][nowY];
			map[nowX][nowY] = 0;
		}
		cout << cube[topIndex]<<endl;
	}
}
void Solve() {
	init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}