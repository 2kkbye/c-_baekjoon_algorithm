#include<iostream>

using namespace std;

int arr[10] = { 0, };

int Solve(int inp) {
	if (inp == 0) return 1;
	while (inp > 0) {
		arr[inp % 10]++;
		inp /= 10;
	}
	int temp = ((arr[6] + arr[9]) / 2) + ((arr[6] + arr[9]) % 2);
	arr[6] = temp; arr[9] = 0;
	int max = -1,max_index = -1;
	for (int a = 0; a < 10; a++) {
		if (arr[a] <= max) continue;
		max = arr[a]; max_index = a;
	}
	return max;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int inp;
	cin >> inp;
	cout << Solve(inp);
}