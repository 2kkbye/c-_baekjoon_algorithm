#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t, sum = 0, n, num;
	cin >> t;
	for (int a = 0; a < t; a++) {
		sum = 0;
		cin >> n;
		for (int a = 0; a < n; a++) {
			cin >> num;
			sum += num;
		}
		cout << sum;
		if (a == t - 1) break;
		cout << '\n';
	}
}