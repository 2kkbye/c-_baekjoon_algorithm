#include<iostream>
#include<vector>

using namespace std;

bool check_specil(char ch) {
	string speil = "~!@#$%^&*";
	for (int a = 0; a < speil.length(); a++) {
		if (ch == speil[a]) return true;
	}
	return false;
}

vector<int> solution(string inp_str) {
	vector<int> answer;
	if (inp_str.length() < 8 || inp_str.length() > 15) {
		answer.push_back(1);
	}
	int check_cout[4] = { 0,0,0,0 };
	
	for (int a = 0; a < inp_str.length(); a++) {
		char ch = inp_str[a];
		if(ch >= 'A' && ch <= 'Z') {
			check_cout[0] = 1;
			continue;
		}
		else if (ch >= 'a' && ch <= 'z') {
			check_cout[1] = 1;
			continue;
		}
		else if (ch >= '0' && ch <= '9') {
			check_cout[2] = 1;
			continue;
		}
		else if (check_specil(ch)) {
			check_cout[3] = 1;
			continue;
		}
		else {
			bool c = true;
			for (int a = 0; a < answer.size(); a++) {
				if (answer[a] == 2) {
					c = false;
					break;
				}
			}
			if (c) {
				answer.push_back(2);
			}
			continue;
		}
	}
	int count = 0;
	for (int a = 0; a < 4; a++) {
		if (check_cout[a] == 1) count++;
	}
	if (count < 3) {
		answer.push_back(3);
	}
	count = 1;
	char check = inp_str[0];
	for (int a = 1; a < inp_str.length(); a++) {
		if (check == inp_str[a]) {
			count++;
			if (count == 4) {
				answer.push_back(4);
				break;
			}
		}
		else {
			check = inp_str[a];
			count = 1;
		}
	}
	vector<char> che;
	bool total_count = false;
	for (int a = 0; a < inp_str.length(); a++) {
		check = inp_str[a];
		total_count = false;
		count = 1;
		for (int b = 0; b < che.size(); b++) {
			if (che[b] == check) total_count = true;
		}
		if (total_count) continue;
		che.push_back(check);
		for (int b = a + 1; b < inp_str.length(); b++) {
			if (check == inp_str[b]) count++;
		}
		if (count > 4) {
			answer.push_back(5);
			break;
		}
	}
	if (answer.size() == 0) {
		answer.push_back(0);
	}
	return answer;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	string inp_s[5] = { "AaTa+!12-3","aaaaZZZZ)","CaCbCgCdC888834A","UUUUU","ZzZz9Z824" };
	//string inp_s[1] = {"CaCbCgCdC888834A"};

	vector<int> result;
	for (int a = 0; a < 5; a++) {
		result = solution(inp_s[a]);
		for (int b = 0; b < result.size(); b++) {
			cout << result[b] << " ";
		}
		cout << '\n';
	}
}