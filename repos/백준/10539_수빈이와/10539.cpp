#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	int N;
	cin >> N;
	unsigned long long num;
	unsigned long long sum = 0;
	for (int a = 1; a <= N; a++) {
		cin >> num;
		unsigned long long temp = a * num;
		cout << temp-sum;
		sum = temp;
		if (a != N) cout << " ";
	}
}