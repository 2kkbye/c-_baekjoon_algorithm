#include<iostream>
#define SIZE 10

using namespace std;

int arr[SIZE];
void init() {
	for (int a = 0; a < SIZE; a++) {
		arr[a] = 0;
	}
}
int result() {
	int max = 0;
	for (int a = 0; a < SIZE; a++) {
		if (arr[a] != 0) max++;
	}
	return max;
}
void Solve(int num) {
	while (num > 9) {
		arr[num % 10]++;
		num /= 10;
	}
	arr[num]++;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t,num;
	cin >> t;

	for (int a = 0; a < t; a++) {
		init();
		cin >> num;
		Solve(num);
		cout << result();
		if (a != t - 1) cout << endl;
	}
	
}