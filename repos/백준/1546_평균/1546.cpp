#include<iostream>

using namespace std;

double Solve(double arr[], double MAX, double N) {
	double sum = 0;
	for (int a = 0; a < N; a++) {
		arr[a] = (arr[a] / MAX) * 100;
		sum += arr[a];
	}
	return sum;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	//double N;
	double N,test_num, MAX = -1;
	double arr[1000] = {-1,};
	cin >> N;
	for (int a = 0; a < N; a++) {
		cin >> test_num;
		arr[a] = test_num;
		if (MAX > test_num)continue;
		MAX = test_num;
	}
	cout << (double)(Solve(arr,MAX,N) / N);
}