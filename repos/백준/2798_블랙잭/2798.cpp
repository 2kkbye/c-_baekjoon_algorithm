#include<iostream>
#define SIZE 101

using namespace std;

bool check[SIZE] = { false, };
int num[SIZE] = { 0, };
int N,M,sum=0,ans=0;

void Solve(int index,int level) {
	if (level == 4) {
		if (sum > ans && sum<=M) ans = sum;
		return;
	}
	for (int a = index; a < N; a++) {
		if (check[a]) continue;
		check[a] = true;
		sum += num[a];
		Solve(a + 1, level + 1);
		sum -= num[a];
		check[a] = false;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	cin >> N >> M;
	for (int a = 0; a < N; a++) {
		cin >> num[a];
	}
	for (int a = 0; a < N - 2; a++) {
		check[a] = true;
		sum += num[a];
		Solve(a + 1, 2);
		sum -= num[a];
		check[a] = false;
	}
	cout << ans;
}