#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t, a_win = 0, b_win = 0, a_sc, b_sc;
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> a_sc >> b_sc;
		if (a_sc == b_sc) continue;
		if (a_sc > b_sc) {
			a_win++;
		}
		else {
			b_win++;
		}
	}
	cout << a_win << " " << b_win;

}