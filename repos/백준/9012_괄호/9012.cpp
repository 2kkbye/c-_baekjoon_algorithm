#include<iostream>
#include<queue>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t;
	cin >> t;
	for (int a = 0; a < t; a++) {
		queue<char> open;
		bool res = true;
		string str;
		cin >> str;

		for (int a = 0; a < str.length(); a++) {
			if (str[a] == '(') {
				open.push(str[a]);
				continue;
			}
			else {
				try
				{
					if (open.empty()) throw 1;
					open.pop();
				}
				catch (...)
				{
					res = false;
					break;
				}
			}
		}
		if (res && !open.empty()) {
			res = false;
		}
		if (!res) {
			cout << "NO";
		}
		else {
			cout << "YES";
		}
		if (a != t - 1) {
			cout << '\n';
		}
	}
}