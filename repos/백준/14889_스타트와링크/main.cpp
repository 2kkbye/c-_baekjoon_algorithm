#include<iostream>
#define SIZE 21
//#define SIZE 5

using namespace std;

int map[SIZE][SIZE];
bool visit[SIZE];
int N;
int ans = 1000000000;
int tans = 0;
void init() {
	for (int a = 0; a < SIZE; a++) {
		visit[a] = false;
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
		}
	}
}
int solve() {
	int team1 = 0, team2 = 0;
	for (int a = 1; a <= N; a++) {
		if (!visit[a]) continue;
		for (int b = a + 1; b <= N; b++) {
			if (!visit[b]) continue;
			team1 += (map[a][b] + map[b][a]);
		}
	}
	for (int a = 1; a <= N; a++) {
		if (visit[a]) continue;
		for (int b = a + 1; b <= N; b++) {
			if (visit[b]) continue;
			team2 += (map[a][b] + map[b][a]);
		}
	}
	if (team1 > team2) {
		return team1 - team2;
	}
	else {
		return team2 - team1;
	}
}
void dfs(int level, int index) {
	if (level == N / 2) {
		tans = solve();
		if (tans < ans) {
			ans = tans;
		}
		return;
	}
	for (int a = index; a <= N; a++) {
		if (visit[a]) continue;
		visit[a] = true;
		dfs(level+1, a + 1);
		visit[a] = false;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	init();

	cin >> N;
	for (int a = 1; a <= N; a++) {
		for (int b = 1; b <= N; b++) {
			cin >> map[a][b];
		}
	}
	tans = 0;
	for (int a = 1; a <= (N / 2)+1; a++) {
		visit[a] = true;
		dfs(1, a + 1);
		visit[a] = false;
	}
	cout << ans;
}