#include<iostream>
#define ROWSIZE 5
#define COLSIZE 15
using namespace std;

char arr[ROWSIZE][COLSIZE];

void init() {
	for (int a = 0; a < ROWSIZE; a++) {
		for (int b = 0; b < COLSIZE; b++) {
			arr[a][b] = '-';
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	init();
	string str;
	for (int a = 0; a < ROWSIZE; a++) {
		cin >> str;
		for (int b = 0; b < str.length(); b++) {
			arr[a][b] = str[b];
		}
	}
	for (int b = 0; b < COLSIZE; b++) {
		for (int a = 0; a < ROWSIZE; a++) {
			if (arr[a][b] == '-') continue;
			cout << arr[a][b];
		}
	}
}