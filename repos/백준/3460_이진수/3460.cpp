#include<iostream>
#define SIZE 25
using namespace std;

int binary[SIZE];
void init() {
	for (int a = 0; a < SIZE; a++) {
		binary[a] = -1;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	int t;
	cin >> t;
	for (int a = 0; a < t; a++) {
		init();
		int index = 0;
		int num;
		cin >> num;
		while (num > 0) {
			binary[index] = num % 2;
			num /= 2;
			index++;
		}
		for (int a = 0; a < SIZE; a++) {
			if (binary[a] == -1) break;
			if (binary[a] == 0) continue;
			cout << a << " ";
		}
		if (a != t - 1) cout << endl;
	}
}