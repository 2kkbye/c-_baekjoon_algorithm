#include<iostream>

using namespace std;
int Solve(int num) {
	int n = 64;
	while (n > 0) {
		if (n <= num) return n;
		n /= 2;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	int X;
	cin >> X;
	int res = 0;
	int num = X;
	int temp;
	while (num!=0) {
		temp = Solve(num);
		num -= temp;
		res++;
	}
	cout << res;
}