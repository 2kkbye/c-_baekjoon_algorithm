#include<iostream>
#include<vector>

#define SIZE 4



using namespace std;



struct fish {
    int x, y, dir, n;
    bool live;
};
int map[4][4];
int dx[9] = { 0,-1,-1,0,1,1,1,0,-1 };
int dy[9] = { 0,0,-1,-1,-1,0,1,1,1 };

vector<fish> v_fish;
int ans = -1;

//숫자별로 정렬을 통해서 숫자 작은 것이 맨 앞으로 가게끔 세팅.
void quick(int start, int end) {
    int s = start;
    int e = end;
    int pivot = v_fish[(s + e) / 2].n;
    do {
        while (v_fish[s].n < pivot) s++;
        while (v_fish[e].n > pivot) e--;
        if (s <= e) {
            fish temp = v_fish[s];
            v_fish[s] = v_fish[e];
            v_fish[e] = temp;
            s++; e--;
        }

    } while (s <= e);
    if (s < end) {
        quick(s, end);
    }
    if (start < e) {
        quick(start, e);
    }
}
void Print() {
    cout << "===================================" << endl;
    for (int a = 0; a < SIZE; a++) {
        for (int b = 0; b < SIZE; b++) {
            cout << map[a][b] << " ";
        }
        cout << endl;
    }
}
void Init() {
    for (int a = 0; a < SIZE; a++) {
        for (int b = 0; b < SIZE; b++) {
            map[a][b] = 0;
        }
    }
}
void Input() {
    for (int a = 0; a < 4; a++) {
        for (int b = 0; b < 4; b++) {
            fish f;
            cin >> f.n >> f.dir;
            f.x = a; f.y = b; f.live = true;
            map[a][b] = f.n;
            v_fish.push_back(f);
        }
    }
}
int find_eatFish(int num) {
    for (int a = 0; a < v_fish.size(); a++) {
        if (v_fish[a].n == num) {
            return a;
        }
    }
}
void eatFish(int x, int y, int rx, int ry, bool type, int index) {
    //물고기를 원래대로 살릴 떄
    if (type) {
        map[x][y] = -1;
        map[rx][ry] = v_fish[index].n;
        v_fish[index].live = type;
    }
    //물고기를 죽일 때
    else {
        map[x][y] = 0;
        map[rx][ry] = -1;
        v_fish[index].live = type;
    }
}
void moveFish() {
    for (int a = 0; a < v_fish.size(); a++) {
        //죽은 애들은 굳이 옮길 필요 없고.
        if (!v_fish[a].live) continue;
        fish f = v_fish[a];
        int px = f.x;
        int py = f.y;
        int pdir = f.dir;
        int cnt = 0;
        while (true) {
            //Print();
            cnt++;
            if (cnt == 9) break;
            v_fish[a].dir = pdir;
            int rx = px + dx[pdir];
            int ry = py + dy[pdir++];
            if (pdir == 9) {
                pdir = 1;
            }
            if (rx < 0 || ry < 0 || rx >= SIZE || ry >= SIZE) {
                continue;
            }
            if (map[rx][ry] == -1) continue;
            if (map[rx][ry] == 0) {
                map[px][py] = 0;
                map[rx][ry] = f.n;
                v_fish[a].x = rx; v_fish[a].y = ry;
            }
            else {
                int index = find_eatFish(map[rx][ry]);
                v_fish[a].x = rx; v_fish[a].y = ry;
                v_fish[index].x = px; v_fish[index].y = py;
                map[rx][ry] = map[px][py];
                map[px][py] = v_fish[index].n;
            }
            break;
        }
    }
}
void copyToMap(bool type, int copyMap[4][4]) {
    if (!type) {
        for (int a = 0; a < SIZE; a++) {
            for (int b = 0; b < SIZE; b++) {
                copyMap[a][b] = map[a][b];
            }
        }
    }
    else {
        for (int a = 0; a < SIZE; a++) {
            for (int b = 0; b < SIZE; b++) {
                map[a][b] = copyMap[a][b];
            }
        }
    }
}
/*dfs에서 x,y는 현재 상어의 위치를 뜻한다. */
void dfs(int x, int y, int sdir, int tans) {
    if (tans > ans) {
        ans = tans;
    }
    /*임시 복사 변수*/
    int copyMap[SIZE][SIZE];
    vector<fish> t_fish = v_fish;
    copyToMap(false, copyMap);

    moveFish();
    //Print();

    for (int a = 1; a <= 3; a++) {
        int rx = x + dx[sdir] * a;
        int ry = y + dy[sdir] * a;

        if (rx < 0 || ry < 0 || rx >= SIZE || ry >= SIZE) break;
        if (map[rx][ry] == 0) continue;

        int temp = map[rx][ry];
        int index = find_eatFish(temp);

        eatFish(x, y, rx, ry, false, index);
        dfs(rx, ry, v_fish[index].dir, tans + temp);
        eatFish(x, y, rx, ry, true, index);
    }
    copyToMap(true, copyMap);
    v_fish = t_fish;
}
void Solution() {
    //물고기들을 n이 커지는 순서로 정렬.
    quick(0, v_fish.size() - 1);

    /* 상어가 기본적으로 처음에 0,0을 먹고 들어간다*/
    int index = find_eatFish(map[0][0]);
    map[0][0] = -1;
    v_fish[index].live = false;

    dfs(0, 0, v_fish[index].dir, v_fish[index].n);
    cout << ans;
}
void Solve() {
    Init();
    Input();
    Solution();
}
int main() {
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    Solve();
}