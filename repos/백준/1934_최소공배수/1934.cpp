#include<iostream>

using namespace std;

int num[2];
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int t;
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> num[0] >> num[1];
		if (num[0] == 1 || num[1] == 1) {
			cout << num[0] * num[1];
			if (a != t - 1) cout << endl;
			continue;
		}
		int div = 2;
		int min = 1;
		while (true) {
			if (num[0] % div == 0 && num[1] % div == 0) {
				min *= div;
				num[0] /= div;
				num[1] /= div;
				div = 2; continue;
			}
			div++;
			if (num[0] < div || num[1] < div) break;
		}
		min *= num[0];
		min *= num[1];
		cout << min;
		if (a != t - 1) cout << endl;
	}
}