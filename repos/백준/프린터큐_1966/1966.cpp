#include<iostream>
#include<vector>	

using namespace std;


class Point {
	int index, imp;
public:
	Point(int index, int imp) {
		this->index = index;
		this->imp = imp;
	}
	int getIndex() { return index; }
	int getImp() { return imp; }
};
vector <Point> vt;
int N, M;

bool check(int temp_ind) {
	int index = temp_ind;
	bool c = true;
	while (true) {
		if (index == vt.size()) index = 0;
		if (index == temp_ind && !c) return true;
		if (index == temp_ind && c) c = false;
		if (vt[temp_ind].getImp() < vt[index].getImp()) return false;
		index++;
	}
}
void Solve() {
	int index = 0;
	int cnt = 0;
	while (vt.size() != 0) {
		if (index == vt.size()) index = 0;
		if (check(index)) {
			cnt++;
			if (vt[index].getIndex() == M) {
				cout << cnt;
				vt.clear();
				return;
			}
			vt.erase(vt.begin()+index);
		}
		else {
			Point p = vt[0];
			vt.erase(vt.begin());
			vt.push_back(p);
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t,temp;

	cin >> t;
	for (int q = 0; q < t; q++) {
		cin >> N >> M;
		for (int a = 0; a < N; a++) {
			cin >> temp;
			vt.push_back(Point(a, temp));
		}
		Solve();
		if (q == t - 1) break;
		cout << '\n';
	}

}