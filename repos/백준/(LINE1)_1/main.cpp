#include<iostream>
#include<vector>
using namespace std;

string solution(vector<string> table, vector<string> languages, vector<int> preference) {
	string answer = "";
	//6-해당 인덱스는 점수
	string ta[5][6];
	for (int a = 0; a < table.size(); a++) {
		string temp="";
		int index = 0;
		for (int b = 0; b < table[a].length(); b++) {
			if (table[a][b] == ' ') {
				ta[a][index] = temp;
				//cout << temp;
				temp = "";
				index++;
			}
			else {
				temp = temp + table[a][b];
			}
		}
		ta[a][index] = temp;
		//cout << temp;
	}
	vector<int> res_temp;
	for (int a = 0; a < 5; a++) {
		int sum = 0;
		for (int b = 0; b < languages.size(); b++) {
			string temp = languages[b];
			int mul = 0;
			for (int c = 1; c < 6; c++) {
				if (temp == ta[a][c]) {
					mul = (6 - c) * preference[b];
					break;
				}
			}
			sum += mul;
			
		}
		res_temp.push_back(sum);
	}
	int max = -1, max_index=0;
	for (int a = 0; a < res_temp.size(); a++) {
		if (res_temp[a] > max) {
			max = res_temp[a];
			max_index = a;
		}
		else if (res_temp[a] == max) {
			if (ta[max_index][0] > ta[a][0]) {
				max = res_temp[a];
				max_index = a;
			}
		}
	}
	answer = ta[max_index][0];
	cout << answer;
	return answer;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	vector<string> table;
	vector<string> lan;
	vector<int> pre;
	table.push_back("SI JAVA JAVASCRIPT SQL PYTHON C#");
	table.push_back("CONTENTS JAVASCRIPT JAVA PYTHON SQL C++");
	table.push_back("HARDWARE C C++ PYTHON JAVA JAVASCRIPT");
	table.push_back("PORTAL JAVA JAVASCRIPT PYTHON KOTLIN PHP");
	table.push_back("GAME C++ C# JAVASCRIPT C JAVA");
	lan.push_back("JAVA");
	lan.push_back("JAVASCRIPT");
	pre.push_back(7);
	pre.push_back(5);
	solution(table, lan, pre);
}