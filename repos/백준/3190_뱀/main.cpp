#include<iostream>
#include<vector>

#define SIZE 101

using namespace std;

int map[SIZE][SIZE];
int dir[SIZE];
int ntime[SIZE];

int N, L;

int dx[4] = {0,1,0,-1};
int dy[4] = {1,0,-1,0};

int nowDir = 0;
int dIndex = 1;
vector<pair<int, int>> snake;
int head_x = 1;
int head_y = 1;
void init() {
	dir[0] = 0;
	for (int a = 1; a < SIZE; a++) {
		dir[a] = 0;
		ntime[a] = 0;
		for (int b = 1; b < SIZE; b++) {
			map[a][b] = 0;
		}
	}
}
void Input() {
	cin >> N;
	int t;
	int x, y;
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> x >> y;
		map[x][y] = 1;
	}
	cin >> L;
	char c;
	for (int a = 1; a <= L; a++) {
		cin >> t >> c;
		ntime[a] = t;
		//0이면 D, 1이면 L
		if (c == 'L') {
			dir[a] = 1;
		}
	}
}
bool checkBody(int x, int y) {
	for (int a = 0; a < snake.size(); a++) {
		if (x == snake[a].first && y == snake[a].second) {
			return false;
		}
	}
	return true;
}
void eraseTail() {
	snake.erase(snake.begin(), snake.begin() + 1);
}
bool makeHead() {
	int px = head_x;
	int py = head_y;
	px += dx[nowDir];
	py += dy[nowDir];

	if (px >= 1 && py >= 1 && px <= N && py <= N) {
		if (checkBody(px, py)) {
			head_x = px;
			head_y = py;
			return true;
		}
	}
	return false;
}
void Solution() {
	int ans = 0;
	for (int a = 1; a <= 1000000000; a++) {
		snake.push_back(make_pair(head_x, head_y));
		//먼저 머리부터 늘리고
		if (!makeHead()) {
			ans = a;
			break;
		}
		//사과 있는지 판단 없으면 꼬리 지우고
		if (map[head_x][head_y] == 0) {
			eraseTail();
		}
		else {
			map[head_x][head_y] = 0;
		}
		//현재 해당 시간이라면
		if (ntime[dIndex] == a) {
			if (dir[dIndex] == 0) {
				nowDir++;
				if (nowDir > 3) nowDir = 0;
			}
			else {
				nowDir--;
				if (nowDir < 0) nowDir = 3;
			}
			dIndex++;
		}
	}
	cout << ans;
}
void Solve() {
	init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();

}