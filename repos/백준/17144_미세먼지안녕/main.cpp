#include<iostream>
#define SIZE 50

using namespace std;

int map[SIZE][SIZE];
int copyMap[SIZE][SIZE];
int ac[2][2];
int dx[4] = {0,-1,0,1};
int dy[4] = {1,0,-1,0};

int dx2[4] = { 0,1,0,-1 };
int dy2[4] = { 1,0,-1,0 };
int R, C;
void init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
			copyMap[a][b] = 0;
		}
	}
}
void initCopymap() {
	for (int a = 0; a < R; a++) {
		for (int b = 0; b < C; b++) {
			copyMap[a][b] = 0;
		}
	}
}
void copyToMap() {
	for (int a = 0; a < R; a++) {
		for (int b = 0; b < C; b++) {
			map[a][b] = copyMap[a][b];
		}
	}
}
void spreadMap() {
	for (int a = 0; a < R; a++) {
		for (int b = 0; b < C; b++) {
			if (map[a][b] ==0) continue;
			if (map[a][b] == -1) {
				copyMap[a][b] = -1;
				continue;
			}
			int spreadNum = map[a][b] / 5;
			int spreadCount = 0;
			for (int c = 0; c < 4; c++) {
				int rx = a + dx[c];
				int ry = b + dy[c];
				if (rx >= 0 && ry >= 0 && rx < R && ry < C) {
					if (map[rx][ry] == -1) continue;
					copyMap[rx][ry] += spreadNum;
					spreadCount++;
				}
			}
			int temp = map[a][b] - (spreadNum * spreadCount);
			copyMap[a][b] += temp;
		}
	}
}
void playAC() {
	int cp = 0;
	for (int a = 0; a < R; a++) {
		for (int b = 0; b < C; b++) {
			if (copyMap[a][b] != -1) continue;
			int px = a;
			int py = b;
			int index = 0;
			int beforeNum;
			//위로 가야하는 공기청정기
			if (cp == 0) {
				while (true) {
					beforeNum = copyMap[px][py];
					if (beforeNum == -1) beforeNum = 0;
					int rx = px + dx[index];
					int ry = py + dy[index];
					if (rx < 0 || ry < 0 || rx >= R || ry >= C) {
						index++; continue;
					}
					if (copyMap[rx][ry] == -1) break;
					map[rx][ry] = beforeNum;
					px = rx;
					py = ry;
				}
				cp++;
			}
			//밑으로 가야하는 공기청정기
			else {
				while (true) {
					beforeNum = copyMap[px][py];
					if (beforeNum == -1) beforeNum = 0;
					int rx = px + dx2[index];
					int ry = py + dy2[index];
					if (rx < 0 || ry < 0 || rx >= R || ry >= C) {
						index++; continue;
					}
					if (copyMap[rx][ry] == -1) break;
					map[rx][ry] = beforeNum;
					px = rx;
					py = ry;
				}
			}
		}
	}
}
void print() {
	cout << "===================================" << endl;
	for (int a = 0; a < R; a++) {
		for (int b = 0; b < C; b++) {
			cout << map[a][b] << " ";
		}
		cout << endl;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	init();
	int t;
	cin >> R >> C >> t;
	for (int a = 0; a < R; a++) {
		for (int b = 0; b < C; b++) {
			cin >> map[a][b];
		}
	}
	for (int a = 0; a < t; a++) {
		spreadMap();
		copyToMap();
		//print();
		playAC();
		initCopymap();
		//print();
	}
	int ans = 0;
	for (int a = 0; a < R; a++) {
		for (int b = 0; b < C; b++) {
			if (map[a][b] <= 0) continue;
			ans += map[a][b];
		}
	}
	cout << ans;
}