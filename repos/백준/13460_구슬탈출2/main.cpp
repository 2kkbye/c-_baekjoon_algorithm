#include<iostream>

#define SIZE 10

using namespace std;

int map[SIZE][SIZE];
int cpMap[SIZE][SIZE];
int dir[10];
int dx[4] = { 0,1,0,-1 };
int dy[4] = { 1,0,-1,0 };
//int dx[4] = { -1,0,1,0 };
//int dy[4] = { 0,1,0,-1 };
int N, M;
int ball[2][2];
int cpBall[2][2];
int ans = 11;

void init() {
	for (int a = 0; a < SIZE; a++) {
		dir[a] = 0;
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
			cpMap[a][b] = 0;
		}
	}
	for (int a = 0; a < 2; a++) {
		for (int b = 0; b < 2; b++) {
			ball[a][b] = 0;
			cpBall[a][b] = 0;
		}
	}
}
void Input() {
	cin >> N >> M;
	for (int a = 0; a < N; a++) {
		string temp;
		cin >> temp;
		for (int b = 0; b < temp.length(); b++) {
			if (temp[b] == '#') {
				map[a][b] = 1;
			}
			else if (temp[b] == 'O') {
				map[a][b] = 2;
			}
			else if (temp[b] == 'R') {
				ball[0][0] = a;
				ball[0][1] = b;
			}
			else if (temp[b] == 'B') {
				ball[1][0] = a;
				ball[1][1] = b;
			}
		}
	}
}
void copyMap() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			cpMap[a][b] = map[a][b];
		}
	}
	for (int a = 0; a < 2; a++) {
		for (int b = 0; b < 2; b++) {
			cpBall[a][b] = ball[a][b];
		}
	}
}
bool moveBall(int d, int type) {
	int x,y,px, py;
	if (type == 3) {
		x = cpBall[0][0];
		y = cpBall[0][1];
	}
	else {
		x = cpBall[1][0];
		y = cpBall[1][1];
	}
	bool check = false;
	px = x, py = y;
	int rx, ry;
	while (true) {
		rx = px + dx[d];
		ry = py + dy[d];
		if (cpMap[rx][ry] == 1) break;
		if (cpMap[rx][ry] == 2) return true;
		if (cpMap[rx][ry] != type && cpMap[rx][ry] !=0) {
			check = true;
		}
		px = rx, py = ry;
	}
	if (check) {
		if (d < 2) {
			d += 2;
		}
		else {
			d -= 2;
		}
		px = px + dx[d];
		py = py + dy[d];
	}
	int index;
	if (type == 3) {
		index = 0;
	}
	else {
		index = 1;
	}
	cpBall[index][0] = px;
	cpBall[index][1] = py;

	return false;
}
void Solution() {
	copyMap();
	//dir 배열에 있는 방향을 바탕으로 공을 옮겨야함.
	for (int a = 0; a < SIZE; a++) {
		int old_x1 = cpBall[0][0], old_x2 = cpBall[1][0], old_y1 = cpBall[0][1], old_y2 = cpBall[1][1];
		cpMap[old_x1][old_y1] = 3;
		cpMap[old_x2][old_y2] = 4;
		bool ch1 = moveBall(dir[a], 3);
		bool ch2 = moveBall(dir[a], 4);
		cpMap[old_x1][old_y1] = 0;
		cpMap[old_x2][old_y2] = 0;
		cpMap[cpBall[0][0]][cpBall[0][1]] = 3;
		cpMap[cpBall[1][0]][cpBall[1][1]] = 4;
		if (ch2) return;
		if (ch1 && !ch2) {
			if (a + 1 < ans) {
				ans = a + 1;
				return;
			}
		}
	}
}
void dfs(int level, int pre) {
	if (level == 10) {
		Solution();
		return;
	}
	for (int a = 0; a < 4; a++) {
		if (pre == a) continue;
		dir[level] = a;
		dfs(level + 1, a);
	}
}
void Solve() {
	init();
	Input();
	dfs(0, -1);
	if (ans == 11) {
		cout << -1;
	}
	else {
		cout << ans;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();
	
}