#include<iostream>
#define SIZE 10000

using namespace std;

int prime_number[SIZE];

void Solve(int n) {
	int temp = n;
	int c = 2;
	while (temp * c < SIZE) {
		prime_number[temp * c] = 2;
		c++;
	}
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int M, N;
	cin >> M >> N;

	for (int a = 2; a < SIZE; a++) {
		if (prime_number[a] == 2) continue;
		if (a > N) break;
		prime_number[a] = 1;
		Solve(a);
	}
	int sum = 0;
	int min = 10001;
	bool check = false;
	for (int a = M; a <= N; a++) {
		if (prime_number[a] != 1) continue;
		if (!check)min = a;
		check = true;
		sum += a;
	}
	if (sum == 0) {
		cout << -1; return 0;
	}
	else {
		cout << sum << endl;
		cout << min;
	}
}