#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int C, K, P;
	cin >> C >> K >> P;
	int ans = 0;
	for (int a = 1; a <= C; a++) {
		ans += (a * K) + (P * (a * a));
	}
	cout << ans;
}