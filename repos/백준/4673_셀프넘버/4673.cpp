#include<iostream>

using namespace std;
#define SIZE 10001

bool check[SIZE];
void init() {
	for (int a = 1; a < SIZE; a++) {
		check[a] = false;
	}
}
int numtoSplit(int num) {
	int sum = num;
	while (num > 0) {
		sum += (num % 10);
		num /= 10;
	}
	return sum;
}

void Print() {
	for (int a = 1; a < SIZE; a++) {
		if (check[a]) continue;
		cout << a;
		cout << '\n';
	}
}

void Solve() {
	int num;
	for (int a = 1; a < SIZE; a++) {
		num = numtoSplit(a);
		if (num > SIZE - 1) continue;
		check[num] = true;
	}
	Print();
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();
}