#include<iostream>

#define PERSON 201
using namespace std;


int score[PERSON][3];
int N;
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	cin >> N;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < 3; b++) {
			cin >> score[a][b];
		}
	}
	int num;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < 3; b++) {
			num = score[a][b];
			bool check = false;
			for (int c = a+1; c < N; c++) {
				if (num != score[c][b])continue;
				check = true;
				score[c][b] = 0;
			}
			if (check) score[a][b] = 0;
		}
	}
	int sum;
	for (int a = 0; a < N; a++) {
		sum = 0;
		for (int b = 0; b < 3; b++) {
			sum += score[a][b];
		}
		cout << sum;
		if (a != N - 1) cout << endl;
	}
}