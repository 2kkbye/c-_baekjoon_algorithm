#include<iostream>
#include<vector>


using namespace std;
int changeTo10(int num, int temp) {
	if (temp == 0) return 1;
	int tmp = num;
	for (int a = 0; a < temp-1; a++) {
		tmp *= num;
	}
	return tmp;
}
void changeToB(int num, int temp) {
	vector<int> vc;
	while (num >= temp) {
		vc.push_back(num % temp);
		num /= temp;
	}
	vc.push_back(num);
	for (int a = vc.size() - 1; a >= 0; a--) {
		cout << vc[a];
		if (a != 0) cout << " ";
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int a_n, b_n,m,temp,sum=0;

	cin >> a_n >> b_n;
	cin >> m;
	for (int a = m - 1; a >= 0; a--) {
		cin >> temp;
		sum += (temp*(changeTo10(a_n, a)));
	}
	changeToB(sum, b_n);
}