#include<iostream>

using namespace std;
string str;
void Input() {
	cin >> str;
}
void Solve() {
	//a97, A65
	Input();
	for (int a = 0; a < str.length(); a++) {
		if (str[a] >= 'a' && str[a] <= 'z') {
			int temp = (int)str[a] - 32;
			char tc = (char)temp;
			str[a] = tc;
		}
		else {
			int temp = (int)str[a] + 32;
			char tc = (char)temp;
			str[a] = tc;
		}
	}
	cout << str;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}