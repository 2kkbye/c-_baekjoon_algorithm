#include<iostream>
#define SIZE 14

using namespace std;

int num[3][SIZE];
void init() {
	for (int a = 0; a < 3; a++) {
		for (int b = 0; b < SIZE; b++) {
			num[a][b] = 0;
		}
	}
}
void insertNum(string aNum,string bNum, int t) {
	int index = SIZE - 1;
	for (int a = bNum.length()-1; a >= 0; a--) {
		num[t][index] = ((int)bNum[a] - 48);
		index--;
	}
	for (int a = aNum.length() - 1; a >= 0; a--) {
		num[t][index] = ((int)aNum[a] - 48);
		index--;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string a, b, c, d;
	cin >> a >> b >> c >> d;
	insertNum(a, b, 0);
	insertNum(c, d, 1);
	int carry = 0;
	for (int a = SIZE - 1; a >= 0; a--) {
		int temp = num[0][a] + num[1][a] + carry;
		if (temp >= 10) {
			carry = 1;
			temp -= 10;
		}
		else {
			carry = 0;
		}
		num[2][a] = temp;
	}
	bool type = false;
	for (int a = 0; a < SIZE; a++) {
		if (!type && num[2][a] == 0) continue;
		type = true;
		cout << num[2][a];
	}
}