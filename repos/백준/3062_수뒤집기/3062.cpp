#include<iostream>
#include<string>
#include<stdlib.h>

using namespace std;
int changeToNum(string str) {
	return atoi(str.c_str());
}
int changeToStr(string str) {
	string temp = "";
	for (int a = str.length() - 1; a >= 0; a--) {
		temp += str[a];
	}
	return atoi(temp.c_str());
}
bool checkPel(int num) {
	string temp = to_string(num);
	for (int a = 0; a <= temp.length() / 2; a++) {
		if (temp[a] == temp[temp.length() - 1 - a]) continue;
		return false;
	}
	return true;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t;
	cin >> t;
	for (int a = 0; a < t; a++) {
		string num1, num2;
		int sum = 0;
		cin >> num1;
		sum += changeToNum(num1);
		sum += changeToStr(num1);
		if (checkPel(sum)) {
			cout << "YES";
		}
		else {
			cout << "NO";
		}
		if (a != t - 1) cout << '\n';

	}
}