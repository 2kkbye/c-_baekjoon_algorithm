#include<iostream>

using namespace std;

int main() {
	int test_num;
	cin >> test_num;

	if (test_num >= 90 && test_num <= 100) {
		cout << "A";
	}
	else if (test_num>=80 && test_num<=89) {
		cout << "B";
	}
	else if (test_num >= 70 && test_num <= 79) {
		cout << "C";
	}
	else if (test_num >= 60 && test_num <= 69) {
		cout << "D";
	}
	else {
		cout << "F";
	}

	return 0;
}

//윤년은 4의 배수는 맞고, 