#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int T,N,M;
	cin >> T;
	for (int a = 0; a < T; a++) {
		cin >> N >> M;
		cout << ((M * 2) - N) << " " << M - ((M * 2) - N);
		if (a != T - 1) cout << endl;
	}
}