#include<iostream>
#define SIZE 10001

using namespace std;

bool prime_number[SIZE];
void Solve() {
	for (int a = 0; a < SIZE; a++) {
		prime_number[a] = true;
	}
	prime_number[1] = false;
	for (int a = 2; a < SIZE; a++) {
		if (!prime_number[a]) continue;
		int temp = a;
		int c = 2;

		while (temp * c < SIZE) {
			prime_number[temp * c] = false;
			c++;
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();

	int t,num,temp1,temp2,res1=0,res2=0;
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> num;

		int index = 1;
		int dis = num;
		while (true) {
			index++;
			if (index >num/2) break;
			if (!prime_number[index]) continue;
			if ((index * 2) == num) {
				res1 = index, res2 = index;
				break;
			}
			temp1 = index;
			if (!prime_number[num - temp1]) continue;
			temp2 = num - temp1;
			if (dis > temp2 - temp1) {
				dis = temp2 - temp1;
				res1 = temp1;
				res2 = temp2;
			}
			
		}
		cout << res1 << " " << res2 << endl;
	}

}