#include<iostream>
#define SIZE 10001

using namespace std;

int num[SIZE];
int temp[SIZE];
bool visit[SIZE];
int N;
bool check = false;
void Solve(int level) {
	if (level == N + 1) {
		if (check) {
			for (int a = 1; a <= N; a++) {
				cout << temp[a];
				if (a != N)cout << " ";
			}
			exit(0);
		}
		check =true;
		return;
	}
	for (int a = 1; a <= N; a++) {
		if (visit[a]) continue;
		if (num[level] != a && !check) continue;
		visit[a] = true;
		temp[level] = a;
		Solve(level + 1);
		visit[a] = false;
	}
}
bool last_check() {
	for (int a = 2; a <= N; a++) {
		if (num[a-1] - num[a] == 1) continue;
		return false;
	}
	return true;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	cin >> N;
	for (int a = 1; a <= N; a++) {
		cin >> num[a];
		temp[a] = 0;
	}
	if (last_check()) {
		cout << -1;
		return 0;
	}
	for (int a = num[1]; a <= num[1]+1; a++) {
		temp[1] = a;
		visit[a] = true;
		Solve(2);
		visit[a] = false;
	}
}