#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string str;
	int n;
	cin >> n;
	for (int a = 0; a < n; a++) {
		cin >> str;
		cout << str[0] << str[str.size() - 1];
		if (a == n - 1) break;
		cout << '\n';
	}
}