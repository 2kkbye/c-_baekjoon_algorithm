#include<iostream>
#define SIZE 11
#include<vector>

using namespace std;

int num[SIZE];
vector<int> v_mul;
vector<int> select_mul;
bool mulVisit[SIZE];
int N;
int ansMax = -1000000000, ansMin = 1000000000;

void init() {
	for (int a = 0; a < SIZE; a++) {
		num[a] = 0;
		mulVisit[a] = false;
	}
}
void Input() {
	cin >> N;

	for (int a = 0; a < N; a++) {
		cin >> num[a];
	}
	int temp;
	for (int a = 0; a < 4; a++) {
		cin >> temp;
		for (int b = 0; b < temp; b++) {
			v_mul.push_back(a);
		}
	}
}
void check() {
	int num1 = num[0];
	int index = 0;
	for (int a = 1; a < N; a++) {
		if (select_mul[index] == 0) {
			num1 = num1 + num[a];
		}
		else if (select_mul[index] == 1) {
			num1 = num1 - num[a];
		}
		else if (select_mul[index] == 2) {
			num1 = num1 * num[a];
		}
		else {
			num1 = num1 / num[a];
		}
		index++;
	}
	if (num1 > ansMax) {
		ansMax = num1;
	}
	if (num1 < ansMin) {
		ansMin = num1;
	}
}
void dfs(int level) {
	if (level == N - 1) {
		check();
		return;
	}
	for (int a = 0; a < v_mul.size(); a++) {
		if (mulVisit[a]) continue;
		mulVisit[a] = true;
		select_mul.push_back(v_mul[a]);
		dfs(level + 1);
		select_mul.erase(select_mul.end() - 1, select_mul.end());
		mulVisit[a] = false;
	}
}
void Solve() {
	init();
	Input();
	dfs(0);
	cout << ansMax<<endl;
	cout << ansMin;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();
}