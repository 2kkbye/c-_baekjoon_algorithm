#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N, student,apple,sum = 0;

	cin >> N;
	for (int a = 0; a < N; a++) {
		cin >> student >> apple;
		sum += (apple % student);
	}

	cout << sum;

}