#include<iostream>
#define SIZE 500

struct TORNADO {
	int x, y, dir, set, limit;
};
using namespace std;

int dx[4] = { 0,1,0,-1 };
int dy[4] = { -1,0,1,0 };
int map[SIZE][SIZE];

int N, ans = 0;
bool checkAns = false;
TORNADO to;

void Init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
		}
	}
}
void Input() {
	cin >> N;
	for (int a = 1; a <= N; a++) {
		for (int b = 1; b <= N; b++) {
			cin >> map[a][b];
		}
	}
	to.x = (N / 2) + 1;
	to.y = to.x;
	to.dir = 0;
	to.set = 1;
	to.limit = 1;
}
void moveTornado() {
	if (to.x == 1 && to.y == 1) {
		checkAns = true;
		return;
	}
	to.x += dx[to.dir];
	to.y += dy[to.dir];
	to.limit--;
}
void changeTornado() {
	bool check = false;
	if (to.limit == 0) {
		to.dir++;
		to.limit = to.set;
		check = true;
	}
	if ((to.dir == 2 || to.dir == 4) && check) {
		to.set++;
		to.limit = to.set;
		if (to.dir == 4) {
			to.dir = 0;
		}
	}
}
void Print() {
	cout << "=========================" << endl;
	for (int a = 1; a <= N; a++) {
		for (int b = 1; b <= N; b++) {
			cout << map[a][b] << " ";
		}
		cout << endl;
	}
}
void sumTornado() {
	int total = map[to.x][to.y];
	int temp_total = total;
	map[to.x][to.y] = 0;
	//숫자가 10보다 작으면 분배를 할 필요가 없다.
	if (total < 10) {
		int px = to.x + dx[to.dir];
		int py = to.y + dy[to.dir];
		if (px< 1 || py < 1 || px >N || py >N) {
			ans += total;
		}
		else {
			map[px][py] += total;
		}
		return;
	}
	int px = to.x;
	int py = to.y;
	int temp;
	//자신의 방향으로 2칸을 검사한다.
	for (int a = 1; a <= 2; a++) {
		int rx = px + (dx[to.dir] * a);
		int ry = py + (dy[to.dir] * a);
		if (a == 1) {
			temp = total * 0.55;
		}
		else {
			temp = total * 0.05;
		}
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry > N) {
			ans += temp;
			continue;
		}
		else {
			map[rx][ry] += temp;
		}
	}
	//상하좌우 2칸씩 전체 검색
	for (int a = 0; a <= 3; a++) {
		if (to.dir % 2 == 0) {
			if (a % 2 == 0) continue;
		}
		else {
			if (a % 2 != 0) continue;
		}
		for (int b = 1; b <= 2; b++) {
			int rx = px + (dx[a] * b);
			int ry = py + (dy[a] * b);
			if (b == 1) {
				temp = total * 0.07;
			}
			else {
				temp = total * 0.02;
			}
			temp_total -= temp;
			if (rx < 1 || ry < 1 || rx > N || ry > N) {
				ans += temp;
				continue;
			}
			else {
				map[rx][ry] += temp;
			}
		}
	}
	if (to.dir == 0) {
		int rx = px - 1;
		int ry = py - 1;
		int temp = total * 0.1;
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry >N) {
			ans += temp;
		}
		else {
			map[rx][ry] += temp;
		}
		rx = px + 1;
		ry = py - 1;
		temp = total * 0.1;
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry >N) {
			ans += temp;
		}
		else {
			map[rx][ry] += temp;
		}
		rx = px - 1;
		ry = py + 1;
		temp = total * 0.01;
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry >N) {
			ans += temp;
		}
		else {
			map[rx][ry] += temp;
		}
		rx = px + 1;
		ry = py + 1;
		temp = total * 0.01;
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry >N) {
			ans += temp;
		}
		else {
			map[rx][ry] += temp;
		}
	}
	//오른쪽
	else if (to.dir == 2) {
		int rx = px - 1;
		int ry = py + 1;
		int temp = total * 0.1;
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry >N) {
			ans += temp;
		}
		else {
			map[rx][ry] += temp;
		}
		rx = px + 1;
		ry = py + 1;
		temp = total * 0.1;
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry >N) {
			ans += temp;
		}
		else {
			map[rx][ry] += temp;
		}
		rx = px - 1;
		ry = py - 1;
		temp = total * 0.01;
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry >N) {
			ans += temp;
		}
		else {
			map[rx][ry] += temp;
		}
		rx = px + 1;
		ry = py - 1;
		temp = total * 0.01;
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry >N) {
			ans += temp;
		}
		else {
			map[rx][ry] += temp;
		}
	}
	else if (to.dir == 1) {
		int rx = px + 1;
		int ry = py - 1;
		int temp = total * 0.1;
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry >N) {
			ans += temp;
		}
		else {
			map[rx][ry] += temp;
		}
		rx = px + 1;
		ry = py + 1;
		temp = total * 0.1;
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry >N) {
			ans += temp;
		}
		else {
			map[rx][ry] += temp;
		}
		rx = px - 1;
		ry = py - 1;
		temp = total * 0.01;
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry >N) {
			ans += temp;
		}
		else {
			map[rx][ry] += temp;
		}
		rx = px - 1;
		ry = py + 1;
		temp = total * 0.01;
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry >N) {
			ans += temp;
		}
		else {
			map[rx][ry] += temp;
		}
	}
	//위를 바라볼때
	else if (to.dir == 3) {
		int rx = px - 1;
		int ry = py - 1;
		int temp = total * 0.1;
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry >N) {
			ans += temp;
		}
		else {
			map[rx][ry] += temp;
		}
		rx = px - 1;
		ry = py + 1;
		temp = total * 0.1;
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry >N) {
			ans += temp;
		}
		else {
			map[rx][ry] += temp;
		}
		rx = px + 1;
		ry = py - 1;
		temp = total * 0.01;
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry >N) {
			ans += temp;
		}
		else {
			map[rx][ry] += temp;
		}
		rx = px + 1;
		ry = py + 1;
		temp = total * 0.01;
		temp_total -= temp;
		if (rx < 1 || ry < 1 || rx > N || ry >N) {
			ans += temp;
		}
		else {
			map[rx][ry] += temp;
		}
	}
	int rx = to.x + dx[to.dir];
	int ry = to.y + dy[to.dir];
	if (rx < 1 || ry < 1 || rx > N || ry > N) {
		ans += temp_total;
	}
	else {
		map[rx][ry] += temp_total;
	}
}
void Solution() {
	while (!checkAns) {
		//cout << to.x << " " << to.y << endl;
		//Print();
		moveTornado();
		sumTornado();
		changeTornado();
	}
	cout << ans;
}
void Solve() {
	Init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}