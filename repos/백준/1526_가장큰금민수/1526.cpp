#include<iostream>
#include<string>
#include <stdlib.h>

using namespace std;

bool checkNum(int num) {
	string str = to_string(num);
	for (int a = 0; a < str.length(); a++) {
		if (str[a] == '4' || str[a] == '7') continue;
		return false;
	}
	return true;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int input;
	cin >> input;
	for (int a = input; a >= 4; a--) {
		if (checkNum(a)) { cout << a; break; }
	}
}