#include<iostream>
#define SIZE 7

using namespace std;

int num[2][SIZE];
void init() {
	for (int a = 0; a < 2; a++) {
		for (int b = 0; b < SIZE; b++) {
			num[a][b] = -1;
		}
	}
}
int changeNum() {
	int sum = 0;
	int n;
	int index,div;
	for (int a = 0; a < 2; a++) {
		index = -1;
		n = 0;
		for (int b = SIZE - 1; b >= 0; b--) {
			if (num[a][b] == -1) continue;
			index++;
			div = 1;
			for (int c = 0; c < index; c++) {
				div *= 10;
			}
			n = n + (div * num[a][b]);
		}
		sum += n;
	}
	return sum;
}
void Solve() {
	for (int a = 0; a < 2; a++) {
		for (int b = 0; b < SIZE; b++) {
			if (num[a][b] == -1) break;
			if (num[a][b] == 5) num[a][b] = 6;

		}
	}
	int max = changeNum();

	for (int a = 0; a < 2; a++) {
		for (int b = 0; b < SIZE; b++) {
			if (num[a][b] == -1) break;
			if (num[a][b] == 6) num[a][b] = 5;

		}
	}
	int min = changeNum();
	cout << min << " " << max;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	init();
	string str;
	for (int a = 0; a < 2; a++) {
		cin >> str;
		for (int b = 0; b < str.length(); b++) {
			num[a][b] = (int)str[b] - 48;
		}
	}
	Solve();
}