#include<iostream>
#define ROW 31
#define COL 20

using namespace std;
int map[ROW][COL];
bool visit[ROW][COL];
int ans = 4;
int N, M;
void Init() {
	for (int a = 0; a < ROW; a++) {
		for (int b = 0; b < COL; b++) {
			map[a][b] = 0;
			visit[a][b] = false;
		}
	}
}
void Input() {
	int H, num1, num2;
	cin >> M >> H >> N;
	N = N + 1;
	M = (M * 2) - 1;
	for (int a = 0; a < H; a++) {
		cin >> num1 >> num2;
		map[num1][num2 * 2] = 1;
	}
}
bool check() {
	for (int a = 1; a <= M; a += 2) {
		int rx = 1;
		int ry = a;

		while (true) {
			if (rx == N) break;
			if (map[rx][ry - 1] == 1) {
				ry = ry - 2;
				rx = rx + 1;
			}
			else if (map[rx][ry + 1] == 1) {
				rx = rx + 1;
				ry = ry + 2;
			}
			else {
				rx = rx + 1;
			}
		}
		if (a == ry) continue;
		else return false;
	}
	return true;
}
void dfs(int level, int x) {
	if (check()) {
		if (level < ans) {
			ans = level;
		}
		return;
	}
	if (level == 3) {
		return;
	}
	for (int a = x; a < N; a++) {
		for (int b = 2; b < M; b += 2) {
			if (map[a][b] != 0) continue;
			if (map[a][b - 2] == 1) continue;
			if (map[a][b + 2] == 1) continue;
			if (visit[a][b]) continue;
			visit[a][b] = true;
			map[a][b] = 1;
			dfs(level + 1, a);
			map[a][b] = 0;
			visit[a][b] = false;
		}
	}
}
void Solution() {
	dfs(0, 1);
	if (ans == 4) {
		cout << -1;
	}
	else {
		cout << ans;
	}
}
void Solve() {
	Init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();
}