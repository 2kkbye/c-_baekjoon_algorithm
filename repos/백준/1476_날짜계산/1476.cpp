#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int E, S, V;
	cin >> E >> S >> V;
	int e=1, s=1, v=1;
	long year=1;
	while (true) {
		if ((E == e) && (S == s) && (V == v)) break;
		e++; s++; v++;
		if (e > 15) e = 1;
		if (s > 28) s = 1;
		if (v > 19) v = 1;
		year++;
	}
	cout << year;

}