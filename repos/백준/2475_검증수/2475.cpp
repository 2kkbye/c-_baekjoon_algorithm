#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int num, res = 0;
	for (int a = 0; a < 5; a++) {
		cin >> num;
		res += (num * num);
	}
	cout << res % 10;
}