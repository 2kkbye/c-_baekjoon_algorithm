#include<iostream>
#define SIZE 101
using namespace std;

int arr[SIZE][SIZE] = {0,};
int n, m;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t;
	cin >> t;
	int res;
	for (int a = 0; a < t; a++) {
		res = 0;
		cin >> n >> m;
		int temp;
		for (int b = 0; b < n; b++) {
			for (int c = 0; c < m; c++) {
				cin >> temp;
				arr[b][c] = temp;
			}
		}
		for (int b = 0; b < m; b++) {
			int cnt = 0;
			for (int c = 0; c < n; c++) {
				if (arr[c][b] == 0) continue;
				int cnt = 0;
				for (int d = c + 1; d < n; d++) {
					if (arr[d][b] == 1) cnt++;
				}
				res += (n - 1 - c - cnt);
			}
		}
		cout << res;
		if (a != t - 1) cout<<endl;
	}
}