#include<iostream>
#define SIZE 10
using namespace std;

int A[SIZE];
int B[SIZE];
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int a_win = 0, b_win = 0;
	for (int a = 0; a < SIZE; a++) {
		cin >> A[a];
	}
	for (int a = 0; a < SIZE; a++) {
		cin >> B[a];
	}
	for (int a = 0; a < SIZE; a++) {
		if (A[a] > B[a]) a_win++;
		else if(A[a]<B[a]) b_win++;
	}
	if (a_win > b_win) cout << "A";
	else if (a_win < b_win) cout << "B";
	else cout << "D";
}