#include<iostream>
#include<vector>
#define SIZE 13

struct point {
	int x, y, dir, num, rank;
};
using namespace std;

int map[SIZE][SIZE];
int moveMap[SIZE][SIZE];

int dx[5] = {0,0, 0, -1, 1};
int dy[5] = {0,1, -1, 0, 0};

vector<point> v_number;
int N, K;
int ans = 1001;
void init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
			moveMap[a][b] = 0;
		}
	}
}
void Input() {
	cin >> N >> K;
	N += 1;
	for (int a = 1; a < N; a++) {
		for (int b = 1; b < N; b++) {
			cin >> map[a][b];
		}
	}
	point p;
	for (int a = 1; a <= K; a++) {
		cin >> p.x >> p.y >> p.dir;
		p.num = a;
		p.rank = 1;
		v_number.push_back(p);
		moveMap[p.x][p.y] = 1;
	}
}
bool checkmoveMap() {
	for (int a = 1; a < N; a++) {
		for (int b = 1; b < N; b++) {
			if (moveMap[a][b] >= 4) {
				return true;
			}
		}
	}
	return false;
}
int findIndexVector(int n) {
	for (int a = 0; a < v_number.size(); a++) {
		if (v_number[a].num == n) {
			return a;
		}
	}
}
int findIndexRank(int rx, int ry,int rk) {
	for (int a = 0; a < v_number.size(); a++) {
		if (v_number[a].x == rx && v_number[a].y == ry && v_number[a].rank == rk) {
			return a;
		}
	}
}
void moveNumCheck(int index) {
	point p = v_number[index];
	int rx = p.x + dx[p.dir];
	int ry = p.y + dy[p.dir];
	int rdir = p.dir;
	bool change = false;
	if (rx < 1 || ry < 1 || rx >= N || ry >= N) {
		change = true;
		if (rdir % 2 == 1) {
			rdir += 1;
		}
		else {
			rdir -= 1;
		}
		rx = p.x + dx[rdir];
		ry = p.y + dy[rdir];
		v_number[index].dir = rdir;
	}

	if (map[rx][ry] == 0) {
		//자신의 위에 아무것도 없음.
		if (moveMap[p.x][p.y] == p.rank) {
			moveMap[p.x][p.y]--;
			moveMap[rx][ry]++;
			p.x = rx; p.y = ry; p.dir = rdir;
			p.rank = moveMap[rx][ry];
			v_number[index] = p; return;
		}
		else {
			int max = moveMap[p.x][p.y];
			for (int a = p.rank; a <= max; a++) {
				int ind = findIndexRank(p.x, p.y, a);
				moveMap[p.x][p.y]--;
				v_number[ind].x = rx;
				v_number[ind].y = ry;
				moveMap[rx][ry]++;
				v_number[ind].rank = moveMap[rx][ry];
			}
			return;
		}
	}
	else if (map[rx][ry] == 1) {
		//자신의 위에 아무것도 없음.
		if (moveMap[p.x][p.y] == p.rank) {
			moveMap[p.x][p.y]--;
			moveMap[rx][ry]++;
			p.x = rx; p.y = ry; p.dir = rdir;
			p.rank = moveMap[rx][ry];
			v_number[index] = p; return;
		}
		else {
			int max = moveMap[p.x][p.y];
			for (int a = max; a >= p.rank; a--) {
				int ind = findIndexRank(p.x, p.y, a);
				moveMap[p.x][p.y]--;
				v_number[ind].x = rx;
				v_number[ind].y = ry;
				moveMap[rx][ry]++;
				v_number[ind].rank = moveMap[rx][ry];
			}
			return;
		}
	}
	else if (map[rx][ry] == 2) {
		if (rdir % 2 == 1) {
			rdir += 1;
		}
		else {
			rdir -= 1;
		}
		if (change) {
			v_number[index].dir = rdir;
			return;
		}
		rx = p.x + dx[rdir];
		ry = p.y + dy[rdir];
		if (rx < 1 || ry < 1 || rx >= N || ry >= N) {
			v_number[index].dir = rdir;
			return;
		}
		v_number[index].dir = rdir;
		if (map[rx][ry] == 2) {
			return;
		}
		if (map[rx][ry] == 0) {
			//자신의 위에 아무것도 없음.
			if (moveMap[p.x][p.y] == p.rank) {
				moveMap[p.x][p.y]--;
				moveMap[rx][ry]++;
				p.x = rx; p.y = ry; p.dir = rdir;
				p.rank = moveMap[rx][ry];
				v_number[index] = p; return;
			}
			else {
				int max = moveMap[p.x][p.y];
				for (int a = p.rank; a <= max; a++) {
					int ind = findIndexRank(p.x, p.y, a);
					moveMap[p.x][p.y]--;
					v_number[ind].x = rx;
					v_number[ind].y = ry;
					moveMap[rx][ry]++;
					v_number[ind].rank = moveMap[rx][ry];
				}
				return;
			}
		}
		else if (map[rx][ry] == 1) {
			//자신의 위에 아무것도 없음.
			if (moveMap[p.x][p.y] == p.rank) {
				moveMap[p.x][p.y]--;
				moveMap[rx][ry]++;
				p.x = rx; p.y = ry; p.dir = rdir;
				p.rank = moveMap[rx][ry];
				v_number[index] = p; return;
			}
			else {
				int max = moveMap[p.x][p.y];
				for (int a = max; a >= p.rank; a--) {
					int ind = findIndexRank(p.x, p.y, a);
					moveMap[p.x][p.y]--;
					v_number[ind].x = rx;
					v_number[ind].y = ry;
					moveMap[rx][ry]++;
					v_number[ind].rank = moveMap[rx][ry];
				}
				return;
			}
		}
	}
}
void Print() {
	cout << "================================="<<endl;
	for (int a = 1; a < N; a++) {
		for (int b = 1; b < N; b++) {
			cout << moveMap[a][b] << " ";
		}
		cout << endl;
	}
}
void Solution() {
	bool res = false;
	for (int a = 1; a < 1001; a++) {
		//cout << "*********************************************" << endl;
		for (int b = 1; b <= K; b++) {
			//if (a == 7) {
				//cout << "check" << endl;
			//}
			//Print();
			if (checkmoveMap()) {
				ans = a;
				res = true;
				break;
			}
			int index = findIndexVector(b);
			moveNumCheck(index);
		}
		if (res) break;
		
	}

	if (ans == 1001) {
		cout << -1;
	}
	else {
		cout << ans;
	}
}
void Solve() {
	init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();
}