#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t,index;
	string str;
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> index >> str;
		for (int a = 0; a < str.size(); a++) {
			if ((a + 1) == index) continue;
			cout << str[a];
		}
		if (a == t - 1) break;
		cout << '\n';

	}
}