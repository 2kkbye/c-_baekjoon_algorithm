#include<iostream>
#define SIZE 50

using namespace std;

int map[SIZE][SIZE];
int copyMap[SIZE][SIZE];
bool visit[SIZE][SIZE];
bool copyVisit[SIZE][SIZE];

int N, L, R;
int dx[4] = { 0,1,0,-1 };
int dy[4] = { 1,0,-1,0 };

void init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
			visit[a][b] = false;
			copyMap[a][b] = -1;
			copyVisit[a][b] = false;
		}
	}
}
void Input() {
	cin >> N >> L >> R;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			cin >> map[a][b];
		}
	}
}
void dfs(int x, int y) {
	for (int a = 0; a < 4; a++) {
		int rx = x + dx[a];
		int ry = y + dy[a];
		if (rx >= 0 && ry >= 0 && rx < N && ry < N) {
			if (visit[rx][ry]) continue;
			if (copyVisit[rx][ry]) continue;
			int temp = map[x][y] - map[rx][ry];
			if (temp < 0) temp *= -1;
			if (temp >= L && temp <= R) {
				visit[rx][ry] = true;
				dfs(rx, ry);
			}
		}
	}
}

void copyToMap() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			copyVisit[a][b] = false;
			if (copyMap[a][b] == -1) continue;
			map[a][b] = copyMap[a][b];
			copyMap[a][b] = -1;
		}
	}
}
void visitInit() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			copyVisit[a][b] = visit[a][b];
			visit[a][b] = false;
		}
	}
}
void checkPopultaion() {
	int count = 0;
	int sum = 0;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			if (!visit[a][b]) continue;
			count++;
			sum += map[a][b];
		}
	}
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			if (!visit[a][b]) continue;
			copyMap[a][b] = sum / count;
		}
	}
}

bool checkSpread(int x, int y) {
	for (int a = 0; a < 4; a++) {
		int rx = x + dx[a];
		int ry = y + dy[a];
		if (rx >= 0 && ry >= 0 && rx < N && ry < N) {
			if (visit[rx][ry]) continue;
			if (copyVisit[rx][ry]) continue;
			int temp = map[x][y] - map[rx][ry];
			if (temp < 0) temp *= -1;
			if (temp >= L && temp <= R) {
				return true;
			}
		}
	}
	return false;
}
void Solution() {
	int ans = 0;
	for (int s = 0; s <= 2000; s++) {
		bool type = false;
		for (int a = 0; a < N; a++) {
			for (int b = 0; b < N; b++) {
				if (copyVisit[a][b]) continue;
				if (!checkSpread(a, b)) {
					copyVisit[a][b] = true;
					continue;
				}
				visit[a][b] = true;
				dfs(a, b);
				type = true;
				checkPopultaion();
				visitInit();
			}
		}
		if (!type) {
			ans = s;
			break;
		}
		copyToMap();
	}
	cout << ans;
}
void Solve() {
	init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}