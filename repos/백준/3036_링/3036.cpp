#include<iostream>
#define SIZE 100

using namespace std;

int num[SIZE];
int Solve(int num1, int num2) {

	int temp;
	while (true) {
		if (num1 % num2 == 0) {
			return num2;
		}
		temp = num1 % num2;
		num1 = num2;
		num2 = temp;
	}
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int N;
	cin >> N;
	for (int a = 0; a < N; a++) {
		cin >> num[a];
	}
	int gcd;
	for (int a = 1; a < N; a++) {
		gcd = Solve(num[0], num[a]);
		cout << num[0] / gcd << "/" << num[a] / gcd;
		if (a != N - 1) cout << endl;
	}

}