#include<iostream>
#include<vector>
#define SIZE 1001

using namespace std;

bool leave_check(int index, vector<int> room, vector<int> leave) {
	for (int a = 0; a < room.size(); a++) {
		if (room[a] == leave[index]) return true;
	}
	return false;
}

int find_index(int index, vector<int> room, vector<int> leave) {
	for (int a = 0; a < room.size(); a++) {
		if (room[a] == leave[index]) return a;
	}
	return 0;
}
vector<int> solution(vector<int> enter, vector<int> leave) {
	vector<int> answer;
	int res[SIZE];
	for (int a = 0; a < SIZE; a++) {
		res[a] = 0;
	}
	vector<int> room;
	int leave_index = 0;
	int input_index = 0;
	while (leave_index<leave.size()) {
		while (true) {
			if (leave_check(leave_index, room, leave)) {
				int index = find_index(leave_index++, room, leave);
				res[room[index]] = res[room[index]] + room.size() - 1;
				room.erase(room.begin() + index);
				for (int a = 0; a < room.size(); a++) {
					res[room[a]]++;
				}
			}
			else {
				break;
			}
		}
		if (input_index < enter.size()) {
			room.push_back(enter[input_index++]);
		}
		
	}
	for (int a = 1; a <= enter.size(); a++) {
		answer.push_back(res[a]);
	}
	return answer;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	vector<int> enter;
	vector<int> leave;
	enter.push_back(1);
	enter.push_back(4);
	enter.push_back(2);
	enter.push_back(3);
	leave.push_back(2);
	leave.push_back(1);
	leave.push_back(3);
	leave.push_back(4);
	vector<int> result;
	result = solution(enter, leave);
	for (int a = 0; a < result.size(); a++) {
		cout << result[a] << " ";
	}

}