#include<iostream>
#define SIZE 100

using namespace std;

int arr[SIZE][SIZE] = { 0, };

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	int N, M,temp;
	cin >> N >> M;
	for (int c = 0; c < 2; c++) {
		for (int a = 0; a < N; a++) {
			for (int b = 0; b < M; b++) {
				cin >> temp;
				arr[a][b] += temp;
			}
		}
	}
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			cout << arr[a][b];
			if (b == M - 1)break;
			cout << " ";
		}
		if (a == N - 1)break;
		cout << '\n';
	}
}