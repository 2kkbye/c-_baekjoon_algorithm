#include<iostream>
#define SIZE 1000001

using namespace std;
bool prime[SIZE];
void solve() {
	for (int a = 2; a < SIZE; a++) {
		prime[a] = true;
	}
	for (int a = 2; a < SIZE; a++) {
		if (!prime[a]) continue;
		int size = 2;
		while (a * size < SIZE) {
			prime[a * size] = false;
			size++;
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int num;
	solve();
	int res_a, res_b;
	bool check;
	while (true) {
		cin >> num;
		if (num == 0) break;
		check = false;
		for (int a = 3; a < SIZE; a += 2) {
			if (!prime[a]) continue;
			if (!prime[num - a]) continue;
			check = true;
			res_a = a, res_b = num - a;
			break;
		}
		if (!check) {
			printf("Goldbach's conjecture is wrong.\n");
		}
		else {
			printf("%d = %d + %d\n", num, res_a, res_b);
		}
	}
}