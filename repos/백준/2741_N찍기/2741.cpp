#include <iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N;
	cin >> N;
	for (int a = N; a >= 1; a--) {
		cout << a;
		if (a == 1)break;
		cout << '\n';
	}
}