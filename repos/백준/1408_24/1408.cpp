#include<iostream>
#include<stdlib.h>

using namespace std;

int input_time[3][3];

void init() {
	for (int a = 0; a < 3; a++) {
		for (int b = 0; b < 3; b++) {
			input_time[a][b] = 0;
		}
	}
}
void inputNum(string check, int index) {
	string temp = "";
	int input = 0;
	for (int a = 0; a < check.length(); a++) {
		if (a == 2 || a == 5) {
			int a = atoi(temp.c_str());
			temp = "";
			input_time[index][input] = a;
			input++;
			continue;
		}
		temp = temp + check[a];
	}
	input_time[index][input] = atoi(temp.c_str());
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	init();
	string temp;
	for (int a = 0; a < 2; a++) {
		cin >> temp;
		inputNum(temp, a);
	}
	int mok = 0;
	int tem = 0;
	for (int a = 2; a >= 0; a--) {
		tem = input_time[1][a] - mok - input_time[0][a];
		if (tem < 0) {
			mok = 1;
			if (a == 0)tem += 24;
			else tem += 60;
		}
		else {
			mok = 0;
		}
		input_time[2][a] = tem;
	}

	for (int a = 0; a < 3; a++) {
		int temp = input_time[2][a];
		if (temp < 10) {
			cout << "0";
		}
		cout << temp;
		if (a == 2) break;
		cout << ":";
	}
}