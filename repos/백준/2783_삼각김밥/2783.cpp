#include<iostream>
#define SIZE 101
using namespace std;

float res[SIZE];

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	float x, y, n;
	cin >> x >> y;

	res[0] = ((x/y) * 1000);
	cin >> n;
	for (int a = 1; a <= n; a++) {
		cin >> x >> y;
		res[a] = ((x / y) * 1000);
	}
	float min = 999999999;
	for (int a = 0; a <= n; a++) {
		if (min > res[a]) min = res[a];
	}
	cout << min;
}