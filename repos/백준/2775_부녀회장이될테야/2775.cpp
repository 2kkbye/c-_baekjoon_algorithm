#include<iostream>
#define SIZE 15
using namespace std;

int arr[SIZE][SIZE];
void Solve() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 1; b < SIZE; b++) {
			if (a == 0) {
				arr[a][b] = b;
			}
			else {
				if (b == 1) {
					arr[a][b] = arr[a - 1][b];
					continue;
				}
				arr[a][b] = arr[a - 1][b] + arr[a][b - 1];
			}
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int T;
	cin >> T;
	int k, n;
	Solve();
	for (int a = 0; a < T; a++) {
		cin >> k >> n;
		cout << arr[k][n];
		if (a != T - 1) cout << endl;
	}
}