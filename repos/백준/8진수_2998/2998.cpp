#include<iostream>
#include <stdlib.h>
#include<string>
using namespace std;
string str[] = { "000","001","010","011","100","101","110","111" };
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string t_num;
	cin >> t_num;
	int need_zero;
	if (t_num.size() % 3 != 0) {
		need_zero = 3 - (t_num.size() % 3);
		for (int a = 0; a < need_zero; a++) {
			t_num = '0' + t_num;
		}
	}
	string temp = "";
	string res = "";
	for (int a = 0; a < t_num.size(); a++) {
		temp += t_num[a];
		if (a != 0 && (a + 1) % 3 == 0) {
			for (int b = 0; b < 8; b++) {
				if (temp != str[b]) continue;
				res = res+ to_string(b);
				break;
			}
			temp = "";
		}
	}
	cout << res;

}