#include<iostream>
#include<queue>
#define SIZE 201

int map[SIZE];
bool visit[SIZE];

using namespace std;
queue<int> q_robot;
int N, K, sIdx, eIdx;
void Init() {
	for (int a = 0; a < SIZE; a++) {
		map[a] = 0;
		visit[a] = false;
	}
}
void Input() {
	cin >> N >> K;
	sIdx = 1; eIdx = N;
	for (int a = 1; a <= 2*N; a++) {
		cin >> map[a];
	}
}
void moveBelt() {
	sIdx--;
	eIdx--;
	if (sIdx == 0) {
		sIdx = 2 * N;
	}
	if (eIdx == 0) {
		eIdx = 2 * N;
	}
}
void robotBelt() {
	int index = q_robot.size();
	for (int a = 0; a < index; a++) {
		int num = q_robot.front();
		q_robot.pop();
		if (num == eIdx) {
			visit[num] = false;
			continue; 
		}
		
		int temp = num + 1;
		if (temp == (2 * N) + 1) {
			temp = 1;
		}
		if (visit[temp] || map[temp] == 0) {
			q_robot.push(num);
			continue;
		}
		visit[num] = false;
		map[temp]--;
		if (temp != eIdx) {
			q_robot.push(temp);
			visit[temp] = true;
		}
	}
}
void makeRobot() {
	if (!visit[sIdx] && map[sIdx] > 0) {
		visit[sIdx] = true;
		q_robot.push(sIdx);
		map[sIdx]--;
	}
}
bool checkAns() {
	int count = 0;
	for (int a = 1; a <= 2*N; a++) {
		if (map[a] == 0) count++;
	}
	if (count >= K) return true;
	return false;
}
void Solution() {
	int ans = 0;
	while (true) {
		ans++;
		moveBelt();
		robotBelt();
		makeRobot();
		if (checkAns()) break;
	}
	cout << ans;
}
void Solve() {
	Init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}