#include<iostream>
#define SIZE 30000

using namespace std;

int numA[SIZE];
int numB[SIZE];
int maxLenA;
int maxLenB;
int resLen;
//false 이면 +, true = -
bool minus_A = false;
bool minus_B = false;

//init array func
void init() {
	for (int a = 0; a < SIZE; a++) {
		numA[a] = 0;
		numB[a] = 0;
	}
}
//A와 B의 길이가 같을경우 누가 더 숫자가 큰지 확인
bool checkSize() {
	for (int a = SIZE-resLen; a < SIZE; a++) {
		if (numA[a] > numB[a]) break;
		if (numA[a] == numB[a]) continue;
		//true이면 B가 더 큰경우
		return true;
	}
	return false;
}
//B가 더 크거나, 긴 숫자일경우 A와 B 배열 변경
void changeBtoA() {
	int temp;
	bool temp2;
	temp2 = minus_A;
	minus_A = minus_B;
	minus_B = temp2;
	for (int a = SIZE - resLen; a < SIZE; a++) {
		if (numA[a] == numB[a]) continue;
		temp = numA[a];
		numA[a] = numB[a];
		numB[a] = temp;
	}
}
void inputNum(string num, int index, int len) {
	int ind = 0;
	if (index == 0) {
		for (int a = SIZE - len; a < SIZE; a++) {
			if (num[ind] == '-') { ind++; }
			numA[a] = (int)num[ind++] - 48;
		}
	}
	else {
		ind = 0;
		for (int a = SIZE - len; a < SIZE; a++) {
			if (num[ind] == '-') { ind++; }
			numB[a] = (int)num[ind++] - 48;
		}
	}
}
void plus_num() {
	int temp = 0;
	int pn = 0;
	for (int a = SIZE-1; a >= SIZE-resLen-1; a--) {
		temp = numA[a] + numB[a] + pn;
		pn = 0;
		if (temp >= 10) {
			pn = 1;
		}
		numA[a] = temp % 10;
	}
}
void minus_num() {
	int temp;
	int pn = 0;
	for (int a = SIZE - 1; a >= SIZE - resLen; a--) {
		temp = numA[a] - numB[a] + pn;
		pn = 0;
		if (temp < 0) {
			pn = -1;
			temp += 10;
		}
		numA[a] = temp;
	}
}
bool checkRet() {
	for (int a = SIZE - resLen; a < SIZE; a++) {
		if (numA[a] == 0) continue;
		return false;
	}
	return true;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string a, b;
	cin >> a >> b;
	maxLenA = a.length();
	maxLenB = b.length();
	init();
	if (a[0] == '-') { minus_A = true; maxLenA--;}
	if (b[0] == '-') { minus_B = true; maxLenB--;}
	resLen = maxLenA;
	if (maxLenA < maxLenB) resLen = maxLenB;
	inputNum(a, 0,maxLenA); inputNum(b, 1,maxLenB);
	bool ch = false;
	if (maxLenA == maxLenB) {
		if (checkSize()) ch = true;
	}
	if ((ch || maxLenB > maxLenA) && minus_A!=minus_B) { changeBtoA(); }

	if (minus_A == minus_B) {
		plus_num();
	}
	else {
		minus_num();
	}
	if (checkRet()) {
		cout << "0"; return 0;
	}
	if (minus_A) cout << "-";
	bool res = false;
	for (int a = 0; a < SIZE; a++) {
		if (numA[a] == 0 && !res) continue;
		res = true;
		cout << numA[a];
	}
}