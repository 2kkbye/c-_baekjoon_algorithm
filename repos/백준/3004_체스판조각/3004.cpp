#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int ans = 2, temp = 2;
	int check = 0;
	int n;
	cin >> n;
	for (int a = 2; a <= n; a++) {
		if (check == 2) {
			temp++;
			check = 1;
			ans += temp;
			continue;
		}
		check++;
		ans += temp;
	}
	cout << ans;
}