#include<iostream>
#define SIZE 15

using namespace std;

int length;
int depth;
int num[SIZE];
bool visit[SIZE];
void init() {
	for (int a = 0; a < SIZE; a++) {
		num[a] = 0;
		visit[a] = false;
	}
}
void quick(int start, int end) {
	int s = start;
	int e = end;
	int pivot = num[(s + e) / 2];
	do {
		while (pivot > num[s]) s++;
		while (pivot < num[e]) e--;
		if (s <= e) {
			int temp = num[s];
			num[s] = num[e];
			num[e] = temp;
			s++; e--;
		}

	} while (s <= e);
	if (s < end) {
		quick(s, end);
	}
	if (start < e) {
		quick(start, e);
	}
}
bool checkNum() {
	//true면 암호로 가능, false면 암호로 불가능
	//aNum = 모음개수, bNum= 자음개수
	int aNum = 0, bNum = 0;
	for (int a = 0; a < length; a++) {
		if (!visit[a]) continue;
		if (num[a] == (int)'a' || num[a] == (int)'e' ||
			num[a] == (int)'i' || num[a] == (int)'o' || num[a] == (int)'u') {
			aNum++;
		}
		else {
			bNum++;
		}
	}
	if (aNum >= 1 && bNum >= 2) return true;
	else return false;
}
void print() {
	for (int a = 0; a < length; a++) {
		if (!visit[a]) continue;
		cout << (char)num[a];
	}
	cout << '\n';
}
void dfs(int level, int index) {
	if (level == depth) {
		if (checkNum()) {
			print();
		}
		return;
	}
	for (int a = index; a < length; a++) {
		if (visit[a]) continue;
		visit[a] = true;
		dfs(level + 1, a + 1);
		visit[a] = false;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	init();
	cin >> depth >> length;
	char temp;
	
	for (int a = 0; a < length; a++) {
		cin >> temp;
		num[a] = (int)temp;
	}
	quick(0,length-1);
	for (int a = 0; a <= length - depth; a++) {
		visit[a] = true;
		dfs(1, a + 1);
		visit[a] = false;
	}
}