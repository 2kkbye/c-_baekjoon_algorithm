#include<iostream>
#define SIZE 1000000

using namespace std;

int num[SIZE];

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int n;
	cin >> n;
	
	int ans = n;
	for (int a = 4; a <= n; a++) {
		int temp = 0;
		for (int b = 2; b < a; b++) {
			if (a % b == 0) temp++;
		}
		ans += (temp / 2);
		if ((temp % 2) == 1) ans++;
	}
	cout << ans;
}