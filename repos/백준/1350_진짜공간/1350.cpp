#include<iostream>
#define SIZE 1000

using namespace std;

int file_size[SIZE];
int cluster;
void init() {
	for (int a = 0; a < SIZE; a++) {
		file_size[a] = 0;
	}
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	init();
	int N, file;
	cin >> N;
	for (int a = 0; a < N; a++) {
		cin >> file_size[a];
	}
	cin >> cluster;
	long long ans = 0;
	int mok;
	for (int a = 0; a < N; a++) {
		if (file_size[a] == 0) continue;
		if (file_size[a] <= cluster) {
			ans += cluster;
			continue;
		}
		mok = file_size[a] / cluster;
		if ((file_size[a] % cluster) != 0) mok++;
		ans += (cluster * mok);
	}
	cout << ans;
}