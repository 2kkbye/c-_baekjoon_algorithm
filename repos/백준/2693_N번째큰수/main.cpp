#include<iostream>
#include<vector>
#define SIZE 10
using namespace std;

int num[SIZE];
vector<int> v_ans;
void Init() {
	for (int a = 0; a < SIZE; a++) {
		num[a] = 0;
	}
}
void quickSort(int start, int end) {
	int s = start;
	int e = end;
	int pivot = num[(start + end) / 2];
	do {
		while (num[s] < pivot) s++;
		while (num[e] > pivot) e--;
		if (s <= e) {
			int temp = num[s];
			num[s] = num[e];
			num[e] = temp;
			s++; e--;
		}
	} while (s <= e);
	if (s < end) {
		quickSort(s, end);
	}
	if (start < e) {
		quickSort(start, e);
	}
}
void Input() {
	int t;
	cin >> t;
	for (int a = 0; a < t; a++) {
		for (int b = 0; b < 10; b++) {
			cin >> num[b];
		}
		quickSort(0, SIZE - 1);
		v_ans.push_back(num[SIZE - 3]);
	}
}
void Solve() {
	Input();
	for (int a = 0; a < v_ans.size(); a++) {
		cout << v_ans[a];
		if (a != v_ans.size() - 1) {
			cout << endl;
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}