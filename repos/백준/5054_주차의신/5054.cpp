#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t;
	cin >> t;
	for (int a = 0; a < t; a++) {
		vector<int> dis;
		int ans = 0;
		int n;
		cin >> n;
		for (int b = 0; b < n; b++) {
			int temp;
			cin >> temp;
			dis.push_back(temp);
		}
		sort(dis.begin(), dis.end());
		for (int b = 1; b < dis.size(); b++) {
			ans += (dis[b] - dis[b - 1]);
		}
		ans += (dis[dis.size() - 1] - dis[0]);
		cout << ans;
		if (a != t - 1) cout << '\n';
	}
}