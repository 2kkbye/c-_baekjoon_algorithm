#include<iostream>
#include <algorithm>
#include <vector>
#define SIZE 100

using namespace std;

int num[SIZE];
int res[SIZE];
int N;
void quick(int start, int end) {
	int s = start;
	int e = end;
	int pivot = num[(start + end) / 2];
	do {
		while (num[s] < pivot) s++;
		while (num[e] > pivot) e--;
		if (s <= e) {
			int temp = num[s];
			num[s] = num[e];
			num[e] = temp;
			s++; e--;
		}
	} while (s <= e);
	if (s < end) {
		quick(s, end);
	}
	if (start < e) {
		quick(start, e);
	}
}
int gcd(int x, int y) {
	if (x < y) {
		int temp = x;
		x = y;
		y = temp;
	}
	while (y != 0) {
		int num = x % y;
		x = y;
		y = num;
	}
	return x;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cin >> N;
	for (int a = 0; a < N; a++) {
		cin >> num[a];
	}
	quick(0, N - 1);
	int gc = num[1] - num[0];
	for (int a = 2; a < N; a++) {
		gc = gcd(gc, num[a] - num[a - 1]);
	}
	vector<int> v;
	for (int a = 2; a * a <= gc; a++) {
		if (gc % a == 0) {
			v.push_back(a);
			v.push_back(gc / a);
		}
	}

	v.push_back(gc);
	sort(v.begin(), v.end());
	v.erase(unique(v.begin(), v.end()), v.end());
	for (int a = 0; a < v.size(); a++) printf("%d ", v[a]);
	printf("\n");
}