#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t, n, k, can,ans;
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> n >> k;
		ans = 0;
		for (int b = 0; b < n; b++) {
			cin >> can;
			ans += (can / k);
		}
		cout << ans;
		if (a != t - 1) cout << endl;

	}
}