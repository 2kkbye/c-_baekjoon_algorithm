#include<iostream>
#define SIZE 20
using namespace std;

int a_st[SIZE];
int b_st[SIZE];

void init() {
	for (int a = 0; a < SIZE; a++) {
		a_st[a] = -1;
		b_st[a] = -1;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	//char ch = 'A';
	//cout << (int)ch-64;

	int t;
	cin >> t;
	string x, y;
	for (int a = 0; a < t; a++) {
		cin >> x >> y;
		cout << "Distances: ";
		for (int b = 0; b < x.size(); b++) {
			int res;
			int num_x = (int)x[b] - 64;
			int num_y = (int)y[b] - 64;
			if (num_y < num_x) {
				num_y += 26;
			}
			res = num_y - num_x;
			cout << res;
			if (b != x.size() - 1) cout << " ";
		}
		if (a != t - 1) cout << endl;
	}
}