#include<iostream>
#define SIZE 13

using namespace std;

bool visit[SIZE];
int num[SIZE];
int length;
void init() {
	for (int a = 0; a < SIZE; a++) {
		visit[a] = false;
		num[a] = 0;
	}
}
void print() {
	for (int a = 0; a < length; a++) {
		if (!visit[a]) continue;
		cout << num[a] << " ";
	}
	cout << '\n';
}
void dfs(int level, int index) {
	if (level == 6) {
		print();
		return;
	}
	for (int a = index; a < length; a++) {
		if (visit[a]) continue;
		visit[a] = true;
		dfs(level + 1,a+1);
		visit[a] = false;
	}

}
void Solve() {
	for (int a = 0; a <= (length - 6); a++) {
		visit[a] = true;
		dfs(1, a+1);
		visit[a] = false;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int temp;
	while (true) {
		cin >> length;
		init();
		if (length == 0) break;
		for (int a = 0; a < length; a++) {
			cin >> temp;
			num[a] = temp;
		}
		Solve();
		cout << '\n';
	}
}