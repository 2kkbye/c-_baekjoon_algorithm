#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N,res =0,num_5=0,num_3=0;
	cin >> N;
	//int temp = N;
	if (N % 5 == 0) {
		cout << N / 5; return 0;
	}
	if ((N % 5) % 3 == 0) {
		cout << (N / 5) + 1;
		return 0;
	}
	num_5 = N / 5;
	num_3 = 1;
	if (N % 5 != 1) {
		num_3++;
	}
	while (num_5>=0) {
		if (N == ((num_3 * 3 + num_5 * 5))) {
			break;
		}
		num_5--;
		num_3++;
	}
	if (num_5 == -1) {
		cout << -1;
		return 0;
	}
	cout << (num_5 + num_3);
}