#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

    int a;
    int b;
    int c; // a-b
    int v;
    int result = 1;

    cin >> a >> b >> v;
    if ((v - a) != 0) {
        c = a - b;
        v = v - b;
        if (v % c == 0) {
            result = (v / c);
        }
        else {
            result = (v / c) + 1;
        }
    }
    cout << result << endl;

    return 0;
}