#include<iostream>

using namespace std;


int changetoNum(int num) {
	int temp = 100,sum=0;
	for (int a = 0; a < 3; a++) {
		sum = sum + ((num % 10) * temp);
		num /= 10;
		temp /= 10;
	}
	return sum;
}

int Solve(int num1, int num2) {
	int res_1 = changetoNum(num1), res_2 = changetoNum(num2);
	//cout << res_1 << " " << res_2<<'\n';
	if (res_1>res_2) {
		return res_1;
	}
	else {
		return res_2;
	}
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int num1, num2;

	cin >> num1 >> num2;

	cout << Solve(num1, num2);
}