#include<iostream>
#define SIZE 100
using namespace std;

int num[SIZE];
void init() {
	for (int a = 0; a < SIZE; a++) {
		num[a] = 0;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int te,sum=0;
	init();
	for (int a = 0; a < 10; a++) {
		cin >> te;
		sum += te;
		num[te / 10]++;
	}
	int max_cnt = -1,max_num;
	for (int a = 1; a < SIZE; a++) {
		if (num[a] > max_cnt) {
			max_num = a * 10;
			max_cnt = num[a];
		}
	}
	cout << sum / 10 << endl;
	cout << max_num;
}