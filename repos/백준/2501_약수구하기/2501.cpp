#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N, K;
	cin >> N >> K;
	if (K == 1) {
		cout << 1; return 0;
	}
	int index = 1;
	int ans=0;
	for (int a = 2; a <= N; a++) {
		if (N % a != 0) continue;
		index++;
		if (index == K) {
			ans = a;
			break;
		}
	}
	cout << ans;
}