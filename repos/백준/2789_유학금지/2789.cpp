#include<iostream>

using namespace std;

int check[9];

void init() {
	string str = "CAMBRIDGE";
	for (int a = 0; a < str.size(); a++) {
		check[a] = (int)str[a];
	}
}
bool check_ch(char c) {
	int num = (int)c;
	for (int a = 0; a < 9; a++) {
		if (check[a] == num) return true;
	}
	return false;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	init();
	string str;
	cin >> str;
	for (int a = 0; a < str.size(); a++) {
		if (check_ch(str[a]))continue;
		cout << str[a];
	}
}