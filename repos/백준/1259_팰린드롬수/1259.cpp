#include<iostream>

using namespace std;

bool Solve(string str) {
	for (int a = 0; a < str.size() / 2; a++) {
		if (str[a] == str[str.size() - a - 1]) continue;
		return false;
	}
	return true;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string N;
	while (true) {
		cin >> N;
		if (N == "0") break;
		if (Solve(N)) {
			cout << "yes";
		}
		else {
			cout << "no";
		}
		cout << '\n';
	}
}