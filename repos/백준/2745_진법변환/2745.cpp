#include<iostream>
#define SIZE 26
using namespace std;

int B;
char ch[SIZE] = {'A','B','C','D','E','F','G','H','I','J','K','L',
'M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

int check_index(char c) {
	for (int a = 0; a < SIZE; a++) {
		if (ch[a] != c) continue;
		return a + 10;
	}
}
int Solve(int index) {
	int sum = 1;
	for (int a = 0; a < index; a++) {
		sum *= B;
	}
	return sum;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string str;
	cin >> str;
	cin >> B;
	int ans = 0;
	for (int a = 0; a < str.length(); a++) {
		int num;
		char t = str[a];
		if (t >= '0' && t <= '9') {
			num = (int)str[a] - 48;
		}
		else {
			num = check_index(str[a]);
		}
		ans = ans + (num * Solve(str.length() - 1-a));
	}
	cout << ans;
}