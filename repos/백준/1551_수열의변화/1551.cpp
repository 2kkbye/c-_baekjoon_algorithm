#include<iostream>
#include<vector>
#include <stdlib.h>

#define SIZE 20

using namespace std;

vector<int> num;
vector<int> tempNum;

void Solve() {
	tempNum.clear();
	int temp;
	for (int a = 1; a < num.size(); a++) {
		temp = num[a] - num[a - 1];
		tempNum.push_back(temp);
	}
	num.clear();
	for (int a = 0; a < tempNum.size(); a++) {
		num.push_back(tempNum[a]);
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int n, k;
	cin >> n >> k;

	string str;
	cin >> str;
	int temp = 1;
	// int a = atoi(str.c_str());
	string inputNum = "";
	for (int a = 0; a < str.length(); a++) {
		if (str[a] == ',') { 
			int t = atoi(inputNum.c_str()) * temp;
			num.push_back(t);
			if (temp == -1) 
				temp = 1;
			inputNum = "";
			continue; 
		}
		if (str[a] == '-') { temp = -1; continue; }
		inputNum += str[a];
	}
	num.push_back(atoi(inputNum.c_str()) * temp);
	for (int a = 0; a < k; a++) {
		Solve();
	}
	for (int a = 0; a < num.size(); a++) {
		cout << num[a];
		if (a != num.size() - 1)cout << ",";
	}
}