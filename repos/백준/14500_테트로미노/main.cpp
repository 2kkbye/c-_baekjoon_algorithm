#include<iostream>
#define SIZE 500

using namespace std;
int map[SIZE][SIZE];
bool visit[SIZE][SIZE];

int dx[4] = { -1,0,1,0 };
int dy[4] = { 0,1,0,-1 };

int dir[8][4] = { {0,1,1,1},{0,-1,0,1},{0,1,1,2},{0,0,1,0},{0,1,1,2},{0,-1,0,0},{0,0,0,1},{0,-1,1,0} };
int tans = 0;
int ans = -1;
int N, M;
void init() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			map[a][b] = 0;
			visit[a][b] = false;
		}
	}
}
void Input() {
	cin >> N >> M;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			cin >> map[a][b];
		}
	}
}
void dfs(int level, int x, int y) {
	if (level == 4) {
		if (tans > ans) {
			ans = tans;
		}
		return;
	}
	for (int a = 0; a < 4; a++) {
		int rx = x + dx[a];
		int ry = y + dy[a];
		if (rx >= 0 && ry >= 0 && rx < N && ry < M) {
			if (visit[rx][ry]) continue;
			visit[rx][ry] = true;
			tans += map[rx][ry];
			dfs(level + 1, rx, ry);
			tans -= map[rx][ry];
			visit[rx][ry] = false;
		}
	}
}
void checkDir(int x, int y) {
	for (int a = 0; a < 8; a += 2) {
		bool type = true;
		int sum = 0;
		for (int b = 0; b < 4; b++) {
			int rx = x + dir[a][b];
			int ry = y + dir[a + 1][b];

			if (rx >= 0 && ry >= 0 && rx < N && ry < M) {
				sum += map[rx][ry];
			}
			else {
				type = false;
				break;
			}
		}
		if (type) {
			if (sum > ans) {
				ans = sum;
			}
		}
	}
}
void Solution() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			visit[a][b] = true;
			tans += map[a][b];
			dfs(1, a, b);
			tans -= map[a][b];
			visit[a][b] = false;
		}
	}
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			checkDir(a, b);
		}
	}
	cout << ans;
}
void Solve() {
	init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();

}