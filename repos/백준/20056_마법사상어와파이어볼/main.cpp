#include<iostream>
#include<queue>
#include<vector>
#define SIZE 51

struct FIRE {
	int x, y, m, d, s;
};

using namespace std;
int map[SIZE][SIZE];
int copyMap[SIZE][SIZE];
int dx[8] = {-1,-1,0,1,1,1,0,-1};
int dy[8] = {0,1,1,1,0,-1,-1,-1};
queue<FIRE> q_fire;
vector<FIRE> v_temp;
int N,M,K;

void Init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
			copyMap[a][b] = 0;
		}
	}
}
void Input() {
	cin >> N >> M >> K;
	FIRE F;
	for (int a = 0; a < M; a++) {
		cin >> F.x >> F.y >> F.m >> F.s >> F.d;
		q_fire.push(F);
		map[F.x][F.y] = 1;
	}
}
void copyInit() {
	for (int a = 1; a <= N; a++) {
		for (int b = 1; b <= N; b++) {
			map[a][b] = copyMap[a][b];
			copyMap[a][b] = 0;
		}
	}
}
void fireBallMove() {
	int size = q_fire.size();
	for (int a = 0; a < size; a++) {
		FIRE F = q_fire.front();
		q_fire.pop();
		int px = F.x;
		int py = F.y;
		px = px + (dx[F.d] * (F.s % N));
		py = py + (dy[F.d] * (F.s % N));
		if (px > N) {
			px -= N;
		}
		else if (px < 1) {
			px += N;
		}
		if (py < 1) {
			py += N;
		}
		else if (py > N) {
			py -= N;
		}
		copyMap[px][py]++;
		F.x = px;
		F.y = py;
		q_fire.push(F);
	}
}
void checkBall() {
	v_temp.clear();
	int size = q_fire.size();
	for (int a = 0; a < size; a++) {
		FIRE F = q_fire.front();
		q_fire.pop();
		if (copyMap[F.x][F.y] == 1) {
			q_fire.push(F);
			continue;
		}
		else {
			v_temp.push_back(F);
		}
	}
}
void makeSumBall() {
	vector<int> temp;
	for (int a = 1; a <= N; a++) {
		for (int b = 1; b <= N; b++) {
			if (copyMap[a][b] <= 1) continue;
			temp.clear();
			//행과 열이 똑같은 임시 vector의 인덱스 찾기.
			for (int c = 0; c < v_temp.size(); c++) {
				if (v_temp[c].x == a && v_temp[c].y == b) {
					temp.push_back(c);
				}
			}
			//
			int sumM = 0, sumS = 0; bool checkD = true;
			for (int c = 0; c < temp.size(); c++) {
				sumM += v_temp[temp[c]].m;
				sumS += v_temp[temp[c]].s;
				if (c == 0) continue;
				if (!checkD) continue;
				if (v_temp[temp[c]].d % 2 != v_temp[temp[c - 1]].d % 2) checkD = false;
			}
			FIRE F;
			int td;
			if (checkD) td = 0;
			else td = 1;
			for (int c = 0; c < 4; c++) {
				F.x = a; F.y = b; F.m = sumM / 5; F.s = sumS / temp.size();
				F.d = td;
				td += 2;
				q_fire.push(F);
			}
		}
	}
}
void eraseBall() {
	int size = q_fire.size();
	for (int a = 0; a < size; a++) {
		FIRE F = q_fire.front();
		q_fire.pop();
		if (F.m == 0) continue;
		q_fire.push(F);
	}
}
int checkAns() {
	int ans = 0;
	int size = q_fire.size();
	for (int a = 0; a < size; a++) {
		FIRE F = q_fire.front();
		q_fire.pop();
		ans += F.m;
	}
	return ans;
}
void Solution() {
	for (int a = 0; a < K; a++) {
		copyInit();
		fireBallMove();
		checkBall();
		makeSumBall();
		eraseBall();
	}
}
void Solve() {
	Init();
	Input();
	Solution();
	cout << checkAns();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}