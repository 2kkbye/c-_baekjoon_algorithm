#include<iostream>

using namespace std;

int N;

bool Solve(int num) {
	int sum = num;
	while (num > 0) {
		sum += num % 10;
		num /= 10;
	}
	if (sum == N) return true;
	else return false;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	cin >> N;
	int res = 0;
	for (int a = 1; a < N; a++) {
		if (!Solve(a))continue;
		res = a;
		break;
	}
	cout << res;
}