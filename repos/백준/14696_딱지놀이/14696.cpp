#include<iostream>
#define SIZE 5

using namespace std;

//0=A,1=B
int arr[2][SIZE];

void init() {
	for (int a = 0; a < SIZE; a++) {
		arr[0][a] = 0;
		arr[1][a] = 0;
	}
}
char Solve() {
	for (int a = 4; a >= 1; a--) {
		if (arr[0][a] == arr[1][a]) continue;
		else if (arr[0][a] > arr[1][a]) return 'A';
		else return 'B';
	}
	return 'D';
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N,tmp,num;
	cin >> N;
	for (int a = 0; a < N; a++) {
		init();
		for (int b = 0; b < 2; b++) {
			cin >> tmp;
			for (int c = 0; c < tmp; c++) {
				cin >> num;
				arr[b][num]++;
			}
		}
		cout << Solve();
		if (a != N - 1) cout << endl;
		
	}

}