#include<iostream>

using namespace std;

int time[] = { 11,2,3,4,5,6,7,8,9,10 };
string alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
int index;
bool Solve(char c, int count) {
	bool check = false;
	int a;
	for (a = index; a < index+count; a++) {
		if (alpha[a] != c) continue;
		check = true;
		if (index == alpha.length() - 1) break;
	}
	index = a;
	return check;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string str;
	cin >> str;
	int cnt, sum = 0;
	for (int a = 0; a < str.length(); a++) {
		index = 0, cnt = 2;
		while (index <alpha.length()) {
			if (cnt == 7 || cnt == 9) {
				if (!Solve(str[a], 4)) cnt++;
				else break;
			}
			else {
				if (!Solve(str[a], 3)) cnt++;
				else break;
			}
		}
		sum += time[cnt];
	}
	cout << sum;
}