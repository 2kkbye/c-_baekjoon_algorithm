#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int in, off, total = 0, max = 0;
	for (int a = 0; a < 10; a++) {
		cin >> off >> in;
		total += in;
		total -= off;
		if (total > max) max = total;
	}
	cout << max;
}