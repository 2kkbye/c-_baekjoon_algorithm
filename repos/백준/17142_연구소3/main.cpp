#include<iostream>
#include<vector>
#include<queue>
#define M_SIZE 50
#define V_SIZE 10

struct VIRUS {
	int x, y;
};
using namespace std;

vector<VIRUS> v_virus;

int map[M_SIZE][M_SIZE];
int copyMap[M_SIZE][M_SIZE];

bool visit[M_SIZE][M_SIZE];
bool selectV[V_SIZE];
int N, M;
int ans = 100000000;

int dx[4] = { 0,1,0,-1 };
int dy[4] = { 1,0,-1,0 };

void init() {
	for (int a = 0; a < M_SIZE; a++) {
		for (int b = 0; b < M_SIZE; b++) {
			map[a][b] = 0;
			visit[a][b] = false;
		}
	}
	for (int a = 0; a < V_SIZE; a++) {
		selectV[a] = false;
	}
}
void copyInit() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			copyMap[a][b] = 0;
			visit[a][b] = false;
		}
	}
}
void Input() {
	cin >> N >> M;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			int temp;
			cin >> temp;
			map[a][b] = temp;
			if (temp == 2) {
				VIRUS v;
				v.x = a;
				v.y = b;
				v_virus.push_back(v);
			}
		}
	}
}
void bfs() {
	copyInit();
	queue<VIRUS> q_virus;
	for (int a = 0; a < v_virus.size(); a++) {
		if (!selectV[a]) continue;
		VIRUS v = v_virus[a];
		visit[v.x][v.y] = true;
		q_virus.push(v);
	}
	while (!q_virus.empty()) {
		VIRUS v = q_virus.front();
		q_virus.pop();
		int px = v.x;
		int py = v.y;
		for (int a = 0; a < 4; a++) {
			int rx = px + dx[a];
			int ry = py + dy[a];
			if (rx >= 0 && ry >= 0 && rx < N && ry < N) {
				if (visit[rx][ry]) continue;
				if (map[rx][ry] == 1) continue;
				VIRUS temp;
				temp.x = rx; temp.y = ry;
				visit[rx][ry] = true;
				q_virus.push(temp);
				copyMap[rx][ry] = copyMap[px][py] + 1;
			}
		}
	}
}
void Print() {
	cout << "=========================" << endl;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			cout << copyMap[a][b] << " ";
		}
		cout << endl;
	}
}
void checkMap() {
	int max = 0;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			if (map[a][b] == 0 && copyMap[a][b] == 0) return;
			if (copyMap[a][b] > max && map[a][b] == 0) {
				max = copyMap[a][b];
			}
		}
	}
	if (ans > max) {
		ans = max;
	}
	return;
}
void dfs(int index, int level) {
	if (level == M) {
		bfs();
		//Print();
		checkMap();
		return;
	}
	for (int a = index; a < v_virus.size(); a++) {
		if (selectV[a]) continue;
		selectV[a] = true;
		dfs(a + 1, level + 1);
		selectV[a] = false;
	}
}
void Solution() {
	dfs(0, 0);
	if (ans == 100000000) {
		cout << -1;
	}
	else {
		cout << ans;
	}
}
void Solve() {
	init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}