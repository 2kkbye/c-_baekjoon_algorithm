#include<iostream>

using namespace std;
#define SIZE 100

int arr[SIZE][SIZE];
void Solve(int H,int W, int N) {
	int cnt = 0;
	for (int a = 1; a <= W; a++) {
		for (int b = H; b >= 1; b--) {
			cnt++;
			if (cnt == N) {
				if (a < 10) {
					cout << (H - b + 1)<<0 << a<<'\n';
				}
				else {
					cout << (H - b + 1) << a<<'\n';
				}
				return;
			}
		}
	}
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int H, W, N,T;
	cin >> T;
	for (int a = 0; a < T; a++) {
		cin >> H >> W >> N;
		Solve(H, W, N);
	}
	

}