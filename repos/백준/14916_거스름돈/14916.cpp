#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int n,ans = -1;
	cin >> n;
	int tMoney, temp = n / 5;
	for (int a = temp; a >= 0; a--) {
		tMoney = ((n - (a * 5)) / 2);
		if (((n - (a * 5)) % 2) == 0) {
			ans = tMoney + a;
			break;
		}
	}
	cout << ans;
}