#include<iostream>
#include<vector>
#define SIZE 1001

using namespace std;

vector<int> ans;
int N, K;
bool visit[SIZE];
int checkNum;
void Init() {
	for (int a = 0; a < N; a++) {
		visit[a] = false;
	}
}
void Input() {
	cin >> N >> K;
	checkNum = N;
	N += 1;
}
void Print() {
	cout << "<";
	for (int a = 0; a < ans.size(); a++) {
		cout << ans[a];
		if (a != ans.size() - 1) {
			cout << ", ";
		}
	}
	cout << ">";
}
void Solve() {
	Init();
	Input();
	int index = 0;
	int ck = 0;
	while (checkNum != 0) {
		index++;
		if (index == N) {
			index = 1;
		}
		if (visit[index]) continue;
		ck++;
		if (ck == K) {
			ck = 0;
			visit[index] = true;
			ans.push_back(index);
			checkNum--;
		}
	}
	Print();
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();
}