#include<iostream>
#define SIZE 1000000
using namespace std;

int room[SIZE];
int N;
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int B, C;
	cin >> N;
	int num;
	for (int a = 0; a < N; a++) {
		cin >> num;
		room[a] = num;
	}
	cin >> B >> C;
	long res = 0;
	for (int a = 0; a < N; a++) {
		res++;
		room[a] -= B;
		if (room[a] <= 0) continue;
		res += (room[a] / C);
		if ((room[a] % C) != 0) res++;
	}
	cout << res;


}