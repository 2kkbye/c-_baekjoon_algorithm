#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int N, num;
	cin >> N;
	int max_num = -1000001, min_num = 1000001;
	for (int a = 1; a <= N; a++) {
		cin >> num;
		if (num > max_num) {
			max_num = num;
		}
		if (num < min_num) {
			min_num = num;
		}
	}
	cout << min_num << " " << max_num;
}