#include<iostream>

using namespace std;
void Solve(int n, int sum,int num) {
	int cnt = sum % n;
	int num2 = 1 + cnt;
	int num1 = num - cnt;
	

	if (num % 2 == 0) {
		cout << num1 << "/" << num2;
	}
	else {
		cout << num2 << "/" << num1;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int n, sum = 0, a = 1;
	cin >> n;
	while (sum < 10000000) {
		sum += a;
		if (sum >= n) {
			Solve(n, sum, a);
			break;
		}
		a++;
	}
}