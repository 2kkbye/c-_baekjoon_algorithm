#include<iostream>
#define SIZE 100000

using namespace std;

int weight[SIZE];
void sort(int start, int end) {
	int s = start;
	int e = end;
	int pivot = weight[(start + end) / 2];
	do {
		while (weight[s] < pivot) s++;
		while (weight[e] > pivot) e--;
		if (s <= e) {
			int temp = weight[s];
			weight[s] = weight[e];
			weight[e] = temp;
			s++; e--;
		}
	}while (s <= e);
	if (start < e) {
		sort(start, e);
	}
	if (s < end) {
		sort(s, end);
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N;
	cin >> N;
	for (int a = 0; a < N; a++) {
		cin >> weight[a];
	}
	sort(0, N - 1);
	long res = 0;
	long sum = 0;
	for (int a = 0; a < N; a++) {
		if (weight[a] > res) res = weight[a];
		sum = weight[a] * (N - a);
		if (sum > res) res = sum;
	}
	cout << res;
}