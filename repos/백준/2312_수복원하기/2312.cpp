#include<iostream>
#define SIZE 99999

using namespace std;

int result[SIZE];
void init() {
	for (int a = 0; a < SIZE; a++) {
		result[a] = 0;
	}
}
void print() {
	for (int a = 2; a < SIZE; a++) {
		if (result[a] == 0) continue;
		cout << a << " " << result[a] << '\n';
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t, num;
	cin >> t;
	for (int a = 0; a < t; a++) {
		init();
		cin >> num;
		int temp = 2;
		while (num > 1) {
			if (num % temp == 0) {
				num /= temp;
				result[temp]++;
				temp = 2;
			}
			else {
				temp++;
			}
		}
		print();
	}
}