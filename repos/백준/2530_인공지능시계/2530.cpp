#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int n_h, n_m, n_s, te_h, te_m, te_s, sec;

	cin >> n_h >> n_m >> n_s;
	cin >> sec;

	te_s = sec % 60;
	int temp = sec / 60;
	te_h = temp / 60;
	te_m = temp % 60;
	int carry = 0;

	n_s += te_s;
	if (n_s >= 60) {
		n_s -= 60;
		carry++;
	}
	n_m += te_m;
	n_m += carry;
	carry = 0;
	if (n_m >= 60) {
		n_m -= 60;
		carry++;
	}
	te_h += carry;
	te_h %= 24;
	n_h += te_h;
	if (n_h >= 24) {
		n_h -= 24;
	}
	cout << n_h << " " << n_m << " " << n_s;
}