#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int num,n;
	int t,ans;
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> n;
		ans = 0;
		cout << n / 25 << " ";
		n %= 25;
		cout << n / 10 << " ";
		n %= 10;
		cout << n / 5 << " ";
		n %= 5;
		cout << n / 1;
		if (a != t - 1) cout << endl;
	}
}