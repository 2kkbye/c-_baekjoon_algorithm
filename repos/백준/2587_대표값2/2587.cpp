#include<iostream>

using namespace std;
int arr[5];
void sort() {
	int temp;
	for (int a = 0; a < 4; a++) {
		for (int b = 0; b < 4 - a; b++) {
			if (arr[b] > arr[b + 1]) {
				temp = arr[b];
				arr[b] = arr[b + 1];
				arr[b + 1] = temp;
			}
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N, sum=0;
	for (int a = 0; a < 5; a++) {
		cin >> N;
		sum += N;
		arr[a] = N;
	}
	sort();
	cout << sum / 5 << '\n' << arr[2];
}