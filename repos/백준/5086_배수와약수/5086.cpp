#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int num1, num2;

	while (true) {
		cin >> num1 >> num2;
		if (num1 == 0 && num2 == 0) break;
		if (num1 > num2) {
			if (num1 % num2 == 0) {
				cout << "multiple"<<'\n';
				continue;
			}
		}
		else {
			if (num2 % num1 == 0) {
				cout << "factor" << '\n';
				continue;
			}
		}
		cout << "neither" << '\n';
	}
}