#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int n, t;
	cin >> n >> t;
	int sum = 0;
	int num,ans = -1;
	for (int a = 1; a <= n; a++) {
		cin >> num;
		sum += num;
		if (sum > t) { ans = a - 1; break; }
	}
	if (ans == -1) ans = n;
	cout << ans;
}