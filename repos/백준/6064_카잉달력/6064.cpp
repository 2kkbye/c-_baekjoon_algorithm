#include<iostream>

using namespace std;
int gcd(int a, int b)
{
	int c;
	while (b != 0)
	{
		c = a % b;
		a = b;
		b = c;
	}

	return a;
}

int lcm(int a, int b)
{
	return a * b / gcd(a, b);
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t, M, N, x, y;
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> M >> N >> x >> y;
		int endDay = lcm(M,N);
		int year = -1;
		for (int i = 0; (M * i) + x <= endDay; i++) {
			int t = ((M * i) + x) % N;
			if (t == 0) t = N;
			if (t == y) {
				year= (M * i) + x;
				break;
			}
		}
		cout << year;
		if (a != t - 1) cout << endl;
	}
}