#include<iostream>
#include<stdlib.h>
using namespace std;

string te[2];
int gcd(int x, int y) {
	while (y != 0) {
		int num = x % y;
		x = y;
		y = num;
	}
	return x;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	string nu;
	cin >> nu;
	int index = 0;
	for (int a = 0; a < nu.length(); a++) {
		if (nu[a] == ':') {
			index++;
			continue;
		}
		te[index] += nu[a];
	}
	int num1, num2;
	bool check = false;
	if (atoi(te[0].c_str()) < atoi(te[1].c_str())) {
		num1 = atoi(te[1].c_str());
		num2 = atoi(te[0].c_str());
		check = true;
	}
	else {
		num1 = atoi(te[0].c_str());
		num2 = atoi(te[1].c_str());
	}
	int ans = gcd(num1, num2);
	num1 /= ans;
	num2 /= ans;
	if (check) {
		cout << num2 << ":" << num1;
	}
	else {
		cout << num1 << ":" << num2;
	}
}