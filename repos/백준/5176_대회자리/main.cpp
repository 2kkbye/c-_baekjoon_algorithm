#include<iostream>
#include<vector>
#define SIZE 501
using namespace std;

bool visit[SIZE];
vector<int> v_ans;
void Init() {
	for (int a = 0; a < SIZE; a++) {
		visit[a] = false;
	}
}
void Solve() {
	int t;
	cin >> t;
	for (int a = 0; a < t; a++) {
		Init();
		int ans = 0;
		int p, m;
		cin >> p >> m;
		int c;
		for (int b = 0; b < p; b++) {
			cin >> c;
			if (visit[c]) ans++;
			else visit[c] = true;
		}
		v_ans.push_back(ans);
	}
	for (int a = 0; a < v_ans.size(); a++) {
		cout << v_ans[a];
		if (a != v_ans.size() - 1) {
			cout << endl;
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}