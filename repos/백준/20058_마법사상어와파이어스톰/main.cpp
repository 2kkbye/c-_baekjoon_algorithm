#include<iostream>
#define SIZE 64

using namespace std;

int map[SIZE][SIZE];
int copyMap[SIZE][SIZE];
bool visit[SIZE][SIZE];

int L_NUM[1000];
int dx[4] = {0,1,0,-1};
int dy[4] = {1,0,-1,0};

int N, Q,L;
long ans = 0;
long tans = 0;
void Init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
			copyMap[a][b] = 0;
			visit[a][b] = false;
		}
	}
	for (int a = 0; a < 1000; a++) {
		L_NUM[a] = 0;
	}
}
int makeL(int num) {
	if (num == 0) {
		return 1;
	}
	int ans = 2;
	for (int a = 1; a < num; a++) {
		ans *= 2;
	}
	return ans;
}

void Input() {
	cin >> N >> Q;
	N = makeL(N);
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			cin >> map[a][b];
		}
	}
	for (int a = 0; a < Q; a++) {
		cin >> L_NUM[a];
	}
}
void Print() {
	cout << "====================================" << endl;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			cout << copyMap[a][b] << " ";
		}
		cout << endl;
	}
}
void Print2() {
	cout << "====================================" << endl;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			cout << map[a][b] << " ";
		}
		cout << endl;
	}
}
void checkSpread() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			int count = 0;
			for (int c = 0; c < 4; c++) {
				int px = a + dx[c];
				int py = b + dy[c];
				if (px < 0 || py < 0 || px >= N || py >= N) continue;
				if (copyMap[px][py] == 0) continue;
				count++;
			}
			if (count < 3) {
				map[a][b] = copyMap[a][b] - 1;
				if (map[a][b] < 0) map[a][b] = 0;
			}
			else {
				map[a][b] = copyMap[a][b];
			}
		}
	}
}
void rotateMetric(int sX, int sY, int L) {
	int dir = 0;
	int x = sX;
	int y = sY;
	int index = 1;
	for (int a = sX; a < sX + L; a++) {
		for (int b = sY; b < sY + L; b++) {
			copyMap[b][L-1-a] = map[a][b];
		}
	}
}
int checkSum() {
	int sum = 0;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			if (map[a][b] <= 0) continue;
			sum += map[a][b];
		}
	}
	return sum;
}
void dfs(int x, int y) {
	tans++;
	for (int c = 0; c < 4; c++) {
		int rx = x + dx[c];
		int ry = y + dy[c];
		if (rx < 0 || ry < 0 || rx >= N || ry >= N) continue;
		if (visit[rx][ry]) continue;
		if (map[rx][ry] <= 0) continue;
		visit[rx][ry] = true;
		dfs(rx, ry);
	}
}
void Solution() {
	for (int d = 0; d < Q; d++) {
		int L = makeL(L_NUM[d]);
		//if (L == 1) continue;
		for (int a = 0; a < N; a+=L) {
			for (int b = 0; b < N; b+=L) {
				rotateMetric(a, b, L);
			}
		}
		Print();
		checkSpread();
	}
	//Print2();
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < N; b++) {
			if (visit[a][b]) continue;
			if (map[a][b] <= 0) continue;
			visit[a][b] = true;
			dfs(a, b);
			if (ans < tans) {
				ans = tans;
			}
			tans = 0;
		}
	}
	
	cout << checkSum()<<endl;
	cout << ans;
}
void Solve() {
	Init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}