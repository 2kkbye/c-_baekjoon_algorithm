#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int x, y, w, h, min_x = 10000, min_y = 10000;
	cin >> x >> y >> w >> h;
	int res_x, res_y;

	if (x < w - x) {
		res_x = x;
	}
	else {
		res_x = w - x;
	}
	if (y < h - y) {
		res_y = y;
	}
	else {
		res_y = h - y;
	}
	if (res_x < res_y) {
		cout << res_x;
	}
	else {
		cout << res_y;
	}
}