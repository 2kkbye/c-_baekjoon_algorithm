#include<iostream>
#define SIZE 9000000

using namespace std;
bool prime_number[SIZE] = { true, };
void Solve(int num) {
	for (int a = 0; a < SIZE; a++) {
		prime_number[a] = true;
	}
	prime_number[1] = false;
	long k = 0;
	for (int a = 2; a < SIZE; a++) {
		if (!prime_number[a]) continue;
		long temp = a;
		long c = 2;
		k++;
		if (k == num) {
			cout << a; return;
		}
		while (temp * c < SIZE) {
			prime_number[temp * c] = false;
			c++;
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int n;
	cin >> n;
	Solve(n);
}