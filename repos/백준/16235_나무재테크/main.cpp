#include<iostream>
#include<queue>
#define SIZE 11

struct tr {
	int x, y, age;
};

using namespace std;

queue<tr> tree;
queue<tr> new_tree;
queue<tr> temp_tree;
queue<tr> dead_tree;

int map[SIZE][SIZE];
int energy[SIZE][SIZE];
int N, K;

int dx[8] = { -1,-1,-1,0,0,1,1,1 };
int dy[8] = { -1,0,1,-1,1,-1,0,1 };

void init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 5;
		}
	}
}
void Input() {
	int M;
	cin >> N >> M >> K;
	for (int a = 1; a <= N; a++) {
		for (int b = 1; b <= N; b++) {
			cin >> energy[a][b];
		}
	}
	for (int a = 0; a < M; a++) {
		tr temp;
		cin >> temp.x;
		cin >> temp.y;
		cin >> temp.age;
		tree.push(temp);
	}
}
void spring() {
	while (tree.size() != 0) {
		tr temp = tree.front();
		tree.pop();
		if (map[temp.x][temp.y] >= temp.age) {
			map[temp.x][temp.y] -= temp.age;
			temp.age++;
			temp_tree.push(temp);
		}
		else {
			dead_tree.push(temp);
		}
	}
}
void summer() {
	while (dead_tree.size() != 0) {
		tr temp = dead_tree.front();
		dead_tree.pop();
		map[temp.x][temp.y] += temp.age / 2;
	}
}
void fall() {
	int size = temp_tree.size();
	for (int a = 0; a < size; a++) {
		tr temp = temp_tree.front();
		temp_tree.pop();
		temp_tree.push(temp);
		if (temp.age % 5 != 0) {
			continue;
		}
		for (int b = 0; b < 8; b++) {
			int rx = temp.x + dx[b];
			int ry = temp.y + dy[b];
			if (rx >= 1 && ry >= 1 && rx <= N && ry <= N) {
				tr new_t;
				new_t.x = rx;
				new_t.y = ry;
				new_t.age = 1;
				new_tree.push(new_t);
			}
		}
	}
}
void winter() {
	for (int a = 1; a <= N; a++) {
		for (int b = 1; b <= N; b++) {
			map[a][b] += energy[a][b];
		}
	}
	int size = new_tree.size();
	for (int a = 0; a < size; a++) {
		tr temp = new_tree.front();
		new_tree.pop();
		tree.push(temp);
	}
	size = temp_tree.size();
	for (int a = 0; a < size; a++) {
		tr temp = temp_tree.front();
		temp_tree.pop();
		tree.push(temp);
	}
}
void Solution() {
	for (int a = 0; a < K; a++) {
		spring();
		summer();
		fall();
		winter();
	}
	cout << tree.size();
}
void Solve() {
	init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}