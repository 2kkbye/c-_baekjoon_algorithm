#include<iostream>
#define SIZE 10


using namespace std;

int num[SIZE];
int color[4];
char co_temp[4] = { 'R','B','Y','G' };
void init() {
	for (int a = 0; a < SIZE; a++) {
		num[a] = 0;
	}
}
int find_minNum() {
	for (int a = 1; a < SIZE; a++) {
		if (num[a] == 0) continue;
		return a;
	}
}
int find_maxNum() {
	for(int a= SIZE-1;a>=1;a--){
		if (num[a] == 0) continue;
		return a;
	}
}
bool check_continue() {
	bool check = false;
	int cnt = 0;
	for (int a = 1; a < SIZE; a++) {
		if (num[a] == 0 && check) return false;
		if (num[a] == 0 && !check) continue;
		cnt++;
		if (cnt == 5) break;
		check = true;
	}
	return true;
}
bool check_color_count(int temp) {
	for (int a = 0; a < 4; a++) {
		if (temp == color[a]) return true;
	}
	return false;
}
int check_num_count2(int temp) {
	for (int a = 1; a < SIZE; a++) {
		if (temp == num[a]) return a;
	}
	return 0;
}
int check_num_count3(int temp) {
	for (int a = SIZE-1; a >=1; a--) {
		if (temp == num[a]) return a;
	}
	return 0;
}
bool check_num_count(int temp) {
	for (int a = 1; a < SIZE; a++) {
		if (temp == num[a]) return true;
	}
	return false;
}
void pirnt(int num) {
	cout << num;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	char co;
	int nu;
	for (int a = 0; a < 5; a++) {
		cin >> co;
		cin >> nu;
		for (int b = 0; b < 4; b++) {
			if (co_temp[b] == co) {
				color[b]++;
				break;
			}
		}
		num[nu]++;
	}
	int ans = 0;
	if (check_color_count(5) && check_continue()) {
		ans += 900;
		ans += (find_maxNum());
		pirnt(ans);
		return 0;
	}
	else if (check_num_count(4)) {
		ans += 800;
		ans += check_num_count2(4);
		pirnt(ans);
		return 0;
	}
	else if (check_num_count(3) && check_num_count(2)) {
		ans += 700;
		ans += ((check_num_count2(3) * 10) + check_num_count2(2));
		pirnt(ans);
		return 0;
	}
	else if (check_color_count(5)) {
		ans += 600;
		ans += (find_maxNum());
		pirnt(ans);
		return 0;
	}
	else if (check_continue()) {
		ans += 500;
		ans += (find_maxNum());
		pirnt(ans);
		return 0;
	}
	else if (check_num_count(3)) {
		ans += 400;
		ans += (check_num_count2(3));
		pirnt(ans);
		return 0;
	}
	else if (check_num_count2(2) != check_num_count3(2)) {
		ans += 300;
		ans += (check_num_count3(2) *10);
		ans += (check_num_count2(2));
		pirnt(ans);
		return 0;
	}
	else if (check_num_count(2)) {
		ans += 200;
		ans += (check_num_count2(2));
		pirnt(ans);
		return 0;
	}
	else {
		ans += 100;
		ans += (find_maxNum());
		pirnt(ans);
		return 0;
	}

}