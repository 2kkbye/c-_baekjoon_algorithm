#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t;
	int r, e, c;
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> r >> e >> c;
		if (r + c == e) {
			cout << "does not matter";
		}
		else if (r + c < e) {
			cout << "advertise";
		}
		else {
			cout << "do not advertise";
		}
		if (a != t - 1) cout << endl;
	}
}