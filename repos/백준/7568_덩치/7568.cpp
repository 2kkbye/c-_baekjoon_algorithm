#include<iostream>

using namespace std;
#define SIZE 50

class Point {
	int x, y,z;
	//x는 몸무게, Y는 키, Z는 등수
public:
	Point() {}
	void setPerson(int x, int y) {
		this->x = x;
		this->y = y;
	}
	void setRes(int z) {
		this->z = z;
	}
	int getX() { return x; }
	int getY() { return y; }
	int getZ() { return z; }
	~Point() {
		//cout << "소멸자 호출"<<'\n';
	}
};
Point p[SIZE];
void Solve(int N) {
	int cnt = 0;
	int a_x, a_y, b_x, b_y;
	for (int a = 0; a < N; a++) {
		cnt = 1;
		for (int b = 0; b < N; b++) {
			if (b == a) continue;
			a_x = p[a].getX(); a_y = p[a].getY(); b_x = p[b].getX(); b_y = p[b].getY();
			if (a_x < b_x && a_y < b_y) {
				cnt++;
			}
		}
		p[a].setRes(cnt);
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	//Point p[SIZE];

	int N,x,y;
	cin >> N;
	for (int a = 0; a < N; a++) {
		cin >> x >> y;
		p[a].setPerson(x, y);
	}
	Solve(N);
	for (int a = 0; a < N; a++) {
		cout << p[a].getZ();
		if (a != N - 1) cout << " ";
	}
	return 0;
}