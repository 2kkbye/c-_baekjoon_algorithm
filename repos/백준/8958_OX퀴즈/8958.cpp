#include<iostream>
#include<string>

using namespace std;

int main() {
	cin.tie(NULL);
	cout.tie(NULL);
	ios::sync_with_stdio(false);
	string str;
	int n;
	cin >> n;
	for (int a = 0; a < n; a++) {
		int sum = 0, cnt = 0;
		cin >> str;
		for (int a = 0; a < str.length(); a++) {
			if (str[a] == 'O') {
				cnt++;
				sum += cnt;
			}
			else {
				cnt = 0;
			}
		}
		cout << sum;
		if (a != n - 1) cout<<'\n';
	}

	return 0;
}