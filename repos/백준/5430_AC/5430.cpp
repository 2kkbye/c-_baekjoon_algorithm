#include<iostream>
#include <stdlib.h>

#define SIZE 100000

using namespace std;

int num[SIZE];
int start = 0, last = 0;
bool type = true;
bool res = true;
void init() {
	start = 0;
	last = 0;
	type = true;
	res = true;
	for (int a = 0; a < SIZE; a++) {
		if (num[a] == 0) continue;
		num[a] = 0;
	}
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t;
	cin >> t;
	string p;
	for (int a = 0; a < t; a++) {
		cin >> p;
		int n;
		string ins;
		cin >> n;
		cin >> ins;
		init();
		string temp = "";
		for (int b = 0; b < ins.length(); b++) {
			if (ins[b] == '[') continue;
			if (ins[b] == ',' || ins[b] == ']' && temp!="") {
				num[last] = atoi(temp.c_str());
				temp = "";
				last++;
				continue;
			}
			temp += ins[b];
		}
		for (int b = 0; b < p.length(); b++) {
			if (p[b] == 'R') {
				if (type) type = false;
				else type = true;
			}
			else {
				if (start == last) {
					res = false;
					break;
				}
				else {
					if (type) start++;
					else last--;
				}
			}
		}
		if (!res) {
			cout << "error";
		}
		else {
			cout << "[";
			if (type) {
				for (int b = start; b < last; b++) {
					cout << num[b];
					if (b != last - 1) cout << ",";
				}
			}
			else {
				for (int b = last-1; b>=start; b--) {
					cout << num[b];
					if (b != start) cout << ",";
				}
			}
			cout << "]";
		}
		if (a != t - 1) cout << '\n';
	}
}