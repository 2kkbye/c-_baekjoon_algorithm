#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string str;

	cin >> str;
	for (int a = 0; a < str.length(); a++) {
		cout << str[a];
		if (a == str.length()-1) break;
		if ((a+1) % 10 == 0) cout << '\n';
	}
}