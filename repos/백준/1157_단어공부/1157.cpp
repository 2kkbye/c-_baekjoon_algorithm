#include<iostream>

using namespace std;
#define SIZE 26

int arr[SIZE];

void init() {
	for (int a = 0; a < SIZE; a++) {
		arr[a] = 0;
	}
}
void print() {
	for (int a = 0; a < SIZE; a++) {
		cout << arr[a]<<" ";
	}
}
void Solve() {
	int max = 0,max_in = 0;

	bool check = false;
	for (int a = 0; a < SIZE; a++) {
		if (arr[a] < max || arr[a]==0)continue;
		if (arr[a] == max) {
			check = true; continue;
		}
		max = arr[a]; check = false;
		max_in = a;
		
	}
	if (check) {
		cout << "?";
	}
	else {
		cout << (char)(max_in+65);
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	string str;
	int index;
	cin >> str;
	init();
	char c;
	for (int a = 0; a < str.length(); a++) {
		c = str[a];
		if (c >= 'a' && c <= 'z') {
			index = ((int)c - 32) % 65;
			arr[index]++;
		}
		else {
			index = (int)c % 65;
			arr[index]++;
		}
	}
	Solve();
	return 0;
}