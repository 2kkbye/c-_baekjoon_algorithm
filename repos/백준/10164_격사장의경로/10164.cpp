#include<iostream>

#define SIZE 16

using namespace std;

int ch_x, ch_y;
int map[SIZE][SIZE];
long cop[SIZE][SIZE];
int N, M,res,ans=0;
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	cin >> N >> M >> res;
	int ind = 1;
	//data input
	//if (res == 0) { cout << "0"; return 0; }
	for (int a = 1; a <= N; a++) {
		for (int b = 1; b <= M; b++) {
			if (ind == res) {
				ch_x = a, ch_y = b;
			}
			cop[a][b] = 0;
			map[a][b] = ind++;
		}
	}
	if (res == 0) {
		ch_x = N; ch_y = M;
	}
	for (int a = 1; a <= ch_x; a++) {
		for (int b = 1; b <= ch_y; b++) {
			if (a == 1 || b == 1) {
				cop[a][b] = 1;
				continue;
			}
			cop[a][b] = cop[a][b - 1] + cop[a - 1][b];
		}
	}
	for (int a = ch_x; a <= N; a++) {
		for (int b = ch_y; b <= M; b++) {
			if (a == ch_x || b == ch_y) {
				cop[a][b] = cop[ch_x][ch_y];
				continue;
			}
			cop[a][b] = cop[a][b - 1] + cop[a - 1][b];
		}
	}
	cout << cop[N][M];
}