#include<iostream>

using namespace std;

int Solve(long x, long y) {
	int ans = 0;
	while (true) {
		if (x % y == 0) { ans = y; break; }
		int temp = x % y;
		x = y;
		y = temp;
	}
	return ans;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	long a_u, a_d, b_u, b_d,res_d,res_u;
	cin >> a_u >> a_d;
	cin >> b_u >> b_d;
	if (a_d < b_d) { //a가 더 큰값으로
		int temp_d, temp_u;
		temp_d = b_d;
		temp_u = b_u;
		b_d = a_d;
		b_u = a_u;
		a_d = temp_d, a_u = temp_u;
	}
	if (b_d == a_d) {
		res_d = b_d;
		res_u = a_u + b_u;
	}
	if (a_d % b_d == 0) { //큰 분모가 작은분모에 나누어 떨어질때
		res_d = a_d;
		res_u = a_u + ((a_d/b_d)*b_u);
	}
	else { //작은수로 나누어 떨어지지 않을때
		res_d = a_d * b_d;
		res_u = (a_d * b_u) + (b_d * a_u);
	}
	int div=-1;
	if (res_d > res_u) {
		div = Solve(res_d, res_u);
	}
	else {
		div = Solve(res_u, res_d);
	}
	res_d /= div;
	res_u /= div;
	cout << res_u << " " << res_d;
}