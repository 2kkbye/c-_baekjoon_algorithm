#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;
vector<vector<int>> v_ans;
vector<int> v_num;
void Input() {
	int t,n, temp;
	cin >> t;
	for (int a = 0; a < t; a++) {
		v_num.clear();
		cin >> n;
		for (int b = 0; b < n; b++) {
			cin >> temp;
			v_num.push_back(temp);
		}
		sort(v_num.begin(), v_num.end());
		v_ans.push_back(v_num);
	}
}
int check(int idx) {
	int max = -1;
	for (int a = v_ans[idx].size()-1; a >=1; a--) {
		if (v_ans[idx][a] - v_ans[idx][a - 1] > max) {
			max = v_ans[idx][a] - v_ans[idx][a - 1];
		}
	}
	return max;
}
void Solve() {
	Input();
	for (int a = 0; a < v_ans.size(); a++) {
		cout << "Class " << a + 1 << endl;
		int max = v_ans[a][v_ans[a].size() - 1];
		int min = v_ans[a][0];
		int checkNum = check(a);
		cout << "Max " << max << ", " << "Min " << min << ", " << "Largest gap " << checkNum;
		if (a != v_ans.size() - 1) cout << endl;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}