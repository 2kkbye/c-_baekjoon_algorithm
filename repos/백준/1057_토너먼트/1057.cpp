#include<iostream>
#define SIZE 100001

using namespace std;

bool arr[SIZE];
void init() {
	for (int a = 1; a < SIZE; a++) {
		arr[a] = true;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N, res_a, res_b;
	cin >> N >> res_a >> res_b;
	if (res_a > res_b) {
		int temp = res_a;
		res_a = res_b;
		res_b = temp;
	}
	init();
	int ans=-1;
	for (int round = 1; round < SIZE; round++) {
		bool check = false;
		int start = -1, end= -1;
		for (int a = 1; a <= N; a++) {
			if (!arr[a]) continue;
			if (start == -1) {
				start = a; continue;
			}
			end = a;
			check = true;
			if (start == res_a && end == res_b) {
				ans = round;
				check = false;
				break;
			}
			else {
				if (end == res_b || end == res_a) {
					arr[start] = false;
				}
				else {
					arr[end] = false;
				}
			}
			start = -1, end = -1;
		}
		if (!check) break;
	}
	printf("%d", ans);
}