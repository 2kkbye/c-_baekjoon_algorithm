#include<iostream>
#include<vector>
#include<queue>

#define SIZE 11

using namespace std;

int person[SIZE];
bool map[SIZE][SIZE];
bool Select[SIZE];
bool visit[SIZE];
int N;
int ans = 987654321;

void init() {
	for (int a = 1; a < SIZE; a++) {
		person[a] = 0;
		Select[a] = false;
		for (int b = 1; b < SIZE; b++) {
			map[a][b] = false;
		}
	}
}
void initVisit() {
	for (int a = 1; a <= N; a++) {
		visit[a] = false;
	}
}
void Input() {
	cin >> N;
	for (int a = 1; a <= N; a++) {
		cin >> person[a];
	}
	for (int a = 1; a <= N; a++) {
		int t, index;
		cin >> t;
		for (int b = 0; b < t; b++) {
			cin >> index;
			map[a][index] = true;
		}
	}
}
bool Check_con(vector<int> v, bool flag) {
	initVisit();
	queue<int> Q;
	visit[v[0]] = true;
	Q.push(v[0]);
	int count = 1;
	while ( Q.size() != 0 ) {
		int temp = Q.front();
		Q.pop();
		
		for (int a = 1; a <= N; a++) {
			if (!visit[a] && map[temp][a] && Select[a] == flag) {
				visit[a] = true;
				Q.push(a);
				count++;
			}
		}
	}
	if (count == v.size()) {
		return true;
	}
	else {
		return false;
	}
}
bool Check() {
	vector<int> v1, v2;
	for (int a = 1; a <= N; a++) {
		if (Select[a]) {
			v1.push_back(a);
		}
		else {
			v2.push_back(a);
		}
	}
	if (v1.size() == 0 || v2.size() == 0) return false;

	if (!Check_con(v1, true)) return false;

	if (!Check_con(v2, false)) return false;

	return true;

}
int countAns() {
	int countT = 0;
	int countF = 0;
	for (int a = 1; a <= N; a++) {
		if (Select[a]) {
			countT += person[a];
		}
		else {
			countF += person[a];
		}
	}
	if (countT > countF) {
		return countT - countF;
	}
	else {
		return countF - countT;
	}
}
void dfs(int index, int cnt) {
	if (cnt >= 1) {
		if (Check()) {
			int tans = countAns();
			if (ans > tans) {
				ans = tans;
			}
		}
	}
	for (int a = index; a <= N; a++) {
		if (Select[a]) continue;
		Select[a] = true;
		dfs(a, cnt + 1);
		Select[a] = false;
	}
}
void Solution() {
	dfs(1, 0);

	if (ans == 987654321) {
		cout << -1;
	}
	else {
		cout << ans;
	}
	return;
}
void Solve() {
	init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();
}