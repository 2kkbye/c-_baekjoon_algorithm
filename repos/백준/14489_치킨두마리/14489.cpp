#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	long a, b, sum = 0, chicken;
	cin >> a >> b;
	sum = a + b;
	cin >> chicken;
	chicken *= 2;
	if (sum >= chicken) {
		cout << sum - chicken;
	}
	else {
		cout << sum;
	}
}