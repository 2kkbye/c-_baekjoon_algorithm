#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	int t, hp, mp, at, de, hp1, mp1, at1, de1;
	int res = 0;
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> hp >> mp >> at >> de >> hp1 >> mp1 >> at1 >> de1;
		hp += hp1; mp += mp1; at += at1; de += de1;
		if (hp < 1) hp=1; if (mp < 1)mp=1; if (at < 0)at = 0;
		cout << (1 * hp) + (5 * mp) + (2 * at) + (2 * de);
		if (a == t - 1) break;
		cout << '\n';
	}

}