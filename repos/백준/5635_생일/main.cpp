#include<iostream>

using namespace std;
struct stu {
	string name;
	int m, d, y;
};
void Solve() {
	int n;
	cin >> n;
	int minY= 2011, minM = 13, minD = 32, maxY = 1989, maxM = -1, maxD = -1;
	string minName, maxName;
	stu s;
	for (int a = 0; a < n; a++) {
		cin >> s.name >> s.d >> s.m >> s.y;
		if (minY > s.y) {
			minY = s.y; minD = s.d; minM = s.m; minName = s.name;
		}
		else if (minY == s.y) {
			if (minM > s.m) {
				minD = s.d; minM = s.m; minName = s.name;
			}
			else if(minM == s.m) {
				if (minD > s.d) {
					minD = s.d; minName = s.name;
				}
			}
		}
		if (maxY < s.y) {
			maxY = s.y; maxD = s.d; maxM = s.m; maxName = s.name;
		}
		else if (maxY == s.y) {
			if (maxM < s.m) {
				maxD = s.d; maxM = s.m; maxName = s.name;
			}
			else if (maxM == s.m) {
				if (maxD < s.d) {
					maxD = s.d; maxName = s.name;
				}
			}
		}
	}
	cout << maxName << endl;
	cout << minName;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}