#include<iostream>
#define SIZE 3
using namespace std;

int num[SIZE];
bool check() {
	int max = -1, max_index = -1;
	for (int a = 0; a < SIZE; a++) {
		if (max < num[a]) {
			max = num[a];
			max_index = a;
		}
	}
	int sum = 0;
	for (int a = 0; a < SIZE; a++) {
		if (max_index == a) continue;
		sum += num[a];
	}
	if (sum <= num[max_index]) { return true; }
	else return false;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);


	while (true) {
		cin >> num[0] >> num[1] >> num[2];
		if (num[0] == 0 && num[1] == 0 && num[2] == 0) break;
		if (num[0] == num[1] && num[1] == num[2]) {
			printf("Equilateral\n");
			continue;
		}
		else if (check()) {
			printf("Invalid\n");
			continue;
		}
		else if (num[0] == num[1] || num[0] == num[2] || num[1] == num[2]) {
			printf("Isosceles\n");
			continue;
		}
		else {
			printf("Scalene\n");
		}
	}
}