#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N;
	cin >> N;
	string num;
	for (int a = 0; a < N; a++) {
		cin >> num;
		
		int t = (int)(num[num.length() - 1]) - 48;
		if (t % 2 == 0) {
			cout << "even";
		}
		else {
			cout << "odd";
		}
		if (a != N - 1) cout << endl;
	}
}