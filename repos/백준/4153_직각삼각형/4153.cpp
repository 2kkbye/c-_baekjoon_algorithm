#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	float A, B, C, max, min1, min2;

	while (true) {
		cin >> A >> B >> C;
		if (A == 0 && B == 0 && C == 0) break;
		if (A > B && A > C) {
			max = A, min1 = B, min2 = C;
		}
		else if (B > A && B > C) {
			max = B, min1 = A, min2 = C;
		}
		else if(C > A && C > B) {
			max = C, min1 = A, min2 = B;
		}
		else {
			cout << "wrong" << endl; continue;
		}
		if ((max * max) == (min1 * min1) + (min2 * min2)) {
			cout << "right" << endl;
		}
		else {
			cout << "wrong" << endl;
		}
	}

}