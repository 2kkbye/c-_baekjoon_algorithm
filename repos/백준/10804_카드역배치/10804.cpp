#include<iostream>
#define SIZE 21

using namespace std;
int num[SIZE];
int temp[SIZE];

void init() {
	for (int a = 1; a < SIZE; a++) {
		num[a] = a;
	}
}
void copy() {
	for (int a = 1; a < SIZE; a++) {
		temp[a] = num[a];
	}
}
void Solve(int s,int e) {
	int start = s;
	int end = e;
	for (int a = 0; a <= e - s; a++) {
		num[start++] = temp[end - a];
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	init();
	int s, e;
	for (int a = 0; a < 10; a++) {
		copy();
		cin >> s >> e;
		Solve(s, e);
	}
	for (int a = 1; a < SIZE; a++) {
		cout << num[a];
		if (a == SIZE - 1) break;
		cout << " ";
	}
}