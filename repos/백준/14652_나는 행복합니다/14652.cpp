#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N, M, K,row=0,index;
	cin >> N >> M >> K;
	index = M - 1;
	while (true) {
		if (index == K) {
			cout << row << " " << M-1;
			return 0;
		}
		if (index < K) {
			index += M;
			row++;
		}
		else {
			for (int a = 1; a < M; a++) {
				if ((index - a) == K) {
					cout << row << " " << M-1 - a;
					return 0;
				}
			}
		}

	}
}