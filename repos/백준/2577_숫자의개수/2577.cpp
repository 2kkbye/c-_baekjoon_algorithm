#include<iostream>

using namespace std;
void solve(int arr[], int num) {
	while (num!=0) {
		arr[num % 10]++;
		num /= 10;
	}
}
void print(int arr[]) {
	for (int a = 0; a < 10; a++) {
		cout << arr[a];
		if (a == 9)break;
		cout << '\n';
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int arr[10] = {};
	int temp,num = 1;
	for (int a = 0; a < 3; a++) {
		cin >> temp;
		num *= temp;
	}
	solve(arr, num);
	print(arr);
}