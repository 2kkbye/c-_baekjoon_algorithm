#include<iostream>

//#include<vector>
//#include<algorithm>
#define SIZE 10000001

using namespace std;
bool prime_number[SIZE];
//vector<int> v;
int num;
void Solve() {
	for (int a = 0; a < SIZE; a++) {
		prime_number[a] = true;
	}
	prime_number[1] = false;
	for (int a = 2; a < SIZE; a++) {
		if (!prime_number[a]) continue;
		int temp = a;
		int c = 2;

		while (temp * c < num) {
			prime_number[temp * c] = false;
			c++;
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
	cin >> num;
	int index = 2;
	while (num!=1) {
		for (int a = 2; a < SIZE; a++) {
			if (!prime_number[a]) continue;
			if (num % a != 0) continue;
			cout << a;
			num /= a;
			if (num != 1)cout << endl;
			break;
		}
	}
}