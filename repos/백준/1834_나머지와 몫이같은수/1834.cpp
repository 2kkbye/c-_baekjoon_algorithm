#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	unsigned long long n;
	cin >> n;
	unsigned long long sum = 0;
	unsigned long long temp = n + 1;
	for (unsigned long long a = temp; a <= 9000000000000; a+=temp) {
		if (a % n == 0) break;
		sum += a;
	}
	cout << sum;
}