#include<iostream>
#define SIZE 101

using namespace std;

bool arr[SIZE];
void init(int N) {
	//false �� ����������
	for (int a = 1; a <= N; a++) {
		arr[a] = true;
	}
	for (int a = 2; a <= N; a += 2) {
		arr[a] = false;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	int T, N;
	cin >> T;
	for (int a = 0; a < T; a++) {
		cin >> N;
		init(N);
		for (int b = 3; b <= N; b++) {
			for (int c = b; c <= N; c += b) {
				if (arr[c]) arr[c] = false;
				else arr[c] = true;
			}
		}
		int ans = 0;
		for (int a = 1; a <= N; a++) {
			if (arr[a])ans++;
		}
		cout << ans;
		if (a != T - 1) cout << endl;
	}

}