#include<iostream>
#define SIZE 16

using namespace std;

int time[SIZE];
int price[SIZE];
int N,sum = 0,ans=0;
void Solve(int day) {
	if (day > N+1) return;
	if (sum > ans) ans = sum;
	for (int a = day; a <= N; a++) {
		sum += price[a];
		Solve(a+time[a]);
		sum -= price[a];
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int t1, t2;
	cin >> N;
	for (int a = 1; a <= N; a++) {
		cin >> t1 >> t2;
		time[a] = t1;
		price[a] = t2;
	}
	for (int a = 1; a <= N; a++) {
		sum += price[a];
		Solve(a+time[a]);
		sum -= price[a];
	}
	cout << ans;
}