#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int n;
	cin >> n;
	int num;
	while (true) {
		cin >> num;
		if (num == 0) break;
		if (num % n == 0) {
			cout << num << " is a multiple of " << n << "."<<endl;
		}
		else {
			cout << num << " is NOT a multiple of " << n << "."<<endl;
		}
	}
}