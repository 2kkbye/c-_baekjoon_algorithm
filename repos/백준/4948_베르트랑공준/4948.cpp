#include<iostream>
#define SIZE 246913
using namespace std;

bool prime_number[SIZE] = {true,};
void Solve() {
	for (int a = 0; a < SIZE; a++) {
		prime_number[a] = true;
	}
	prime_number[1] = false;
	for (int a = 2; a < SIZE; a++) {
		if (!prime_number[a]) continue;
		int temp = a;
		int c = 2;
		while (temp * c <SIZE) {
			prime_number[temp * c] = false;
			c++;
		}
	}
}
int check(int start, int end) {
	int cnt = 0;
	for (int a = start + 1; a <= end; a++) {
		if (prime_number[a]) {
			cnt++;
			//cout << a << endl;
		}
	}
	return cnt;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
	int num;
	while (true) {
		cin >> num;
		if (num == 0) break;
		//check(num, 2 * num);
		cout << check(num, num * 2) << endl;
	}
}