#include<iostream>
#define SIZE 3
using namespace std;
int num[SIZE];
void quick(int start, int end) {
	int s = start;
	int e = end;
	int pivot = num[(start + end) / 2];
	do {
		while (num[s] < pivot) s++;
		while (num[e] > pivot) e--;
		if (s <= e) {
			int temp = num[s];
			num[s] = num[e];
			num[e] = temp;
			s++, e--;
		}
	} while (s <= e);
	if (start < e) {
		quick(start, e);
	}
	if (s < end) {
		quick(s, end);
	}
	
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	for (int a = 0; a < SIZE; a++) {
		cin >> num[a];
	}
	quick(0, SIZE-1);
	int cnt = 1,ans=1;
	for (int a = 1; a < SIZE; a++) {
		if (num[a] == num[a - 1]) {
			cnt++;
			if (cnt > ans) ans = cnt;
			continue;
		}
		cnt = 1;
	}
	int res = 0;
	if (ans == 1) {
		res = num[2] * 100;
	}
	else if (ans == 2) {
		res = 1000+(num[1] * 100);
	}
	else {
		res = 10000 + (num[1] * 1000);
	}
	cout << res;
}