#include<iostream>

using namespace std;
long long gcd(int a, int b) {
	while (b != 0) {
		int r = a % b;
		a = b;
		b = r;
	}
	return a;
}
long long lcm(int a, int b) {
	return (unsigned long long)a * b / gcd(a, b);
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	//a*b = 최소공배수 * 최대공약수
	int n;
	cin >> n;
	int num1, num2;
	for (int a = 0; a < n; a++) {
		cin >> num1 >> num2;
		if (num1 < num2) {
			int temp = num1;
			num1 = num2;
			num2 = temp;
		}
		cout << lcm(num1, num2);
		if (a != n - 1) cout << '\n';
	}
}