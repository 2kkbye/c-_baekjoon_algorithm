#include<iostream>

using namespace std;

bool Solve(string str) {
	bool change, check;
	for (int a = 0; a < str.length()-1; a++) {
		change = true;
		for (int b = a + 1; b < str.length(); b++) {
			if (str[a] == str[b] && change) continue;
			if (str[a] == str[b] && !change) return false;
			if ((str[a] != str[b]) && !change) continue;
			change = false; continue;
		}
	}
	return true;
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	
	int N, res = 0;
	cin >> N;
	string str;

	for (int a = 0; a < N; a++) {
		cin >> str;
		if (Solve(str)) res++;
	}
	cout << res;
}