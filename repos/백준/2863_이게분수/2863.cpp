#include<iostream>
#define SIZE 5

using namespace std;

int dx[SIZE] = { 0,1,-1,0 };
int dy[SIZE] = { 1,0,0,-1 };
int map[2][2];
int copyMap[2][2];
void init() {
	for (int a = 0; a < 2; a++) {
		for (int b = 0; b < 2; b++) {
			map[a][b] = 0;
			copyMap[a][b] = 0;
		}
	}
}
void move() {
	int index = 0;
	for (int a = 0; a < 2; a++) {
		for (int b = 0; b < 2; b++) {
			copyMap[a + dx[index]][b + dy[index]] = map[a][b];
			index++;
		}
	}
	for (int a = 0; a < 2; a++) {
		for (int b = 0; b < 2; b++) {
			map[a][b] = copyMap[a][b];
		}
	}
}
void print() {
	for (int a = 0; a < 2; a++) {
		for (int b = 0; b < 2; b++) {
			cout << copyMap[a][b]<<" ";
		}
		cout << '\n';
	}
	cout << "*********************************"<<'\n';
}
float solve() {
	float sum = 0;
	for (int a = 0; a < 2; a++) {
		sum = sum + ((float)map[0][a] / (float)map[1][a]);
	}
	return sum;
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	init();
	for (int a = 0; a < 2; a++) {
		for (int b = 0; b < 2; b++) {
			cin >> map[a][b];
		}
	}
	float min = -1;
	float temp;
	int index = 0;
	int ans = 0;
	for (int a = 0; a < 4; a++) {
		float temp = solve();
		if (temp > min) {
			min = temp;
			ans = a;
		}
		move();
	}
	cout << ans;
}