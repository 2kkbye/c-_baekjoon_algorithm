#include<iostream>
#include<vector>

using namespace std;

vector<int> v;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string num1, num2;
	cin >> num1 >> num2;
	if (num1.length() < num2.length()) {
		string temp = num1;
		num1 = num2;
		num2 = temp;
	}
	int mok = 0;
	int dif = num1.length() - num2.length();
	bool check_mok;
	for (int a = num2.length() - 1; a >= 0; a--) {
		check_mok = false;
		int temp = mok + ((int)num2[a] - 48) + ((int)num1[a + dif] - 48);
		if (temp > 1) {
			temp -= 2;
			mok = 1;
			check_mok = true;
		}
		if (!check_mok) mok = 0;
		v.push_back(temp);
	}
	for (int a = dif - 1; a >= 0; a--) {
		check_mok = false;
		int temp = ((int)num1[a] - 48) + mok;
		if (temp > 1) {
			temp -= 2;
			mok = 1;
			check_mok = true;
		}
		if (!check_mok) mok = 0;
		v.push_back(temp);
	}
	if (mok == 1) v.push_back(1);
	bool check = false;
	for (int a = v.size() - 1; a >= 0; a--) {
		if (!check && v[a] == 0) continue;
		check = true;
		cout << v[a];
	}
	if (!check) cout << "0";


}