#include<iostream>
#define SIZE 50

using namespace std;


void quick(int start, int end, int arr[]) {
	int s = start;
	int e = end;
	int pivot = arr[(start + end) / 2];
	do {
		while (arr[s] < pivot) s++;
		while (arr[e] > pivot) e--;
		if (s <= e) {
			int temp = arr[s];
			arr[s] = arr[e];
			arr[e] = temp;
			s++; e--;
		}
	} while (s <= e);
	if (s < end) {
		quick(s, end, arr);
	}
	if (start < e) {
		quick(start, e, arr);
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int set_pe[SIZE];
	int pe[SIZE];
	int N, M;
	cin >> N >> M;
	for (int a = 0; a < M; a++) {
		cin >> set_pe[a] >> pe[a];
	}
	quick(0, M - 1, pe);
	quick(0, M - 1, set_pe);
	
	if (N <= 6) {
		if (pe[0] * N > set_pe[0]) {
			cout << set_pe[0];
		}
		else {
			cout << pe[0] * N;
		}
		return 0;
	}
	//N이 6이상일때 구해야한다.
	int temp = N / 6;
	if (N % 6 != 0) temp++;
	int ans = pe[0] * N;
	for (int a = 1; a <= temp; a++) {
		int tp = (set_pe[0] * a);
		if ((N - (6 * a)>=0)) {
			tp += (pe[0] * (N - (6 * a)));
		}
		if (ans > tp) ans = tp;
	}
	cout << ans;
}