#include<iostream>

using namespace std;

int arr[2][7] = { 0, };
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N, K, sum = 0, S, Y;

	cin >> N >> K;
	for (int a = 0; a < N; a++) {
		cin >> S >> Y;
		arr[S][Y]++;
	}
	for (int a = 0; a < 2; a++) {
		for (int b = 1; b < 7; b++) {
			sum += (arr[a][b] / K);
			if (arr[a][b] % K != 0) sum += 1;
		}
	}
	cout << sum;
}