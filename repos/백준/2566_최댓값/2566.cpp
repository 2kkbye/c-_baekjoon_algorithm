#include<iostream>
#define SIZE 10
using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int num,max_num = -1;
	int max_x, max_y;
	for (int a = 1; a < SIZE; a++) {
		for (int b = 1; b < SIZE; b++) {
			cin >> num;
			if (num > max_num) {
				max_num = num;
				max_x = a; max_y = b;
			}
		}
	}
	cout << max_num << '\n';
	cout << max_x << " " << max_y;

}