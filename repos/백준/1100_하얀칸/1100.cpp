#include<iostream>

#define SIZE 8

int arr[SIZE][SIZE] = {0,};

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string str;

	for (int a = 0; a < SIZE; a++) {
		cin >> str;
		for (int b = 0; b < str.length(); b++) {
			if (str[b] == '.') continue;
			arr[a][b] = 1;
		}
	}
	int cnt = 0;
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			if (a % 2 == 0 && b % 2 == 1) continue;
			if (a % 2 == 1 && b % 2 == 0) continue;
			if (arr[a][b] == 0) continue;
			cnt++;
		}
	}
	cout << cnt;

}