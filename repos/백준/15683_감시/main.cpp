#include<iostream>
#include<vector>
#define SIZE 9

using namespace std;

bool visit[SIZE * SIZE];
int map[SIZE][SIZE];
int copyMap[SIZE][SIZE];
int dx[4] = { 0,1,0,-1 };
int dy[4] = { 1,0,-1,0 };
int N, M;
int level = 0;

vector<pair<int, int>> cc;

vector<vector<int>> cctv;
int ans = 100000000;
void copyToMap() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			copyMap[a][b] = map[a][b];
		}
	}
}

void init() {
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			map[a][b] = 0;
			copyMap[a][b] = 0;
		}
	}
	for (int a = 0; a < SIZE * SIZE; a++) {
		visit[a] = false;
	}
}
void Input() {
	cin >> N >> M;
	int temp;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			cin >> temp;
			map[a][b] = temp;
			if (temp != 0 && temp != 6) {
				cc.push_back(make_pair(a, b));
				level++;
			}
		}
	}
}
void moveCCTV() {
	for (int a = 0; a < cctv.size(); a++) {
		int px = cctv[a][0];
		int py = cctv[a][1];
		for (int b = 2; b < cctv[a].size(); b++) {
			int dir = cctv[a][b];
			int rx = px;
			int ry = py;
			while (true) {
				rx = rx + dx[dir];
				ry = ry + dy[dir];
				if (rx >= 0 && ry >= 0 && rx < N && ry < M) {
					if (copyMap[rx][ry] == 6) break;
					if (copyMap[rx][ry] == 0) {
						copyMap[rx][ry] = 7;
						continue;
					}
				}
				else {
					break;
				}
			}
		}
	}
}
int checkMap() {
	int count = 0;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			if (copyMap[a][b] == 0) {
				count++;
			}
		}
	}
	return count;
}
void Print() {
	cout << "========================" << endl;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			cout << copyMap[a][b] << " ";
		}
		cout << endl;
	}
}
void dfs(int lev) {
	if (lev == level) {
		moveCCTV();
		//Print();
		int tans = checkMap();
		if (tans < ans) {
			ans = tans;
		}
		copyToMap();
		return;
	}
	int a = cc[lev].first;
	int b = cc[lev].second;
	int index;
	vector<int> temp;

	if (copyMap[a][b] == 1) {
		index = 4;
		for (int c = 0; c < index; c++) {
			temp.push_back(a);
			temp.push_back(b);
			temp.push_back(c);
			cctv.push_back(temp);

			dfs(lev + 1);
			cctv.erase(cctv.end() - 1, cctv.end());
			temp.clear();
		}
	}
	else if (copyMap[a][b] == 2) {
		int index = 2;
		for (int c = 0; c < index; c++) {
			temp.push_back(a);
			temp.push_back(b);
			temp.push_back(c);
			temp.push_back(c + 2);
			cctv.push_back(temp);
			dfs(lev + 1);
			cctv.erase(cctv.end() - 1, cctv.end());
			temp.clear();
		}
	}
	else if (copyMap[a][b] == 3) {
		int index = 4;
		for (int c = 0; c < index; c++) {
			temp.push_back(a);
			temp.push_back(b);
			temp.push_back(c);
			if (c + 1 == 4) {
				temp.push_back(0);
			}
			else {
				temp.push_back(c + 1);
			}
			cctv.push_back(temp);
			dfs(lev + 1);
			cctv.erase(cctv.end() - 1, cctv.end());
			temp.clear();
		}
	}
	else if (copyMap[a][b] == 4) {
		int index = 4;
		for (int c = 0; c < index; c++) {
			temp.push_back(a);
			temp.push_back(b);
			temp.push_back(c);
			if (c + 1 > 3) {
				temp.push_back(c + 1 - 4);
			}
			else temp.push_back(c + 1);
			if (c + 2 > 3) {
				temp.push_back(c + 2 - 4);
			}
			else {
				temp.push_back(c + 2);
			}
			cctv.push_back(temp);
			dfs(lev + 1);
			cctv.erase(cctv.end() - 1, cctv.end());
			temp.clear();
		}
	}
	else {
		temp.push_back(a);
		temp.push_back(b);
		temp.push_back(0);
		temp.push_back(1);
		temp.push_back(2);
		temp.push_back(3);
		cctv.push_back(temp);
		dfs(lev + 1);
		cctv.erase(cctv.end() - 1, cctv.end());
		temp.clear();
	}
}
void Solution() {
	copyToMap();
	dfs(0);
	cout << ans;
}
void Solve() {
	init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	Solve();
}