#include<iostream>
#include<cmath>

using namespace std;

float price[] = {350.34,230.90,190.55,125.30,180.90};
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	float res;
	int t;
	cin >> t;
	for (int a = 0; a < t; a++) {
		float num;
		res = 0;
		for (int b = 0; b < 5; b++) {
			cin >> num;
			res += (num * price[b]);
		}
		cout << "$";
		cout << fixed;
		cout.precision(2);
		//cout << round(res * 100) / 100;
		cout << res;
		if (a != t - 1) cout << endl;
	}

}