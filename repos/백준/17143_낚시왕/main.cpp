#include<iostream>
#include<vector>
#define SIZE 101

struct shark {
	int x, y, force, speed, dir;
};
using namespace std;

//1위 , 2 아래, 3 오른쪽, 4 왼쪽
int dx[5] = { 0,-1,1,0,0 };
int dy[5] = { 0,0,0,1,-1 };

int map[SIZE][SIZE];
int copyMap[SIZE][SIZE];
vector<shark> v_shark;
int N, M;
int ans = 0;
void Input() {
	int k;
	cin >> N >> M >> k;
	N += 1;
	M += 1;
	for (int a = 0; a < k; a++) {
		shark temp;
		cin >> temp.x >> temp.y >> temp.speed >> temp.dir >> temp.force;
		v_shark.push_back(temp);
	}
}
void init() {
	for (int a = 0; a < SIZE; a++) {
		for (int b = 0; b < SIZE; b++) {
			map[a][b] = 0;
		}
	}
}
void print() {
	cout << "==============" << endl;
	for (int a = 1; a < N; a++) {
		for (int b = 1; b < M; b++) {
			cout << map[a][b] << " ";
		}
		cout << endl;
	}
}
void markSharkToMap() {
	for (int a = 0; a < v_shark.size(); a++) {
		map[v_shark[a].x][v_shark[a].y] = a + 1;
	}
}
void deadShark(int index) {
	for (int a = 1; a < N; a++) {
		if (map[a][index] == 0) continue;
		int te = map[a][index] - 1;
		ans += v_shark[te].force;
		map[v_shark[te].x][v_shark[te].y] = 0;
		v_shark.erase(v_shark.begin() + te, v_shark.begin() + te + 1);
		return;
	}
}
void moveShark() {
	vector<shark> temp_shark;
	for (int a = 0; a < v_shark.size(); a++) {
		shark sh = v_shark[a];
		int px = sh.x;
		int py = sh.y;
		map[px][py] = 0;
		int rotate = (M - 2) * 2;
		if (sh.dir == 1 || sh.dir == 2) {
			rotate = (N - 2) * 2;
		}
		for (int a = 0; a < sh.speed % rotate; a++) {
			px += dx[sh.dir];
			py += dy[sh.dir];
			if (px >= 1 && py >= 1 && px < N && py < M) continue;
			if (sh.dir % 2 == 0) {
				sh.dir -= 1;
			}
			else {
				sh.dir += 1;
			}
			a--;
			px += dx[sh.dir];
			py += dy[sh.dir];
		}
		if (copyMap[px][py] > sh.force) continue;
		sh.x = px;
		sh.y = py;
		if (copyMap[px][py] == 0) {
			copyMap[px][py] = sh.force;
			temp_shark.push_back(sh);
			continue;
		}
		if (copyMap[px][py] < sh.force) {
			for (int b = 0; b < temp_shark.size(); b++) {
				if (temp_shark[b].x == px && temp_shark[b].y == py) {
					temp_shark.erase(temp_shark.begin() + b, temp_shark.begin() + b + 1);
					break;
				}
			}
			copyMap[px][py] = sh.force;
			temp_shark.push_back(sh);
		}
	}
	v_shark.clear();
	for (int a = 0; a < temp_shark.size(); a++) {
		v_shark.push_back(temp_shark[a]);
		copyMap[temp_shark[a].x][temp_shark[a].y] = 0;
	}
}
void Solution() {
	for (int a = 1; a < M; a++) {
		markSharkToMap();
		//print();
		deadShark(a);
		moveShark();
	}
	cout << ans;
}
void Solve() {
	init();
	Input();
	Solution();
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();
}