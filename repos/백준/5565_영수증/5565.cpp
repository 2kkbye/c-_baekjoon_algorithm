#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int sum, res = 0, num;

	cin >> sum;

	for (int a = 0; a < 9; a++) {
		cin >> num;
		res += num;
	}
	cout << sum - res;
}