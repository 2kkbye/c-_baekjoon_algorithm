#include<iostream>
#define SIZE 10001
using namespace std;

long long arr[SIZE] = {0,};
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	long long n;
	cin >> n;
	arr[0] = 0;
	arr[1] = 1;
	for (long long a = 2; a <= n; a++) {
		arr[a] = arr[a - 1] + arr[a - 2];
	}
	cout << arr[n];
}