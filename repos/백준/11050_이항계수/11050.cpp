#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N, K;
	cin >> N >> K;

	if ((K == N) || K == 0) {
		cout << 1;
		return 0;
	}
	int up=1, down=1;
	for (int a = N; a > N - K; a--) {
		up *= a;
	}
	for (int a = 2; a <=K; a++) {
		down *= a;
	}
	cout << up / down;
}