#include<iostream>
#define SIZE 100
using namespace std;

string str[SIZE];
int N;
void solve(int k) {
	if (k == 1) {
		for (int a = 0; a < N; a++) {
			cout << str[a];
			if (a == N - 1) break;
			cout << endl;
		}
	}
	else if (k == 2) {
		for (int a = 0; a < N; a++) {
			for (int b = N - 1; b >= 0; b--) {
				cout << str[a][b];
			}
			if (a == N - 1) break;
			cout << endl;
		}
	}
	else {
		for (int a = N - 1; a >= 0; a--) {
			cout << str[a];
			if (a == 0) break;
			cout << endl;
		}
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	int K;
	cin >> N;
	for (int a = 0; a < N; a++) {
		cin >> str[a];
	}
	cin >> K;
	solve(K);
	

}