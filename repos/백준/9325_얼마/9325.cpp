#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t,car_price,option_num,q,p;

	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> car_price;
		cin >> option_num;
		for (int a = 0; a < option_num; a++) {
			cin >> q >> p;
			car_price += (q * p);
		}
		cout << car_price;
		if (a == t - 1) break;
		cout << '\n';
	}

}