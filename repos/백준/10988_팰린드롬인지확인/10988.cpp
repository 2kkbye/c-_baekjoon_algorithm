#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string str;
	cin >> str;
	int res = 1;
	for (int a = 0; a < (str.size() / 2); a++) {
		if (str[a] == str[str.size() - 1 - a]) continue;
		res = 0;
	}
	cout << res;
}