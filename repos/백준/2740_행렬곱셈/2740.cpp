#include<iostream>
#define SIZE 100
using namespace std;

int mA[SIZE][SIZE];
int mB[SIZE][SIZE];
int res[SIZE][SIZE];
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int N, M, K;
	cin >> N >> M;
	int num;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < M; b++) {
			cin >> num;
			mA[a][b] = num;
		}
	}
	cin >> M >> K;
	for (int a = 0; a < M; a++) {
		for (int b = 0; b < K; b++) {
			cin >> num;
			mB[a][b] = num;
		}
	}
	int rs = 0;
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < K; b++) {
			for (int c = 0; c < M; c++) {
				rs += (mA[a][c] * mB[c][b]);
			}
			res[a][b] = rs;
			rs = 0;
		}
	}
	for (int a = 0; a < N; a++) {
		for (int b = 0; b < K; b++) {
			cout << res[a][b];
			if (b != K - 1) cout << " ";
		}
		if (a != N - 1) cout << endl;
	}
}