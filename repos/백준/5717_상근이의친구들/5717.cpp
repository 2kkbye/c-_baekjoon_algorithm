#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int m, w;
	while (true) {
		cin >> m >> w;
		if (m == 0 && w == 0) break;
		cout << m + w << endl;
	}
}