#include<iostream>
#include<cmath>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t, n;
	double c, sum_c = 0, res, g, sum_g = 0;

	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> n;
		for (int b = 0; b < n; b++) {
			cin >> c >> g;
			sum_c += c;
			sum_g += (c * g);
		}
		res = sum_g / sum_c;
		cout << sum_c << " " << round(res*10)/10;
		if (a == t - 1) break;
		cout << '\n';
		sum_c = 0; sum_g = 0;
	}
}