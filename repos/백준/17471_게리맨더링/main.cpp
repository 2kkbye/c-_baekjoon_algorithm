#include<iostream>
#include<vector>
#include<queue>
#define SIZE 11
//#define SIZE 7
using namespace std;

int people[SIZE];
int N;
int ans = 100000000;

bool visit[SIZE];
bool cpVisit[SIZE];
bool map[SIZE][SIZE];

void initCpVisit() {
	for (int a = 1; a <= N; a++) {
		cpVisit[a] = false;
	}
}
void Input() {
	cin >> N;
	for (int a = 1; a <= N; a++) {
		cin >> people[a];
	}
	for (int a = 1; a <= N; a++) {
		int cnt, index;
		cin >> cnt;
		for (int b = 0; b < cnt; b++) {
			cin >> index;
			map[a][index] = true;
		}
	}
}
int countAns() {
	int cntT = 0;
	int cntF = 0;
	for (int a = 1; a <= N; a++) {
		if (visit[a]) {
			cntT += people[a];
		}
		else {
			cntF += people[a];
		}
	}
	if (cntT > cntF) {
		return cntT - cntF;
	}
	else {
		return cntF - cntT;
	}
}

bool checkConnection(vector<int> v, bool T) {

	initCpVisit();
	queue<int> q;
	cpVisit[v[0]] = true;
	q.push(v[0]);
	int cnt = 1;

	while (q.size() != 0) {
		int temp = q.front();
		q.pop();

		for (int a = 1; a <= N; a++) {
			if (map[temp][a] && visit[a] == T && !cpVisit[a]) {
				cpVisit[a] = true;
				q.push(a);
				cnt++;
			}
		}
	}
	if (cnt == v.size()) return true;
	else return false;
}

bool check() {

	vector<int> A, B;
	for (int a = 1; a <= N; a++) {
		if (visit[a]) A.push_back(a);
		else B.push_back(a);
	}

	if (A.size() == 0 || B.size() == 0) return false;

	if (!checkConnection(A, true)) return false;

	if (!checkConnection(B, false)) return false;

	return true;

}
void dfs(int idx, int cnt) {
	if (cnt >= 1) {

		if (check()) {
			int tans = countAns();
			if (tans < ans) {
				ans = tans;
			}
		}
	}
	for (int a = idx; a <= N; a++) {
		if (visit[a]) continue;
		visit[a] = true;
		dfs(a, cnt + 1);
		visit[a] = false;
	}
}
void Solution() {
	dfs(1, 0);

	if (ans == 100000000) cout << -1;
	else cout << ans;
}
void Solve() {
	Input();
	Solution();
}

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	Solve();
}