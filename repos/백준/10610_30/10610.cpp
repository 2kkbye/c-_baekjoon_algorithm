#include<iostream>
#define SIZE 100000
using namespace std;

int num[SIZE];
void quick(int start, int end) {
	int s = start;
	int e = end;
	int pivot = num[(start + end) / 2];
	do {
		while (num[s] > pivot) s++;
		while (num[e] < pivot) e--;
		if (s <= e) {
			int temp = num[s];
			num[s] = num[e];
			num[e] = temp;
			s++, e--;
		}
	} while (s <= e);
	if (s < end) {
		quick(s, end);
	}
	if (start < e) {
		quick(start, e);
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	string str;
	cin >> str;
	
	int max_len = str.length();
	long long sum = 0;
	bool check = false;
	for (int a = 0; a < max_len; a++) {
		num[a] = ((int)str[a] - 48);
		if (num[a] == 0) check = true;
		sum += num[a];
	}
	if (check && sum % 3 == 0) {
		quick(0, max_len - 1);
		for (int a = 0; a < max_len; a++) {
			printf("%d", num[a]);
		}
	}
	else {
		printf("-1");
	}

}