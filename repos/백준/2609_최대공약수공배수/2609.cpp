#include<iostream>
#define SIZE 2
using namespace std;

int num[SIZE];
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	for (int a = 0; a < SIZE; a++) {
		cin >> num[a];
	}
	bool check = true;
	int div = 2;
	int min=1, max=1;
	while (check) {
		if (num[0] % div == 0 && num[1] % div == 0) {
			min *= div;
			max *= div;
			num[0] /= div;
			num[1] /= div;
			div = 2; continue;
		}
		div++;
		if (num[0] < div || num[1] < div) break;
	}
	min *= num[0];
	min *= num[1];
	cout << max << endl;
	cout << min;
}