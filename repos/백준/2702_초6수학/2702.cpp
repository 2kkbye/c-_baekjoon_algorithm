#include<iostream>

using namespace std;
int gcd(int a, int b) {
	int c;
	while (b) {
		c = a % b;
		a = b;
		b = c;
	}
	return a;
}
int ch_min(int x, int y) {
	int ans = 1;
	for (int a = 2; a <= x; a++) {
		if (x % a != 0 || y % a != 0) continue;
		ans *= a;
		x /= a;
		y /= a;
		a = 1; continue;
	}
	ans *= x;
	ans *= y;
	return ans;

}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int t, x, y;
	cin >> t;
	for (int a = 0; a < t; a++) {
		cin >> x >> y;
		int max,min;
		if (x < y) {
			max = gcd(x, y);
			min = ch_min(x, y);
		}
		else {
			max = gcd(y, x);
			min = ch_min(y, x);
		}
		cout << min << " " << max;
		if (a != t - 1) cout << endl;
	}
}