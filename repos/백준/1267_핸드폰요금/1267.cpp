#include<iostream>

using namespace std;

int main() {
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);

	int young=0, min=0, n,temp;
	cin >> n;
	for (int a = 0; a < n; a++) {
		cin >> temp;
		young += (((temp / 30)+1) * 10);
		min += (((temp / 60)+1) * 15);
	}
	if (young > min) {
		cout << "M" << " " << min;
	}
	else if (young < min) {
		cout << "Y" << " " << young;
	}
	else {
		cout << "Y M " << min;
	}
}